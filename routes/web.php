<?php
Auth::routes();
Route::get('/','SinglePageController@home');
Route::get('/dashboard','SinglePageController@home');
Route::get('/logout','SinglePageController@cerrarSession');
Route::group(['middleware' => ['auth','validateActive']],function()
{
    Route::get('documentotipo','RecursosHumanosController@documentoTipos');
    Route::resource('impuestos', 'TblImpuestosController');
    Route::resource('estudiosprevios', 'EstudiosPreviosController');
    Route::resource('estudiospreviostipo', 'EstudiosPreviosTipoController');
    Route::resource('estudiospreviosestado', 'EstudiosPreviosEstadoController');
    Route::resource('departamento', 'DepartamentosController');
    Route::resource('municipio', 'MunicipiosController');
    #region OTHERS FUNCTIONS
    Route::get('/limpiar','SinglePageController@limpiar');
    Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');
    Route::get('/dashboard/{any}', 'SinglePageController@index')->where('any', '.*');
    #endregion OTHERS FUNCTIONS
    Route::group(['prefix' =>'contratistas'],function()
    {
        Route::post('/searcdocumento','ContratistaController@search');
        Route::post('/veroferentes','ContratistaController@verOferentes');
    });
    Route::group(['prefix' =>'informes'],function()
    {
        Route::get('/contratosfinalizados','informesController@contratosfinalizados');
        Route::get('/contratospagosestados','informesController@contratosOrdenesPago');
    });

    Route::post('/plantilla/listachequeo','TblListaChequeoController@indexPlantilla');
    Route::group(['prefix' =>'listachequeo'],function()
    {
        Route::resource('/','TblListaChequeoController');
    });
    
    Route::group(['prefix' =>'contratos'],function()
    {
        Route::resource('/', 'contratosController');
        Route::get('/all', 'contratosController@allContratos');
        Route::post('/searchcodigo', 'contratosController@searchEstudioNecesidad');
        Route::post('/searchcpd', 'contratosController@indexCDPaprobados');
        Route::post('/porcentaje/ejecucion', 'contratosController@porcentajeEjecucion');
        Route::get('/show/{id}', 'contratosController@show');
        Route::get('/showCDP/{id}', 'contratosController@showCDP');
        Route::get('/datosCDP/{id}', 'contratosController@datosCDP');
        Route::post('/datosCDP/save', 'contratosController@saveContrato');
        Route::get('/todos', 'contratosController@todosContratos');
        Route::get('/liquidacion', 'contratosController@liquidacionShow');
        Route::get('/detalle/{contrato_id}', 'contratosController@detalleContrato');

    });
    Route::group(['prefix' =>'contratacion'],function()
    {
        Route::resource('/menorcuantia', 'ContratacionDirectaController');
        Route::resource('/directa', 'ContratacionDirectaController');
        Route::get('/tipos/{name}', 'EstudiosPreviosController@tiposContrato');
        Route::get('/menorcuantia/detail/{id}', 'ContratacionDirectaController@indexDetail');
        Route::get('/directa/detail/{id}', 'ContratacionDirectaController@indexDetail');
    });

    Route::resource('liquidacion', 'TblContratoLiquidacionController');
    Route::resource('liquidacion-revision-inicial', 'TblContratoLiquidacionRevisionInicialController');
    Route::resource('liquidacion-firma-gerencia', 'TblContratoLiquidacionFirmaGerenciaController');
    Route::resource('observaciones-postcontracuales', 'TblContratoLiquidacionObservaionesPostContractualesController');
    
    Route::group(['prefix' =>'supervision'],function()
    {
        Route::post('/', 'SupervisionController@store');
        Route::get('/contrato/{id}', 'SupervisionController@supervisionesContrato');
    });

    Route::group(['prefix' =>'devoluciones'],function()
    {
        Route::post('/', 'DevolucionesController@store');
        Route::get('/index', 'DevolucionesController@verDevoluciones');
    });

    Route::group(['prefix' =>'revisiones'],function()
    {
        Route::get('/', 'RevisionesTramitesController@index');
        Route::get('/aprobada', 'RevisionesTramitesController@index');
        Route::post('/', 'RevisionesTramitesController@guardarPresentacion');
        Route::put('/{id}', 'RevisionesTramitesController@editarPresentacion');
        Route::get('/contratos/{id}', 'RevisionesTramitesController@presentacionesContrato');

    });

    Route::group(['prefix' =>'oferente'],function()
    {
        Route::resource('/', 'OferenteController');
        Route::post('documento', 'OferenteController@SearchDocumento');
    });
    Route::group(['prefix' =>'proponente'],function()
    {
        Route::resource('/', 'ProponenteController');
        Route::post('documento', 'ProponenteController@SearchDocumento');
    });
    Route::prefix('usuarios')->group(function () {
        Route::get('/selected',      'UsuariosController@usuarioActual');
        Route::get('/',              'UsuariosController@index');
        Route::get('/roles',         'UsuariosController@rolesAll');
        Route::post('/delete',       'UsuariosController@borrar');
        Route::get('/edit/{id}',     'UsuariosController@usuariosFind');
        Route::post('/editSave/{id}','UsuariosController@editSave');
        Route::post('/newsave',      'UsuariosController@newsave');
        Route::get('/permistions',   'AuthController@permistions');
        Route::post('/permistions/all','AuthController@permistionsActuales');
        Route::post('/permistions/save','AuthController@permistionsSave');
    });
    Route::group(['prefix' => 'comiteevaluador'], function() {
        Route::resource('/', 'ComiteEvaluadorController');
        Route::post('documento', 'ComiteEvaluadorController@SearchDocumento');
    });
    Route::group(['prefix' => 'cargos'], function() {
        Route::resource('/','CargosController');
    });
    Route::group(['prefix' => 'recursoshumanos'], function() {
        Route::resource('/','RecursosHumanosController');
        Route::post('documento', 'RecursosHumanosController@searchDocument');
    });
    //Route::group(['prefix' => 'contratosmodalidad'], function() {
        //    Route::resource('/','ContratosModalidadController');
        //});
        Route::group(['prefix' => 'plananualadquisicion'], function() {
        Route::get('','TblPlanAnualAdquisicionController@index');
        Route::post('','TblPlanAnualAdquisicionController@store');
        Route::get('/show/{id}','TblPlanAnualAdquisicionController@show');
        Route::get('search','TblPlanAnualAdquisicionController@search'); 
        Route::post('{id}','TblPlanAnualAdquisicionController@update');
        Route::delete('{id}','TblPlanAnualAdquisicionController@destroy');
    });
    Route::group(['prefix' => 'formaspago'], function() {
        Route::get('/','TblFomasPagoController@index');
    });
    Route::group(['prefix' => 'tipologiascontractuales'], function() {
        Route::get('/','TblTipologiasContractualesController@index');
    });
    Route::group(['prefix' => 'garantias'], function() {
        Route::resource('/','TblGarantiasController');
    });

    Route::group(['prefix' => 'coberturas'], function() {
        Route::resource('/','TblContratosCoberturasController');
        Route::get('/contratos', 'TblContratosCoberturasController@getContratos');
    });

    Route::group(['prefix' => 'actas-de-inicio'], function() {
        Route::resource('/', 'ActaDeInicioController');
        Route::get('/contrato/{id}', 'ActaDeInicioController@getContrato');
    });

    Route::group(['prefix' => 'actas-de-suspension'], function() {
        Route::resource('/', 'ActasDeSuspensionController');
        Route::get('/contratos-validos/{ncontrato}', 'ActasDeSuspensionController@contratosValidos');
    });

    Route::group(['prefix' => 'actas-de-reinicio'], function() {
        Route::resource('/', 'ActasReinicioController');
        Route::get('/contratos-validos/{ncontrato}', 'ActasReinicioController@contratosValidos');
    });

    Route::group(['prefix' => 'otros-si'], function() {
        Route::resource('/', 'OtrosSiController');
        Route::get('/contratos-validos/{ncontrato}', 'OtrosSiController@contratosValidos');
    });
    
    Route::resource('ordenes-de-pago', 'TblContratosPagosOrden');
    Route::group(['prefix' => 'ordenes-de-pago'], function() {
        Route::get('/status/{status}', 'TblContratosPagosOrden@indexStatus');
        Route::get('/realizados/{id}', 'TblContratosPagosOrden@pagosRealizadosIndex');
    });

    Route::resource('registroscontables', 'TblContratatosPagosRegistroContableController');
    Route::group(['prefix' => 'registroscontables'], function() {
        Route::get('/showxordenpago/{id}', 'TblContratatosPagosRegistroContableController@showxOrdenPago');
        Route::get('/status/{status}', 'TblContratatosPagosRegistroContableController@indexStatus');
    });
    
    Route::resource('registropresupuestal', 'TblContratatosPagosRegistroPresupuestalController');
    Route::group(['prefix' => 'registropresupuestal'], function() {
        Route::get('/status/{status}', 'TblContratatosPagosRegistroPresupuestalController@indexStatus');
    });
    Route::resource('firmaadministracion', 'TblContratatosPagosFirmaAdministrtacionController');
    Route::group(['prefix' => 'firmaadministracion'], function() {
        Route::get('/status/{status}', 'TblContratatosPagosFirmaAdministrtacionController@indexStatus');
    });

    Route::resource('firmagerencia', 'TblContratatosPagosFirmaGerenciaController');
    Route::group(['prefix' => 'firmagerencia'], function() {
        Route::get('/status/{status}', 'TblContratatosPagosFirmaGerenciaController@indexStatus');
    });

    Route::resource('tesoreria', 'TblContratatosPagosTesoreriaController');
    Route::group(['prefix' => 'tesoreria'], function() {
        Route::get('/status/{status}', 'TblContratatosPagosTesoreriaController@indexStatus');
    });

    Route::group(['prefix' => 'actas-de-suspension'], function() {
        Route::resource('/', 'ActasDeSuspensionController');
        Route::get('/contratos-validos/{ncontrato}', 'ActasDeSuspensionController@contratosValidos');
    });

    Route::group(['prefix' => 'actas-de-reinicio'], function() {
        Route::resource('/', 'ActasReinicioController');
        Route::get('/contratos-validos/{ncontrato}', 'ActasReinicioController@contratosValidos');
    });

    Route::group(['prefix' => 'otros-si'], function() {
        Route::resource('/', 'OtrosSiController');
        Route::get('/contratos-validos/{ncontrato}', 'OtrosSiController@contratosValidos');
    });
    Route::get('/add', 'TblMensajesAlertController@add');
    Route::get('/showAlerts', 'TblMensajesAlertController@showAlert');
    Route::get('/showAlerts/{id}', 'TblMensajesAlertController@show');



});
