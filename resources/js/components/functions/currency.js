const fn = {
    formatCurrency(value)
    {
        let miles=',';
        let decimales='.';
        let data = (typeof value == 'undefined' || value == null )?'0':value.toString();
        data = parseFloat(data.split(miles).join('')).toFixed(2);
        var out = '';
        var filtro = '-1234567890'+decimales;
        let decimal=true;
        for (var i=0; i<data.length; i++)
        {

            if (filtro.indexOf(data.charAt(i)) != -1) 
            {
                if(data.charAt(i)==decimales)
                {
                    if((data.split(decimales).filter( c => c != ' ' ).length-1)<=1)
                    {
                        out += data.charAt(i);        
                        decimal=false;
                    }
                    if (decimal)
                    {
                        decimal=false;
                        out += data.charAt(i);        
                    }
                }else{

                    out += data.charAt(i);
                }
            }
        }
        let returns=out.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1'+miles)
        return returns.split('.00').join('');
    },
    deleteFormatCurrency(value)
    {
        let miles=',';
        let decimales='.';
        let data = (typeof value == 'undefined' || value == null )?'0':value.toString();
        return parseFloat(parseFloat(data.split(miles).join('')).toFixed(2));
    }
}
export default fn;