var CifrasEnLetras = {
 
    PREFIJO_ERROR: "Error: ",
    COMA: ",",
    MENOS: "-",
    SEPARADOR_SEIS_CIFRAS: " ",
   
    // Listas -------------------------------------
   
    listaUnidades: [ // De 0 a 29
      "cero", "un", "dos", "tres", "cuatro", "cinco", "seis", "siete", "ocho", "nueve",
      "diez", "once", "doce", "trece", "catorce", "quince", "dieciséis", "diecisiete", "dieciocho", "diecinueve",
      "veinte", "veintiún", "veintidós", "veintitrés", "veinticuatro", "veinticinco", "veintiséis", "veintisiete", "veintiocho", "veintinueve",
    ],
    listaDecenas: [
      "", "diez", "veinte", "treinta", "cuarenta", "cincuenta", "sesenta", "setenta", "ochenta", "noventa",
    ],
    listaCentenas: [
      "", "cien", "doscientos", "trescientos", "cuatrocientos", "quinientos", "seiscientos", "setecientos", "ochocientos", "novecientos",
    ],
    listaOrdenesMillonSingular: [
      "", "millón", "billón", "trillón", "cuatrillón", "quintillón",
      "sextillón", "septillón", "octillón", "nonillón", "decillón",
      "undecillón", "duodecillón", "tridecillón", "cuatridecillón", "quidecillón",
      "sexdecillón", "septidecillón", "octodecillón", "nonidecillón", "vigillón",
    ],
    listaOrdenesMillonPlural: [
      "", "millones", "billones", "trillones", "cuatrillones", "quintillones",
      "sextillones", "septillones", "octillones", "nonillones", "decillones",
      "undecillones", "duodecillones", "tridecillones", "cuatridecillones", "quidecillones",
      "sexdecillones", "septidecillones", "octodecillones", "nonidecillones", "vigillones",
    ],
    convertirUnidades(unidades, genero) 
    {
      if(true)// (CifrasEnLetras)
      {
        if (unidades == 1) {
          if (genero == "masculino") return "uno";
          else if (genero == "femenino") return "una";
        }
        else if (unidades == 21) {
          if (genero == "masculino") return "veintiuno";
          else if (genero == "femenino") return "veintiuna";
        }
        return this.listaUnidades[unidades];
      }
    },
    convertirCentenas(centenas, genero) {
      if(true){// (CifrasEnLetras) {
        var resultado = this.listaCentenas[centenas];
        if (genero == "femenino") {
          resultado = this.reemplazar(resultado, "iento","ienta");
        }
        return resultado;
      }
    },
    convertirDosCifras(cifras, genero) {
      if(true){// (CifrasEnLetras) {
        var unidad = cifras % 10;
        var decena = Math.floor(cifras / 10);
        if (cifras < 30) {
          return this.convertirUnidades(cifras, genero);
        }
        else if (unidad == 0) {
          return this.listaDecenas[decena];
        }
        else {
          return this.listaDecenas[decena] + " y " + this.convertirUnidades(unidad, genero);
        }
      }
    },
    convertirTresCifras(cifras, genero) {
      if(true){// (CifrasEnLetras) {
        var decenas_y_unidades = cifras % 100;
        var centenas = Math.floor(cifras / 100);
        if  (cifras < 100) {
          return this.convertirDosCifras(cifras, genero);
        }
        else if (decenas_y_unidades == 0) {
          return this.convertirCentenas(centenas, genero);
        }
        else if (centenas == 1) {
          return "ciento " + this.convertirDosCifras(decenas_y_unidades, genero);
        }
        else {
          return this.convertirCentenas(centenas, genero) + " " +
            this.convertirDosCifras(decenas_y_unidades, genero);
        }
      }
    },
    convertirSeisCifras(cifras, genero) {
      if(true){// (CifrasEnLetras) {
        var primerMillar = cifras % 1000;
        var grupoMiles = Math.floor(cifras / 1000);
        var generoMiles = genero == "masculino" ? "neutro" : genero;
        if (grupoMiles == 0) {
          return this.convertirTresCifras(primerMillar, genero);
        }
        else if (grupoMiles == 1) {
          if (primerMillar == 0) return "mil";
          else return "mil " + this.convertirTresCifras(primerMillar, genero);
        }
        else if (primerMillar == 0) {
          return this.convertirTresCifras(grupoMiles, generoMiles) + " mil";
        }
        else {
          return this.convertirTresCifras(grupoMiles, generoMiles) + " mil " +
            this.convertirTresCifras(primerMillar, genero);
        }
      }
    },
    convertirCifrasEnLetras(cifras, genero, separadorGruposSeisCifras)
    {
      if(true){// (CifrasEnLetras) {
   
        // Predeterminado
        cifras = isNaN(cifras)? cifras:this.reemplazar(cifras+"", ".", this.COMA);
        genero = genero || "neutro";
        separadorGruposSeisCifras = separadorGruposSeisCifras || this.SEPARADOR_SEIS_CIFRAS;
   
        // Inicialización
        cifras = this.recortar(cifras);
        var numeroCifras = cifras.length;
   
        // Comprobación
        if (numeroCifras == 0) {
          return this.PREFIJO_ERROR + "No hay ningún número";
        }
        for (var indiceCifra=0; indiceCifra<numeroCifras; ++indiceCifra) {
            var cifra = cifras.charAt(indiceCifra);
            var esDecimal = "0123456789".indexOf(cifra) >= 0;
            if (!esDecimal) {
              return this.PREFIJO_ERROR + "Uno de los caracteres no es una cifra decimal";
            }
        }
        if (numeroCifras > 126) {
          return this.PREFIJO_ERROR + "El número es demasiado grande ya que tiene más de 126 cifras";
        }
   
        // Preparación
        var numeroGruposSeisCifras = Math.floor(numeroCifras / 6) + this.signo(numeroCifras);
        var cerosIzquierda = this.repetir('0', numeroGruposSeisCifras * 6 - numeroCifras);
        cifras = cerosIzquierda + cifras;
        var ordenMillon = numeroGruposSeisCifras - 1;
   
        // Procesamiento
        var resultado = [];
        for (var indiceGrupo=0; indiceGrupo<numeroGruposSeisCifras*6; indiceGrupo+=6) {
            var seisCifras = parseInt(cifras.substring(indiceGrupo, indiceGrupo+6), 10);
            if (seisCifras != 0) {
                if (resultado.length > 0) {
                  resultado.push(separadorGruposSeisCifras);
                }
   
                if (ordenMillon == 0) {
                    resultado.push(this.convertirSeisCifras(seisCifras, genero));
                }
                else if (seisCifras == 1) {
                    resultado.push("un " + this.listaOrdenesMillonSingular[ordenMillon]);
                }
                else {
                    resultado.push(this.convertirSeisCifras(seisCifras, "neutro") +
                        " " + this.listaOrdenesMillonPlural[ordenMillon]);
                }
            }
            ordenMillon--;
        }
   
        // Finalización
        if (resultado.length == 0) {
          resultado.push(this.listaUnidades[0]);
        }
        return resultado.join("");
      }
    },
   
    convertirCifrasEnLetrasMasculinas(cifras) {
      return CifrasEnLetras.convertirCifrasEnLetras(cifras, "masculino");
    },
   
    convertirCifrasEnLetrasFemeninas(cifras) {
      return CifrasEnLetras.convertirCifrasEnLetras(cifras, "femenino");
    },
    convertirNumeroEnLetras(cifras, numeroDecimales,
      palabraEntera, palabraEnteraPlural, esFemeninaPalabraEntera,
      palabraDecimal, palabraDecimalPlural, esFemeninaPalabraDecimal)
    {
      if(true){// (CifrasEnLetras) {
        // Argumentos predeterminados
        cifras = isNaN(cifras)? cifras:this.reemplazar(cifras+"",".",this.COMA);
        numeroDecimales = numeroDecimales===0? 0: numeroDecimales || -1;
        palabraEntera = palabraEntera || "";
        palabraEnteraPlural = palabraEnteraPlural || palabraEntera+"s";
        esFemeninaPalabraEntera = esFemeninaPalabraEntera || false;
        palabraDecimal = palabraDecimal || "";
        palabraDecimalPlural = palabraDecimalPlural || palabraDecimal+"s";
        esFemeninaPalabraDecimal = esFemeninaPalabraDecimal || false;
   
        // Limpieza
        cifras = this.dejarSoloCaracteresDeseados(cifras, "0123456789" + this.COMA + this.MENOS);
   
        // Comprobaciones
        var repeticionesMenos = this.numeroRepeticiones(cifras, this.MENOS);
        var repeticionesComa = this.numeroRepeticiones(cifras, this.COMA);
        if (repeticionesMenos > 1 || (repeticionesMenos == 1 && !this.empiezaPor(cifras, this.MENOS)) ) 
        {
          return this.PREFIJO_ERROR + "Símbolo negativo incorrecto o demasiados símbolos negativos";
        }
        else if (repeticionesComa > 1) {
          return this.PREFIJO_ERROR + "Demasiadas this.COMAs decimales";
        }
   
        // Negatividad
        var esNegativo = this.empiezaPor(cifras, this.MENOS);
        if (esNegativo) cifras = cifras.substring(1);
   
        // Preparación
        var posicionComa = cifras.indexOf(this.COMA);
        if (posicionComa == -1) posicionComa = cifras.length;
   
        var cifrasEntera = cifras.substring(0, posicionComa);
        if (cifrasEntera == "" || cifrasEntera == this.MENOS) cifrasEntera = "0";
        var cifrasDecimal = cifras.substring(Math.min(posicionComa + 1, cifras.length));
   
        var esAutomaticoNumeroDecimales = numeroDecimales < 0;
        if (esAutomaticoNumeroDecimales) {
          numeroDecimales = cifrasDecimal.length;
        }
        else {
          cifrasDecimal = cifrasDecimal.substring(0, Math.min(numeroDecimales, cifrasDecimal.length));
          var cerosDerecha = repetir('0', numeroDecimales - cifrasDecimal.length);
          cifrasDecimal = cifrasDecimal + cerosDerecha;
        }
   
        // Cero
        var esCero = this.dejarSoloCaracteresDeseados(cifrasEntera,"123456789") == "" &&
          this.dejarSoloCaracteresDeseados(cifrasDecimal,"123456789") == "";
   
        // Procesar
        var resultado = [];
   
        if (esNegativo && !esCero) resultado.push("menos ");
   
        var parteEntera = this.procesarEnLetras(cifrasEntera,
          palabraEntera, palabraEnteraPlural, esFemeninaPalabraEntera);
        if (this.empiezaPor(parteEntera, this.PREFIJO_ERROR)) return parteEntera;
        resultado.push(parteEntera);
   
        if (cifrasDecimal != "") {
          var parteDecimal = this.procesarEnLetras(cifrasDecimal,
            palabraDecimal, palabraDecimalPlural, esFemeninaPalabraDecimal);
          if (this.empiezaPor(parteDecimal, this.PREFIJO_ERROR)) return parteDecimal;
          resultado.push(" con ");
          resultado.push(parteDecimal);
        }
   
        return resultado.join("");
      }
    },
   
    convertirNumeroConParametrosEnLetras(cifras, parametros) {
      parametros = parametros || {};
      return CifrasEnLetras.convertirNumeroEnLetras(
        cifras,
        parametros.numeroDecimales || -1,
   
        parametros.palabraEntera || "",
        parametros.palabraEnteraPlural || "",
        parametros.esFemeninaPalabraEntera || false,
   
        parametros.palabraDecimal || "",
        parametros.palabraDecimalPlural || "",
        parametros.esFemeninaPalabraDecimal || false
      );
    },
   
    /*
      Convertir euros en letras
   
      Ejemplos:
        CifrasEnLetras.convertirEurosEnLetras("44276598801,2",2) --> "cuatrocientos noventa y ocho mil un euros con veinte céntimos"
        CifrasEnLetras.convertirEurosEnLetras(85009) --> "ochenta y cinco mil nueve euros"
        CifrasEnLetras.convertirEurosEnLetras(10200.35) --> "diez mil doscientos euros con treinta y cinco céntimos"
     */
    convertirEurosEnLetras(cifras, numeroDecimales) {
      numeroDecimales = numeroDecimales===0? 0: numeroDecimales || 2;
      return CifrasEnLetras.convertirNumeroEnLetras(cifras, numeroDecimales,
        "euro", "euros", false, "céntimo", "céntimos", false);
    },
   
    /*
      Separa las cifras en grupos de 6 con subrayados y los grupos de 6 en grupos de 2 con punto
      Ejemplo: CifrasEnLetras.formatearCifras("-4739249,2") --> "-4_739.249,2"
    */
    formatearCifras(cifras, formato) {
      if(true){// (CifrasEnLetras) {
        cifras = cifras + "";
        formato = formato || "";
   
        cifras = this.dejarSoloCaracteresDeseados(cifras, "0123456789" + this.COMA + this.MENOS);
        if (cifras.length == 0) return cifras;
   
        var esNegativo = this.empiezaPor(cifras, this.MENOS);
        if (esNegativo) cifras = cifras.substring(1);
   
        var posicionComa = cifras.indexOf(this.COMA);
        var esDecimal = posicionComa >= 0;
   
        if (!esDecimal) posicionComa = cifras.length;
        var cifrasEntera = cifras.substring(0, posicionComa);
        var cifrasDecimal = "";
   
        if (esDecimal) cifrasDecimal = cifras.substring(Math.min(posicionComa + 1, cifras.length));
        if (cifrasEntera == "") cifrasEntera = "0";
   
        var resultado = [];
        var numeroCifras = cifrasEntera.length;
        var par = true;
        var contador = 1;
   
        for (var indice=0; indice<numeroCifras; indice+=3) {
          var indiceGrupo = numeroCifras - indice;
          var tresCifras = cifras.substring(Math.max(indiceGrupo-3,0), indiceGrupo);
          if (indice > 0) {
            switch (formato) {
              case 'html': resultado.unshift(par? ".": '<sub>'+(contador++)+'</sub>'); break;
              default: resultado.unshift(par? '.': '_');
            }
            par = !par;
          }
          resultado.unshift(tresCifras);
        }
        if (esNegativo) resultado.unshift(this.MENOS);
        if (esDecimal) resultado.push(this.COMA + cifrasDecimal);
   
        return resultado.join("");
      }
    },
   
    //---------------------------------------------
    // FUNCIONES AUXILIARES
    //---------------------------------------------
   
    /*
      Borra todos los caracteres del texto que no sea alguno de los caracteres deseados.
      Ejemplos:
        CifrasEnLetras.dejarSoloCaracteresDeseados("89.500.400","0123456789") --> "89500400"
        CifrasEnLetras.dejarSoloCaracteresDeseados("ABC-000-123-X-456","0123456789") --> "000123456"
    */
    dejarSoloCaracteresDeseados(texto, caracteresDeseados) {
      var indice = 0;
      var resultado = [];
      for (var indice = 0; indice < texto.length; ++indice) {
        var caracter = texto.charAt(indice);
        if (caracteresDeseados.indexOf(caracter) >= 0) resultado.push(caracter);
      }
      return resultado.join("");
    },
   
    /*
     Cuenta el número de repeticiones en el texto de los caracteres indicados
     Ejemplo: CifrasEnLetras.numeroRepeticiones("89.500.400","0") --> 4
    */
    numeroRepeticiones(texto, caracteres) {
      var resultado = 0;
      for (var indice=0; indice<texto.length; ++indice) {
        var caracter = texto.charAt(indice);
        if (caracteres.indexOf(caracter) >= 0) resultado++;
      }
      return resultado;
    },
   
    /*
      Función auxiliar de "convertirNumeroEnLetras"
      para procesar por separado la parte entera y la parte decimal
    */
    procesarEnLetras(cifras, palabraSingular, palabraPlural, esFemenina) {
      if(true){// (CifrasEnLetras) {
        // Género
        var genero = "neutro";
        if (esFemenina) genero = "femenino";
        else if (palabraSingular == "") genero = "masculino";
   
        // Letras
        var letras = this.convertirCifrasEnLetras(cifras, genero);
        if (this.empiezaPor(letras, this.PREFIJO_ERROR)) return letras;
   
        // Propiedades
        var esCero = letras == this.convertirUnidades(0, genero) || letras == "";
        var esUno = letras == this.convertirUnidades(1, genero);
        var esMultiploMillon = !esCero && this.acabaPor(cifras, "000000");
   
        // Palabra
        var palabra = "";
        if (!palabraSingular == "") {
          if (esUno || palabraPlural == "")
            palabra = palabraSingular;
          else
            palabra = palabraPlural;
        }
   
        // Resultado
        var resultado = [];
        resultado.push(letras);
        if (palabra != "") {
          resultado.push (esMultiploMillon? " de ": " ");
          resultado.push(palabra);
        }
        return resultado.join("");
      }
    },
   
    reemplazar(texto, buscado, reemplazo) {
      return texto.split(buscado).join(reemplazo);
      //return texto.replace(new RegExp(buscado, "g"), reemplazo);
    },
   
    recortar(texto) {
      return texto.replace(/^\s*|\s*$/g,"");
    },
   
    signo(numero){
      if (numero > 0) return 1;
      else if (numero < 0) return -1;
      else return 0;
    },
   
    repetir(texto, veces) {
      return new Array(isNaN(veces)? 1: veces+1).join(texto);
    },
   
    empiezaPor(texto, prefijo) {
      //return texto.match("^"+prefijo) == prefijo;
      return texto.substr(0, prefijo.length) == prefijo;
    },
   
    acabaPor(texto, sufijo) {
      //return texto.match(sufijo+"$") == sufijo;
      return texto.substr(texto.length-sufijo.length) == sufijo;
    },
   
  }; // CifrasEnLetras
   
  module.exports=CifrasEnLetras;