import Vue from 'vue';
import VueRouter from 'vue-router';
//#region Old data
import notificaciones  from '../components/notificaciones/index'
import estudiosprevioIndex  from '../components/estudiosprevio/index'
import estudiosprevioCreate from '../components/estudiosprevio/create'
import folder from '../components/estudiosprevio/import'
import estudiosprevioUpdate from '../components/estudiosprevio/update'
//Comite evaluador

//INFORMES
import contratos from '../components/informes/contratos'
import informeOrdenesPago from '../components/informes/contratosordenespago'
//INFORMES

import ListasChequeoIndex from '../components/listachequeo/index'
import ListasChequeoCreate from '../components/listachequeo/Create'

//Tipos de contratacion
import CargosIndex  from '../components/cargos/Index'
import CargosForm  from '../components/cargos/Create'


import PlanAnualAdquisicionIndex  from '../components/plananualadquisicion/Index'
import PlanAnualAdquisicionForm  from '../components/plananualadquisicion/Create'


import RecursosHumanosIndex  from '../components/recursoshumanos/Index'
import RecursosHumanosForm from '../components/recursoshumanos/Create'


import contratosTipos  from '../components/contratacion/index'
import oferenteCreate  from '../components/oferentes/create'
import ComiteEvaluadorCreate  from '../components/ComiteEvaluador/create'
import proponenteCreate  from '../components/proponentes/create'

import contratacionDirecta       from '../components/directa/form'
import devolucionesIndex from '../components/devoluciones/index'
//Tipos de contratacion


import Users from '../components/user/Users'
import UsersEdit from '../components/user/UsersEdit'
import UsersNew from '../components/user/UsersNew'
import password from '../components/user/changepass';
import uservalidate from '../components/user/validate'

import start from '../components/pages/start'
import logout from '../components/pages/logout'
import noAutorizate from '../components/pages/noAutorizate'
import permisos from '../components/user/permisions'
import PageNotFound from '../components/pages/error404'
//#endregion

//#region new data
import revision                 from '../components/revisiontramite/index'
import cdpasignar               from '../components/cdp/index'
import supervision              from '../components/supervision/index'
import informefinal             from '../components/informefinal/index'
import cuentassoportes          from '../components/cuentassoportes/index'
import detallerevision          from '../components/revisiontramite/detalle'
import detallesupervision       from '../components/supervision/detalle'
import pagosejecutadosIndex     from '../components/pagosejecutados/index'
import pagosejecutadosDetail    from '../components/pagosejecutados/detail'


//PAGOS
import ordenesPagoCreate   from '../components/pagosordenpago/create'
import ordenesPagoindex   from '../components/pagosordenpago/index'

import registroContableCreate   from '../components/pagosregistrocontable/create'
import registroContableindex   from '../components/pagosregistrocontable/index'

import registroPresupuestalCreate   from '../components/pagosregistropresupuestal/create'
import registroPresupuestalindex   from '../components/pagosregistropresupuestal/index'

import administracionCreate   from '../components/pagosadministracion/create'
import administracionindex   from '../components/pagosadministracion/index'

import gerenciaFirmaCreate   from '../components/pagosgerenciafirma/create'
import gerenciaFirmaindex   from '../components/pagosgerenciafirma/index'

import tesoreriaPagosCreate   from '../components/pagostesoreria/create'
import tesoreriaPagosindex   from '../components/pagostesoreria/index'


//PAGOS


//#endregion

//#region contratacion
import asignarCDP               from '../components/cdp/asignar'
import asignarProtocolarizacion from '../components/protocolarizacion/form'
import minuta                   from '../components/protocolarizacion/index'
import cobertura                from '../components/cobertura/index'
import asignarCobertura         from '../components/cobertura/form'
import actaInicio               from '../components/actasdeinicio/index'
import asignarActaInicio        from '../components/actasdeinicio/form'
import actaSuspension           from '../components/actasdesuspension/index'
import asignarActaSuspension    from '../components/actasdesuspension/form'
import actaReinicio           from '../components/actasdereinicio/index'
import asignarActaReinicio    from '../components/actasdereinicio/form'
import otrossi           from '../components/otrossi/index'
import asignarOtrosSi    from '../components/otrossi/form'



import liquidacionIndex             from '../components/liquidar/index'
import liquidacionTipo              from '../components/liquidar/tipoliquidacion'
import liquidacionRevisionInicial   from '../components/liquidar/revisionInicial'
import observacionpostcontractual   from '../components/liquidar/observacionpostcontractual'
import firmagerenciaactaliquidacion from '../components/liquidar/firmagerenciaactaliquidacion'

import contratoDetalle  from '../components/contratacion/detalle'

//#endregion
const prefix = '/dashboard';
Vue.use(VueRouter);
const router = new VueRouter({
    mode: 'history',
    routes: [
        //#region  old routes

        {
            path: prefix + '/vinculacion/create',
            name: 'CargoCreate',
            component: CargosForm,
            meta: {
                auth: true,
                title:'Crear tipo de vinculacion',
            },
        },
        {
            path: prefix + '/notificaciones',
            name: 'notificaciones',
            component: notificaciones,
            meta: {
                title:'Notificaciones',
            },
        },
        {
            path: prefix + '/vinculacion',
            name: 'CargoIndex',
            component: CargosIndex,
            meta: {
                auth: true,
                title:'Listado de vinculaciones',
            }
        },

        //PAGOS
        {
            path: prefix + '/ordenpago/create/:id_revisiones_tramites',
            name: 'ordenesPagoCreate',
            component: ordenesPagoCreate,
            meta: {
                auth: true,
                title:'Elaboracion de orden de pago',
            },
        },
        {
            path: prefix + '/ordenpago',
            name: 'ordenesPagoindex',
            component: ordenesPagoindex,
            meta: {
                auth: true,
                title:'Listado de ordernes de pago',
            }
        },
        {
            path: prefix + '/registrocontable/create/:id_ordenes_de_pago',
            name: 'registroContableCreate',
            component: registroContableCreate,
            meta: {
                auth: true,
                title:'Crear registro contalble',
            },
        },
        {
            path: prefix + '/registrocontable',
            name: 'registroContableindex',
            component: registroContableindex,
            meta: {
                auth: true,
                title:'Listado de registros contables',
            }
        },
        {
            path: prefix + '/registropresupuestal/create/:id_registro_contable',
            name: 'registroPresupuestalCreate',
            component: registroPresupuestalCreate,
            meta: {
                auth: true,
                title:'Asignar registro presupuestal',
            },
        },
        {
            path: prefix + '/registropresupuestal',
            name: 'registroPresupuestalindex',
            component: registroPresupuestalindex,
            meta: {
                auth: true,
                title:'Listado de registros presupuestales',
            }
        },
        {
            path: prefix + '/firmaadministracion/create/:registro_presupuestal_id',
            name: 'administracionCreate',
            component: administracionCreate,
            meta: {
                auth: true,
                title:'Firma profesional universitario administracion',
            },
        },
        {
            path: prefix + '/firmaadministracion',
            name: 'administracionindex',
            component: administracionindex,
            meta: {
                auth: true,
                title:'Listado de firmas profesional universitario administracion',
            }
        },
        {
            path: prefix + '/firmagerencia/create/:contratatos_pagos_firma_administracion_id',
            name: 'gerenciaFirmaCreate',
            component: gerenciaFirmaCreate,
            meta: {
                auth: true,
                title:'Registrar firma de gerencia',
            },
        },
        {
            path: prefix + '/firmagerencia',
            name: 'gerenciaFirmaindex',
            component: gerenciaFirmaindex,
            meta: {
                auth: true,
                title:'Listado de firmas de gerencia',
            }
        },
        {
            path: prefix + '/registrotesoreria/create/:contratatos_pagos_firma_gerencia_id',
            name: 'tesoreriaPagosCreate',
            component: tesoreriaPagosCreate,
            meta: {
                auth: true,
                title:'Crear registro de tesoreria',
            },
        },
        {
            path: prefix + '/registrotesoreria',
            name: 'tesoreriaPagosindex',
            component: tesoreriaPagosindex,
            meta: {
                auth: true,
                title:'Listado de registros de tesoreria',
            }
        },
        //PAGOS
        {
            path: prefix + '/vinculacion/update/:id',
            name: 'CargoUpdate',
            component: CargosForm,
            meta: {
                auth: true,
                title:'Listado de vinculaciones',
            }
        },

        {
            path: prefix + '/plananualadquisicion/create',
            name: 'PlanAnualAdquisicionCreate',
            component: PlanAnualAdquisicionForm,
            meta: {
                auth: true,
                title:'Agregar plan anual de adquisición',
            },
        },
        {
            path: prefix + '/plananualadquisicion',
            name: 'PlanAnualAdquisicionIndex',
            component: PlanAnualAdquisicionIndex,
            meta: {
                auth: true,
                title:'Listado de  plan anual de adquisición',
            }
        },
        {
            path: prefix + '/plananualadquisicion/update/:id',
            name: 'PlanAnualAdquisicionUpdate',
            component: PlanAnualAdquisicionForm,
            meta: {
                auth: true,
                title:'Actualizar plan anual de adquisición',
            }
        },


        {
            path: prefix + '/contratacion/cobertura/',
            name: 'cobertura',
            component: cobertura,
            meta: {
                auth: true,
                title:'Cobertura',
                roles:['admin']
            }
        },
        {
            path: prefix + '/contratacion/cobertura/:contrato_id',
            name: 'asignarCobertura',
            component: asignarCobertura,
            meta: {
                auth: true,
                title:'Asignar cobertura a contrato'
            }
        },
        {
            path: prefix + '/contrato/:contrato_id',
            name: 'contratoDetalle',
            component: contratoDetalle,
            meta: {
                auth: true,
                title:'Contrato'
            }
        },
        {
            path: prefix + '/contratacion/acta-de-inicio/',
            name: 'acta-de-inicio',
            component: actaInicio,
            meta: {
                auth: true,
                title:'Acta de inicio',
                roles:['admin']
            }
        },
        {
            path: prefix + '/contratacion/acta-de-inicio/:contrato_id',
            name: 'asignaractainicio',
            component: asignarActaInicio,
            meta: {
                auth: true,
                title:'Asignar Acta de inicio a contrato'
            }
        },

        {
            path: prefix + '/contratacion/acta-de-suspension/',
            name: 'acta-de-suspension',
            component: actaSuspension,
            meta: {
                auth: true,
                title:'Acta de suspensión',
                roles:['admin']
            }
        },
        {
            path: prefix + '/contratacion/crear/acta-de-suspension',
            name: 'asignaractasuspension',
            component: asignarActaSuspension,
            meta: {
                auth: true,
                title:'Asignar Acta de suspension'
            }
        },
        {
            path: prefix + '/contratacion/acta-de-reinicio/',
            name: 'acta-de-reinicio',
            component: actaReinicio,
            meta: {
                auth: true,
                title:'Acta de reinicio',
                roles:['admin']
            }
        },
        {
            path: prefix + '/contratacion/crear/acta-de-reinicio',
            name: 'asignaractareinicio',
            component: asignarActaReinicio,
            meta: {
                auth: true,
                title:'Asignar Acta de reinicio'
            }
        },

        {
            path: prefix + '/contratacion/otros-si/',
            name: 'otros-si',
            component: otrossi,
            meta: {
                auth: true,
                title:'Otros si',
                roles:['admin']
            }
        },
        {
            path: prefix + '/contratacion/crear/otros-si',
            name: 'asignarOtrosSi',
            component: asignarOtrosSi,
            meta: {
                auth: true,
                title:'Asignar otros si'
            }
        },


        {
            path: prefix + '/recursohumano/create',
            name: 'RecursoHumanoCreate',
            component: RecursosHumanosForm,
            meta: {
                auth: true,
                title:'Nuevo',
            },
        },
        {
            path: prefix + '/recursohumano',
            name: 'RecursoHumanoIndex',
            component: RecursosHumanosIndex,
            meta: {
                auth: true,
                title:'Listar',
            }
        },
        {
            path: prefix + '/recursohumano/update/:id',
            name: 'RecursoHumanoUpdate',
            component: RecursosHumanosForm,
            meta: {
                auth: true,
                title:'Actualizar',
            }
        },
        {
            path: prefix + '/contratacion/directa/:id',
            name: 'contratacionDirecta',
            component: contratacionDirecta,
            meta: {
                auth: true,
                title:'Contratacion directa',
                roles:['admin','supervisor','observador']
            }
        },
        {
            path: prefix + '/CDP/asignar/:estudios_previos_id',
            name: 'asignarCDP',
            component: asignarCDP,
            meta: {
                title:'Asignacion CDP'
            }
        },
        {
            path: prefix + '/contratacion/protocolarizacion/',
            name: 'minuta',
            component: minuta,
            meta: {
                auth: true,
                title:'Protocolarización',
                roles:['admin']
            }
        },
        {
            path: prefix + '/contratacion/protocolarizacion/:cdp_id',
            name: 'asignarProtocolarizacion',
            component: asignarProtocolarizacion,
            meta: {
                auth: true,
                title:'Asginar contrato'
            }
        },

        {
            path: prefix + '/folder/:id',
            name: 'folder',
            component: folder,
            meta: {
                auth: true,
                title:'Archivos del estudio y contrato',
                roles:['admin','supervisor','observador']
            }
        },
        {
            path: prefix + '/archivos/:id',
            name: 'archivos',
            component: folder,
            meta: {
                auth: true,
                title:'Archivos del estudio y contrato',
                roles:['admin','supervisor','observador']
            }
        },
        {
            path: prefix + '/contratos/:name',
            name: 'contratosTipos',
            component: contratosTipos,
            meta: {
                auth: true,
                title:'Contratos',
                roles:['admin','supervisor','observador']
            }
        },
        {
            path: prefix + '/estudiosprevios/index',
            name: 'ListasChequeoIndex',
            component: ListasChequeoIndex,
            meta: {
                auth: true,
                title:'Listas de chequeo',
                roles:['admin','supervisor','observador']
            }
        },
        {
            path: prefix + '/listachequeo/index',
            name: 'ListasChequeoCreate',
            component: ListasChequeoCreate,
            meta: {
                auth: true,
                title:'Listas de chequeo',
                roles:['admin','supervisor','observador']
            }
        },
        {
            path: prefix + '/estudiosprevios/index',
            name: 'estudiosprevioIndex',
            component: estudiosprevioIndex,
            meta: {
                auth: true,
                title:'Estudios previos',
                roles:['admin','supervisor','observador']
            }
        },
        {
            path: prefix + '/estudiosprevio/:id',
            name: 'estudiosprevioUpdate',
            component: estudiosprevioUpdate,
            meta: {
                auth: true,
                title:'Actualizar estudios previos',
                roles:['admin','supervisor','observador']
            }
        },
        {
            path: prefix + '/estudiosprevios/nuevo',
            name: 'estudiosprevioCreate',
            component: estudiosprevioCreate,
            meta: {
                auth: true,
                title:'Nuevo estudio previo',
                roles:['admin','supervisor','observador']
            }
        },

        //TIPOS DE CONTRATOS
        {
            path: prefix + '/contratos/oferente/:tipo_contrato/:id',
            name: 'oferenteCreate',
            component: oferenteCreate,
            meta: {
                auth: true,
                title:'Nuevo oferente',
                roles:['admin','supervisor','observador']
            }
        },
        {
            path: prefix + '/contratos/comiteevaluador/:tipo_contrato/:id',
            name: 'ComiteEvaluadorCreate',
            component: ComiteEvaluadorCreate,
            meta: {
                auth: true,
                title:'Nuevo miembro de comite evaluador',
                roles:['admin','supervisor','observador']
            }
        },
        {
            path: prefix + '/contratos/proponente/:tipo_contrato/:id',
            name: 'proponenteCreate',
            component: proponenteCreate,
            meta: {
                auth: true,
                title:'Nuevo Proponente',
                roles:['admin','supervisor','observador']
            }
        },
        //TIPOS DE CONTRATOS

        {
            path: prefix + '/start',
            name: 'start',
            component: start
        },
        {
            path: prefix + '/logout',
            name: 'logout',
            component: logout
        },
        {
            path: prefix + '/usuarios',
            name: 'users',
            component: Users,
            meta: {
                auth: true,
                title:'Usuarios',
                roles:['admin','supervisor','observador']
            }
        },
        {
            path: prefix + '/usuarios/edit/:id',
            name: 'usersedit',
            component: UsersEdit,
            meta: {
                auth: true,
                title:'Editar usuario',
                roles:['admin','supervisor','observador']
            }
        },
        {
            path: prefix + '/uservalidate',
            name: 'uservalidate',
            component: uservalidate,
            meta: {
                auth: true,
                title:'',
                roles:['admin','supervisor','observador']
            }
        },
        {
            path: prefix + '/usuarios/nuevo',
            name: 'usersnew',
            component: UsersNew,
            meta: {
                auth: true,
                title:'Nuevo usuario',
                roles:['admin','supervisor','observador']
            }
        },
        {
            path: prefix + '/usuarios/cambiarpass',
            name: 'cambiarpass',
            component: password,
            meta: {
                auth: true,
                title:'Cambiar contraseña',
                roles:['user','admin','supervisor','observador']
            }
        },
        {
            path: prefix + '/usuarios/permisos',
            name: 'permisos',
            component: permisos,
            meta: {
                auth: true,
                title:'Permisos de usuario',
                roles:['admin']
            }
        },
        {
            path: prefix + '/devoluciones',
            name: 'devolucionesIndex',
            component: devolucionesIndex,
            meta: {
                auth: true,
                title:'Devoluciones'
            }
        },
        
        { path: prefix + '/index', redirect:  { name: 'uservalidate' }},
        { path: prefix, redirect:  { name: 'estudiosprevioIndex' }},
        { path: "*", redirect:  { name: 'error404' }, component: PageNotFound, meta: { auth: false, title:'Sitio no encontrado' } },
        { path: prefix + "/accesodenegado", name: 'noAutorizate', component: noAutorizate, meta: { auth: false, title:'No autorizado'} },
        //#endregion

        {
            path: prefix + '/CDP/asignar',
            name: 'cdpasignar',
            component: cdpasignar,
            meta: {
                auth: true,
                title:'Asignar CDP',
                roles:['admin']
            }
        },
        {
            path: prefix + '/contratacion/supervision',
            name: 'supervision',
            component: supervision,
            meta: {
                auth: true,
                title:'Informe de Supervisión',
                roles:['admin']
            }
        },
        {
            path: prefix + '/contratacion/informe-supervision/:contrato_id',
            name: 'detallesupervision',
            component: detallesupervision,
            meta: {
                auth: true,
                title:'Detalle de Supervisión',
                roles:['admin']
            }
        },
        {
            path: prefix + '/contratacion/revision',
            name: 'revision',
            component: revision,
            meta: {
                auth: true,
                title:'Presentación de la cuenta y demás soportes - Revisión y tramite',
                roles:['admin']
            }
        },
        {
            path: prefix + '/contratacion/revision/:contrato_id',
            name: 'detallerevision',
            component: detallerevision,
            meta: {
                auth: true,
                title:'Detalle de la presentación de la cuenta y demás soportes - Revisión y tramite',
                roles:['admin']
            }
        },
        {
            path: prefix + '/contratacion/cuentasysoportes',
            name: 'cuentassoportes',
            component: cuentassoportes,
            meta: {
                auth: true,
                title:'Presentación de la cuenta y demás soportes',
                roles:['admin']
            }
        },
        {
            path: prefix + '/contratacion/pagosejecutados',
            name: 'pagosejecutados',
            component: pagosejecutadosIndex,
            meta: {
                auth: true,
                title:'Pagos ejecutados',
                roles:['admin']
            }
        },
        {
            path: prefix + '/contratacion/pagosejecutados/:id_contrato',
            name: 'pagosejecutadosDetail',
            component: pagosejecutadosDetail,
            meta: {
                auth: true,
                title:'Pagos ejecutados',
                roles:['admin']
            }
        },
        {
            path: prefix + '/contratacion/informefinal',
            name: 'informefinal',
            component: informefinal,
            meta: {
                auth: true,
                title:'Informe final',
                roles:['admin']
            }
        },
        {
            path: prefix + '/contratos',
            name: 'contratos',
            component: contratos,
            meta: {
                title:'Contratos',
                roles:['admin']
            }
        },
        {
            path: prefix + '/informes/ordenespago',
            name: 'informeOrdenesPago',
            component: informeOrdenesPago,
            meta: {
                title:'Informes de ordenes de pago'
            }
        },
        {
            path: prefix + '/liquidacion',
            name: 'liquidacionIndex',
            component: liquidacionIndex,
            meta: {
                title:'Buscar contratos a liquidar'
            }
        },
        {
            path: prefix + '/liquidacion/tipo/:contrato_id',
            name: 'liquidacionTipo',
            component: liquidacionTipo,
            meta: {
                title:'Tipo de contrato a liquidar'
            }
        },
        {
            path: prefix + '/liquidacion/revision-inicial/:contrato_liquidacion_id',
            name: 'liquidacionRevisionInicial',
            component: liquidacionRevisionInicial,
            meta: {
                title:'Revisión inicial'
            }
        },
        {
            path: prefix + '/liquidacion/firma-gerencia/:contrato_liquidacion_revision_inicial_id',
            name: 'firmagerenciaactaliquidacion',
            component: firmagerenciaactaliquidacion,
            meta: {
                title:'Revisión inicial'
            }
        },
        {
            path: prefix + '/liquidacion/observacionespostcontractuales/:contrato_id',
            name: 'observacionpostcontractual',
            component: observacionpostcontractual,
            meta: {
                title:'Observaciones post-contractuales'
            }
        },
    ]
});
router.beforeEach((to, from, next) =>
{
    const nearestWithTitle = to.matched.slice().reverse().find(r => r.meta && r.meta.title);
    const nearestWithMeta = to.matched.slice().reverse().find(r => r.meta && r.meta.metaTags);
    const previousNearestWithMeta = from.matched.slice().reverse().find(r => r.meta && r.meta.metaTags);
    if(nearestWithTitle) document.title = nearestWithTitle.meta.title;
    Array.from(document.querySelectorAll('[data-vue-router-controlled]')).map(el => el.parentNode.removeChild(el));
    if(!nearestWithMeta) return next();
    nearestWithMeta.meta.metaTags.map(tagDef =>
    {
        const tag = document.createElement('meta');
        Object.keys(tagDef).forEach(key => {
            tag.setAttribute(key, tagDef[key]);
        });
        tag.setAttribute('data-vue-router-controlled', '');
        return tag;
    })
    .forEach(tag => document.head.appendChild(tag));
    next();
});
export default router;
