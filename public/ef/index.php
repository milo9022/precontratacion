<?php
if(!(isset($_GET['asd']) || isset($_GET['dir']))){header("HTTP/1.0 404 Not Found");exit;}
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=2">
		<title>elFinder 2.1.x source version with PHP connector</title>
		<style>
			#data_div{heigth:100px}
		</style>
		<script data-main="./main.default.js" src="//cdnjs.cloudflare.com/ajax/libs/require.js/2.3.6/require.min.js"></script>
		<script>
			define('elFinderConfig', {
				defaultOpts : {
					<?php
					if(isset($_GET['asd']))
					{
						?>url : 'php/connector.minimal.php?asd=<?=$_GET['asd'];?>',<?php
					}
					else
					{
						?>url : 'php/connector.minimal.php?dir=<?=$_GET['dir'];?>',<?php
					}
					?>
					commandsOptions : {
						edit : {
							extraOptions : {
								creativeCloudApiKey : '',
								managerUrl : ''
							}
						},
						quicklook : {
							// to enable CAD-Files and 3D-Models preview with sharecad.org
							sharecadMimes : ['image/vnd.dwg', 'image/vnd.dxf', 'model/vnd.dwf', 'application/vnd.hp-hpgl', 'application/plt', 'application/step', 'model/iges', 'application/vnd.ms-pki.stl', 'application/sat', 'image/cgm', 'application/x-msmetafile'],
							googleDocsMimes : ['application/pdf', 'image/tiff', 'application/vnd.ms-office', 'application/msword', 'application/vnd.ms-word', 'application/vnd.ms-excel', 'application/vnd.ms-powerpoint', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', 'application/vnd.openxmlformats-officedocument.presentationml.presentation', 'application/postscript', 'application/rtf'],
							officeOnlineMimes : ['application/vnd.ms-office', 'application/msword', 'application/vnd.ms-word', 'application/vnd.ms-excel', 'application/vnd.ms-powerpoint', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', 'application/vnd.openxmlformats-officedocument.presentationml.presentation', 'application/vnd.oasis.opendocument.text', 'application/vnd.oasis.opendocument.spreadsheet', 'application/vnd.oasis.opendocument.presentation']
						}
					},
					bootCallback : function(fm, extraObj)
					{
						var title = document.title;
						fm.bind('init', function() {});
						fm.bind('open', function()
						{
							var path = '',cwd  = fm.cwd();
							if (cwd) {path = fm.path(cwd.hash) || null;}
							document.title = path? path + ':' + title : title;
						})
						.bind('destroy', function() {document.title = title;});
					}
				},
				managers : {
					'elfinder': {}
				}
			});
		</script>
	</head>
	<body>

		<!-- Element where elFinder will be created (REQUIRED) -->
		<div id="data_div">
			<div id="elfinder"></div>
		</div>

	</body>
</html>
