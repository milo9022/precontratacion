<?php

namespace App\Http\Controllers;
use App\Models\TblContratatosPagosRegistroContable;
use App\Models\TblContratatosPagosOrden;
use App\Models\TblContratatosPagosRegistroPresupuestal;
use App\Http\ControllersTblMensajesAlertController;
use Illuminate\Http\Request;

class TblContratatosPagosRegistroContableController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }
    public function indexStatus($status)
    {
        $data=[];
        if((trim($status)=='all'))
        {
            $data = TblContratatosPagosRegistroContable::
            whereNotNull('fecha_egreso')->
            orderBy('id');
        }
        else{
            $data = TblContratatosPagosRegistroContable::
            whereNotNull('fecha_egreso')->
            where('estado','=',$status);

        }
        $data = $data->
        with('tbl_contratatos_pagos_orden.tbl_revision_tramite.tbl_contratos')->
        get();
        return response()->json(['data'=>$data,'validate'=>true]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(is_null($request->id))
        {
            $data = new TblContratatosPagosRegistroContable();
        }
        else{
            $data = TblContratatosPagosRegistroContable::find($request->id);
        }
        $data->contratatos_pagos_orden_id   = $request->contratatos_pagos_orden_id;
        $data->numero_registro_contable     = $request->numero_registro_contable;
        $data->fecha_ingreso                = date('Y-m-d',strtotime($request->fecha_ingreso));
        $data->fecha_egreso                 = is_null($request->fecha_egreso)?null:date('Y-m-d',strtotime($request->fecha_egreso));
        $data->estado                       = 'tramite';
        $data->save();
        TblMensajesAlertController::add('Se ha aprobado un nuevo registro contable');
        $ordenPago                          = TblContratatosPagosOrden::find($request->contratatos_pagos_orden_id);
        $ordenPago->estado                  = is_null($request->fecha_egreso)?$ordenPago->estado:'aprobado';
        $ordenPago->save();
        TblMensajesAlertController::add('Se ha aprobado una nueva orden de pago');
        return response()->json(['data'=>$data,'validate'=>true]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $tramite_actual=TblContratatosPagosRegistroPresupuestal::where('contratatos_pagos_registro_contable_id','=',$id)->first();
        $data = TblContratatosPagosRegistroContable::
        with('tbl_contratatos_pagos_orden.tbl_revision_tramite.tbl_contratos')->
        find($id);
        $data->tramite_actual = $tramite_actual;
        return response()->json(['data'=>$data,'validate'=>true]);
    }
    public function showxOrdenPago($id)
    {
        $tramite_actual=TblContratatosPagosRegistroContable::where('contratatos_pagos_orden_id','=',$id)->first();
        $data = TblContratatosPagosOrden::
        with('tbl_revision_tramite')->
        where('id','=',$id)->
        firstOrFail();
        $data->tramite_actual = $tramite_actual;
        return response()->json(['data'=>$data,'validate'=>true]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
