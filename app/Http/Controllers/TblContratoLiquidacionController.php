<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\TblContrato;
use App\Models\TblContratoLiquidacion;

class TblContratoLiquidacionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return [123];
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = TblContratoLiquidacion::where('contrato_id','=',$request->contrato_id)->first();
        if(is_null($data))
        {
            $data = new TblContratoLiquidacion();
            $data->contrato_id          = $request->contrato_id;
            $data->tipo_liquidacion     = $request->tipo_liquidacion;
            $data->observacion          = $request->observacion;
            $data->save();
            $contrato = TblContrato::findOrFail($data->contrato_id);
            $contrato->contratos_estados_id = 4;
            $contrato->save();
            return $this->sendResponse($data);
        }
        else
        {
            return response()->json(['validate'=>false,'data'=>'Liquidación registrada'],422);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = TblContratoLiquidacion::where('id','=',$id)->firstOrFail();
        return $this->sendResponse($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
