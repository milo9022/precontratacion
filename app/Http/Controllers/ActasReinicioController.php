<?php

namespace App\Http\Controllers;

use App\Models\TblContrato;
use App\Models\TblActaDeReinicio;

use Carbon\Carbon;

use Illuminate\Http\Request;

class ActasReinicioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $data = TblActaDeReinicio::all();

      return response()->json(['data' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $data = [
        'user_id' => auth()->user()->id,
        'contrato_id' => $request->id_contrato,
        'fecha' => Carbon::parse($request->fecha)->format('Y-m-d'),
        'motivo' => $request->motivo,
      ];

      $result = TblActaDeReinicio::create($data);

      return response()->json($result);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function contratosValidos($ncontrato)
    {
      $data = TblContrato::where('numero', $ncontrato)->first();
      $count = TblContrato::where('numero', $ncontrato)->count();

      return response()->json(['data' => $data, 'count' => $count]);
    }
}
