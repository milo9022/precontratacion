<?php

namespace App\Http\Controllers;

use App\Models\TblContrato;
use App\Models\TblDevolucion;
use App\Models\TblTipoDevolucion;
use App\Models\TblRevisionTramite;
use App\Models\TblContratatosPagosOrden;
use App\Models\TblContratatosPagosFirmaGerencium;
use App\Models\TblContratatosPagosRegistroContable;
use App\Models\TblContratatosPagosRegistroPresupuestal;
use App\Models\TblContratatosPagosFirmaAdministracion;
use App\Http\Controllers\TblMensajesAlertController;

use Illuminate\Http\Request;

class DevolucionesController extends Controller
{
    protected $model;

    public function __construct(TblDevolucion $devolucion)
    {
    	$this->model = $devolucion;
	}
	public function verDevoluciones()
	{
		$data = $this->model::
		//orderBy('tipo_to_id')->
		orderBy('tipo_from_id')->
		with('tbl_tipo_devolucion_to')->
		with('tbl_tipo_devolucion_from')->
		//where('id','=',4)->
		get();
		foreach($data as $key => $temp)
		{
			$temp1=(object)['id_tercero'=>$temp->id_tercero,'numero'=>null,'estado'=>null];
			$id=$temp->tbl_tipo_devolucion_from->id;
			switch($id)
			{

				case 4:
					$res=TblRevisionTramite::
					with('tbl_contratos')->
					find($temp->id_tercero);
					if(!is_null($res)){
						$temp1=(object)['id_tercero'=>$temp->id_tercero,'numero'=>$res->tbl_contratos->numero,'estado'=>$res->estado];
					}
				break;
				case 5:
					$res=TblContratatosPagosRegistroContable::
					with('tbl_contratatos_pagos_orden.tbl_revision_tramite.tbl_contratos')->
					find($temp->id_tercero);
					if(!is_null($res)){
						$temp1=(object)['id_tercero'=>$temp->id_tercero,'numero'=>$res->tbl_contratatos_pagos_orden->tbl_revision_tramite->tbl_contratos->numero,'estado'=>$res->estado];
					}
				break;
				case 7:
					$res=TblContratatosPagosFirmaAdministracion::
					with('tbl_contratatos_pagos_registro_presupuestal.tbl_contratatos_pagos_registro_contable.tbl_contratatos_pagos_orden.tbl_revision_tramite.tbl_contratos')->
					find($temp->id_tercero);
					if(!is_null($res)){
						$temp1=(object)
						[
							'id_tercero'	=>	$temp->id_tercero,
							'numero'		=>	$res->tbl_contratatos_pagos_registro_presupuestal->tbl_contratatos_pagos_registro_contable->tbl_contratatos_pagos_orden->tbl_revision_tramite->tbl_contratos->numero,
							'estado'		=>	$res->estado
						];
					}
				break;
				case 8:
					$res=TblContratatosPagosFirmaGerencium::
					with('tbl_contratatos_pagos_firma_administracion.tbl_contratatos_pagos_registro_presupuestal.tbl_contratatos_pagos_registro_contable.tbl_contratatos_pagos_orden.tbl_revision_tramite.tbl_contratos')->
					find($temp->id_tercero);
					if(!is_null($res)){
						$temp1=(object)
						[
							'id_tercero'	=>	$temp->id_tercero,
							'numero'		=>	$res->tbl_contratatos_pagos_firma_administracion->tbl_contratatos_pagos_registro_presupuestal->tbl_contratatos_pagos_registro_contable->tbl_contratatos_pagos_orden->tbl_revision_tramite->tbl_contratos->numero,
							'estado'		=>	$res->estado
						];
					}
				break;
				case 9:
					$res=TblContrato::
					find($temp->id_tercero);
					if(!is_null($res))
					{
						$temp1=(object)
						[
							'id_tercero'	=>	$temp->id_tercero,
							'numero'		=>	$res->numero,
							'estado'		=>	null
						];
					}
				break;
				
			}
			$data[$key]->numero 		= $temp1->numero;
			$data[$key]->estado 		= $temp1->estado;
			$data[$key]->id_tercero 	= $temp1->id_tercero;
		}
		return $this->sendResponse($data);
	}
	private function changeStatus($tipo_to_id,$id_tercero)
	{
		$res=null;
		switch($tipo_to_id)
		{
			case 1:
			break;
			case 2:
			break;
			case 3:$res=TblContratatosPagosOrden::find($id_tercero);
			break;
			case 4:$res=TblContratatosPagosRegistroContable::find($id_tercero);
			break;
			case 5:$res=TblContratatosPagosRegistroPresupuestal::find($id_tercero);
			break;
			case 6:$res=TblContratatosPagosFirmaAdministracion::find($id_tercero);
			break;
			case 7:$res=TblContratatosPagosFirmaGerencium::find($id_tercero);
			break;
			case 8:
			break;
			case 9:$res=TblRevisionTramite::find($id_tercero);
			break;
		}
		if(!is_null($res))
		{	
			$res->estado='devuelto';$res->save();
		}
	}
    public function store(Request $request)
    {
		try
		{
			$create 					= $this->model;
			$create->fecha_ingreso	  	= $request->fecha_ingreso;
			$create->motivo_devolucion	= $request->motivo_devolucion;
			$create->fecha_egreso		= $request->fecha_egreso;
			$create->fecha_devolucion	= $request->fecha_devolucion;
			$create->tipo_from_id		= $request->tipo_from_id;
			$create->tipo_to_id			= $request->tipo_to_id;
			$create->id_tercero			= $request->id;
			$create->user_id			= auth()->id();
			$create->save();
			$this->changeStatus($create->tipo_to_id,$create->id_tercero);
			$tipo_to = TblTipoDevolucion::find($request->tipo_to_id);
			$tipo_from = TblTipoDevolucion::find($request->tipo_from_id);
			TblMensajesAlertController::add($tipo_to->nombre.' ha hecho una devolucion a '.$tipo_from->nombre);
    		return $this->sendResponse(null, 'Devolución creada');
    	} catch(\Throwable $th) {
    		return $this->sendError('Error', $th->getMessage(), 422);
    	}
    }
}
