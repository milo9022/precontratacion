<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\TblContrato;
use App\Models\TblRevisionTramite;
use App\Models\TblContratatosPagosTesorerium;
use App\Models\TblContratatosPagosFirmaGerencium;
use App\Http\ControllersTblMensajesAlertController;

class TblContratatosPagosTesoreriaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }
    public function indexStatus($status)
    {

        if((trim($status)=='all'))
        {
            $data = TblContratatosPagosTesorerium::orderBy('id');
        }
        else{
            $data = TblContratatosPagosTesorerium::
            where('estado','=',$status);
        }
        $data = $data->
        with('tbl_contratatos_pagos_firma_gerencia.tbl_contratatos_pagos_firma_administracion.tbl_contratatos_pagos_registro_presupuestal.tbl_contratatos_pagos_registro_contable.tbl_contratatos_pagos_orden.tbl_revision_tramite.tbl_contratos')->
        get();
        return response()->json(['data'=>$data,'validate'=>true]);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    private function validarLiquidacion($id)
    {
        $data = TblContratatosPagosTesorerium::
        with('tbl_contratatos_pagos_firma_gerencia.tbl_contratatos_pagos_firma_administracion.tbl_contratatos_pagos_registro_presupuestal.tbl_contratatos_pagos_registro_contable.tbl_contratatos_pagos_orden.tbl_revision_tramite.tbl_contratos')->
        find($id);//contrato_id
        $contrato=$data->tbl_contratatos_pagos_firma_gerencia->tbl_contratatos_pagos_firma_administracion->tbl_contratatos_pagos_registro_presupuestal->tbl_contratatos_pagos_registro_contable->tbl_contratatos_pagos_orden->tbl_revision_tramite->tbl_contratos;
        $saldo=$data->tbl_contratatos_pagos_firma_gerencia->tbl_contratatos_pagos_firma_administracion->tbl_contratatos_pagos_registro_presupuestal->tbl_contratatos_pagos_registro_contable->tbl_contratatos_pagos_orden->saldo_contrato;
        if($saldo==0 && $contrato->procede_liquidacion===1)
        {
            TblMensajesAlertController::add('El contrato número '.$contrato->numero.' procede a liquidar');
            $dataContrato = TblContrato::find($contrato->id);
            $dataContrato->contratos_estados_id=5;
            $dataContrato->save();
        }
    }
    public function store(Request $request)
    {
        $data = new TblContratatosPagosTesorerium();
        $data->fecha_cancelacion                    = (is_null($request->fecha_cancelacion))?         null:date('Y-m-d',strtotime($request->fecha_cancelacion)); 
        $data->fecha_ingreso                        = (is_null($request->fecha_ingreso))?             null:date('Y-m-d',strtotime($request->fecha_ingreso));
        //$data->fecha_egreso                         = (is_null($request->fecha_egreso))?              null:date('Y-m-d',strtotime($request->fecha_egreso));
        $data->fecha_envio_area_juridica            = (is_null($request->fecha_envio_area_juridica))? null:date('Y-m-d',strtotime($request->fecha_envio_area_juridica));
        $data->quien_recibe_area_juridica           = $request->quien_recibe_area_juridica;
        $data->contratatos_pagos_firma_gerencia_id  = $request->contratatos_pagos_firma_gerencia_id;
        $data->estado                               = 'aprobado';
        $data->save();
        $this->validarLiquidacion($data->id);
        TblMensajesAlertController::add('Se ha agregado un nuevo pago de tesorería');
        $gerencia = TblContratatosPagosFirmaGerencium::find($request->contratatos_pagos_firma_gerencia_id);
        $gerencia->estado = 'aprobado';
        $gerencia->save();
        TblMensajesAlertController::add('Se ha aprobado una nueva firma de gerencia');
        return response()->json(['data'=>$data,'validate'=>true]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try
        {
            $data = TblRevisionTramite::
            select(
                'tbl_contratos.id as contrato_id',
                'tbl_contratatos_pagos_tesoreria.id',
                'tbl_contratatos_pagos_orden.fecha_ingreso',
                'tbl_contratatos_pagos_orden.revisiones_tramites_id',
                'tbl_contratatos_pagos_orden.valor',
                'tbl_contratatos_pagos_orden.valor_contrato',
                'tbl_contratatos_pagos_orden.numero_orden_pago',
                'tbl_contratatos_pagos_orden.valor_ejecutado',
                'tbl_contratatos_pagos_orden.saldo_contrato',
                'tbl_contratatos_pagos_orden.valor_pagado',
                'tbl_contratatos_pagos_orden.porcentaje',
                'tbl_contratatos_pagos_orden.observaciones',
                'tbl_contratatos_pagos_orden.fecha_egreso',
                'tbl_contratatos_pagos_orden.estado'
            )->
            join('tbl_contratos','tbl_revisiones_tramites.contrato_id', '=', 'tbl_contratos.id')-> 
            join('tbl_contratatos_pagos_orden','tbl_contratatos_pagos_orden.revisiones_tramites_id', '=', 'tbl_revisiones_tramites.id')-> 
            join('tbl_contratatos_pagos_registro_contable','tbl_contratatos_pagos_registro_contable.contratatos_pagos_orden_id', '=', 'tbl_contratatos_pagos_orden.id')-> 
            join('tbl_contratatos_pagos_registro_presupuestal','tbl_contratatos_pagos_registro_presupuestal.contratatos_pagos_registro_contable_id', '=', 'tbl_contratatos_pagos_registro_contable.id')-> 
            join('tbl_contratatos_pagos_firma_administracion','tbl_contratatos_pagos_firma_administracion.contratatos_pagos_registro_presupuestal_id', '=', 'tbl_contratatos_pagos_registro_presupuestal.id')-> 
            join('tbl_contratatos_pagos_firma_gerencia','tbl_contratatos_pagos_firma_gerencia.contratatos_pagos_firma_administracion_id', '=', 'tbl_contratatos_pagos_firma_administracion.id')-> 
            join('tbl_contratatos_pagos_tesoreria','tbl_contratatos_pagos_tesoreria.contratatos_pagos_firma_gerencia_id', '=', 'tbl_contratatos_pagos_firma_gerencia.id')->
            where('tbl_contratos.id','=',$id)->
            get();
            return response()->json(['data'=>$data,'validate'=>true]);
        } catch (\Throwable $th) {
            return response()->json(['data'=>$th->getMessage(),'validate'=>false],422);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
