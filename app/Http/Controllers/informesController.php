<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\TblContrato;
use App\Models\TblContratatosPagosOrden;
use App\Models\TblContratatosPagosFirmaGerencium;

class informesController extends Controller
{
    public function contratosfinalizados(Request $request)
    {
        $data = TblContrato::
        whereDate('vigencia_hasta','<',date('Y-m-d'))->
        with('tbl_contratos_estados')->
        with('tbl_contratos_cuotas')->
        with('tbl_contratistas')->
        with('tbl_contratistas')->
        with('tbl_recursos_humanos_supervisor')->
        with('tbl_recursos_humanos_apoyo')->
        get();
        foreach($data as $key=>$temp)
        {
            $res=TblContratatosPagosFirmaGerencium::
            select(
                'tbl_contratatos_pagos_orden.numero_orden_pago',
                'tbl_contratatos_pagos_orden.valor',                       
            )->
            where('tbl_revisiones_tramites.contrato_id','=',$temp->id)->
            join('tbl_contratatos_pagos_tesoreria','tbl_contratatos_pagos_firma_gerencia.id','=','tbl_contratatos_pagos_tesoreria.contratatos_pagos_firma_gerencia_id')-> 
            join('tbl_contratatos_pagos_firma_administracion','tbl_contratatos_pagos_firma_administracion.id','=','tbl_contratatos_pagos_firma_gerencia.contratatos_pagos_firma_administracion_id')-> 
            join('tbl_contratatos_pagos_registro_presupuestal','tbl_contratatos_pagos_firma_administracion.contratatos_pagos_registro_presupuestal_id','=','tbl_contratatos_pagos_registro_presupuestal.id')-> 
            join('tbl_contratatos_pagos_registro_contable','tbl_contratatos_pagos_registro_contable.id','=','tbl_contratatos_pagos_registro_presupuestal.contratatos_pagos_registro_contable_id')-> 
            join('tbl_contratatos_pagos_orden','tbl_contratatos_pagos_orden.id','=','tbl_contratatos_pagos_registro_contable.contratatos_pagos_orden_id')-> 
            join('tbl_revisiones_tramites','tbl_contratatos_pagos_orden.revisiones_tramites_id','=','tbl_revisiones_tramites.id')-> 
            get();
            $data[$key]->ordenesPago=$res;
        }
        return $this->sendResponse
        (
            $data
        );
    }
    public function contratosOrdenesPago(Request $request)
    {
        $data = TblContratatosPagosOrden::
        with('tbl_revision_tramite.tbl_contratos')->
        with('tbl_revision_tramite.tbl_contratos.tbl_contratos_estados')->
        with('tbl_revision_tramite.tbl_contratos.tbl_contratos_cuotas')->
        with('tbl_revision_tramite.tbl_contratos.tbl_contratistas')->
        with('tbl_revision_tramite.tbl_contratos.tbl_contratistas')->
        with('tbl_revision_tramite.tbl_contratos.tbl_recursos_humanos_supervisor')->
        with('tbl_revision_tramite.tbl_contratos.tbl_recursos_humanos_apoyo')->
        get();
        return $this->sendResponse(
        [
            'data'=>$data,
            'paginas'=>1
        ]);
    }
}
