<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\TblRevisionTramite;
use App\Models\TblContratatosPagosOrden;
use App\Models\TblContratatosPagosRegistroContable;
use App\Http\ControllersTblMensajesAlertController;
use DB;

class TblContratosPagosOrden extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = TblContratatosPagosOrden::all();
        return response()->json(['data'=>$data,'validate'=>true]);
    }
    public function indexStatus($status)
    {
        if($status=='all')
        {
            $registroCotable=TblContratatosPagosRegistroContable::
            groupBy('contratatos_pagos_orden_id')->
            pluck('contratatos_pagos_orden_id');
            $data = TblContratatosPagosOrden::
            with('tbl_revision_tramite.tbl_contratos')->
            whereNotNull('tbl_contratatos_pagos_orden.fecha_egreso')->
            get();
        }
        else{
            
            $registroCotable=TblContratatosPagosRegistroContable::
            groupBy('contratatos_pagos_orden_id')->
            pluck('contratatos_pagos_orden_id');
            $data = TblContratatosPagosOrden::
            with('tbl_revision_tramite.tbl_contratos')->
            where('estado','=',$status)->
            whereNotNull('tbl_contratatos_pagos_orden.fecha_egreso')->
            whereNotIn('id',$registroCotable)->
            get();
        }
        return response()->json(['data'=>$data,'validate'=>true]);
    }
    

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(is_null($request->id))
        {
            $data = new TblContratatosPagosOrden();
        }
        else{
            $data = TblContratatosPagosOrden::findOrFail($request->id);
        }
        if(is_null(TblContratatosPagosOrden::where('numero_orden_pago','=',trim($request->numero_orden_pago))->first()))
        {
            $data->revisiones_tramites_id = $request->revisiones_tramites_id;
            $data->numero_orden_pago      = trim($request->numero_orden_pago);
            $data->valor                  = (is_null($request->valor))?null:str_replace([',','$'],['','',],$request->valor);
            $data->valor_contrato         = (is_null($request->valor_contrato))?null:str_replace([',','$'],['','',],$request->valor_contrato);
            $data->valor_ejecutado        = (is_null($request->valor_ejecutado))?null:str_replace([',','$'],['','',],$request->valor_ejecutado);
            $data->valor_pagado           = (is_null($request->valor_pagado))?null:str_replace([',','$'],['','',],$request->valor_pagado);
            $data->saldo_contrato         = (is_null($request->saldo_contrato))?null:str_replace([',','$'],['','',],$request->saldo_contrato);
            $data->porcentaje             = $request->porcentaje;
            $data->observaciones          = trim($request->observaciones);
            $data->fecha_ingreso          = date('Y-m-d',strtotime($request->fecha_ingreso));
            $data->fecha_egreso           = is_null($request->fecha_egreso)?null:date('Y-m-d',strtotime($request->fecha_egreso));
            $data->estado                 = 'tramite';
            $data->save();
            TblMensajesAlertController::add('Se ha agregado una nueva orden de pago');
            $revisiones_tramites          = TblRevisionTramite::find($request->revisiones_tramites_id);
            $revisiones_tramites->estado  = is_null($data->fecha_egreso)?$data->estado:'aprobado';
            $revisiones_tramites->save();
            TblMensajesAlertController::add('Se ha aprobado un nuevo tramite de revisión');
            return $this->sendResponse($data, 'Nueva orden creada');
        }
        else{
            $data->revisiones_tramites_id = $request->revisiones_tramites_id;
            $data->numero_orden_pago      = trim($request->numero_orden_pago);
            $data->valor                  = (is_null($request->valor))?null:str_replace([',','$'],['','',],$request->valor);
            $data->valor_contrato         = (is_null($request->valor_contrato))?null:str_replace([',','$'],['','',],$request->valor_contrato);
            $data->valor_ejecutado        = (is_null($request->valor_ejecutado))?null:str_replace([',','$'],['','',],$request->valor_ejecutado);
            $data->valor_pagado           = (is_null($request->valor_pagado))?null:str_replace([',','$'],['','',],$request->valor_pagado);
            $data->saldo_contrato         = (is_null($request->saldo_contrato))?null:str_replace([',','$'],['','',],$request->saldo_contrato);
            $data->porcentaje             = $request->porcentaje;
            $data->observaciones          = trim($request->observaciones);
            $data->fecha_ingreso          = date('Y-m-d',strtotime($request->fecha_ingreso));
            $data->fecha_egreso           = is_null($request->fecha_egreso)?null:date('Y-m-d',strtotime($request->fecha_egreso));
            $data->estado                 = 'tramite';
            $data->save();
            TblMensajesAlertController::add('Se ha agregado una nueva orden de pago');
            $revisiones_tramites          = TblRevisionTramite::find($request->revisiones_tramites_id);
            $revisiones_tramites->estado  = is_null($data->fecha_egreso)?$data->estado:'aprobado';
            $revisiones_tramites->save();
            TblMensajesAlertController::add('Se ha aprobado un nuevo tramite de revisión');
            return $this->sendResponse($data, 'Nueva orden creada');
            return response()->json(['validate'=>false,'data'=>'número de orden de pago '.trim($request->numero_orden_pago).' repetido'], 402);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
	{
		try {
            $tramite_actual=TblContratatosPagosOrden::where('revisiones_tramites_id','=',$id)->first();
            $max=is_null($tramite_actual)?(TblContratatosPagosOrden::max('numero_orden_pago')+1):$tramite_actual->numero_orden_pago;
            $data = TblRevisionTramite::
			with('tbl_contratos')->
			where('id','=',$id)->
            firstOrFail();
			$data->ejecutado = $this->pagosRealizados($data->contrato_id);
			$data->pagado 	 = 0;
            $data->numero_sugerido=$max;
            $data->saldo 	 = 0;
            $data->tramite_actual=$tramite_actual;
			return $this->sendResponse($data, 'Presentacion actualizada');
		} catch(\Throwable $th) {
			return $this->sendError('Error', $th->getMessage(), 422);
		}
    }
    public function pagosRealizadosIndex($id)
    {
        try{
            $res = TblRevisionTramite::
			with('tbl_contratos')->
			where('id','=',$id)->
            firstOrFail();

            $data = TblContratatosPagosOrden::
            join('tbl_revisiones_tramites','tbl_contratatos_pagos_orden.revisiones_tramites_id', '=', 'tbl_revisiones_tramites.id')
            ->where('tbl_revisiones_tramites.contrato_id','=',$res->contrato_id)
            ->get();
			return $this->sendResponse($data, 'Listado de pagos');
        } 
        catch(\Throwable $th)
        {
			return $this->sendError('Error', $th->getMessage(), 422);
		}
    }
	private function pagosRealizados($id_contrato)
	{
		$data = TblRevisionTramite::
		select(DB::raw('COALESCE(SUM(tbl_contratatos_pagos_orden.valor),0) as total'))->
		join('tbl_contratos', 'tbl_revisiones_tramites.contrato_id', '=', 'tbl_contratos.id')-> 
        join('tbl_contratatos_pagos_orden', 'tbl_contratatos_pagos_orden.revisiones_tramites_id', '=', 'tbl_revisiones_tramites.id')->
        where('tbl_contratos.id','=',$id_contrato)->
		first();
		return ($data->total);
	}

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
