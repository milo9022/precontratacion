<?php

namespace App\Http\Controllers;

use App\Models\TblContratcionConvocatoriaPublica;
use Illuminate\Http\Request;

class TblContratcionConvocatoriaPublicaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\TblContratcionConvocatoriaPublica  $tblContratcionConvocatoriaPublica
     * @return \Illuminate\Http\Response
     */
    public function show(TblContratcionConvocatoriaPublica $tblContratcionConvocatoriaPublica)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\TblContratcionConvocatoriaPublica  $tblContratcionConvocatoriaPublica
     * @return \Illuminate\Http\Response
     */
    public function edit(TblContratcionConvocatoriaPublica $tblContratcionConvocatoriaPublica)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\TblContratcionConvocatoriaPublica  $tblContratcionConvocatoriaPublica
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TblContratcionConvocatoriaPublica $tblContratcionConvocatoriaPublica)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\TblContratcionConvocatoriaPublica  $tblContratcionConvocatoriaPublica
     * @return \Illuminate\Http\Response
     */
    public function destroy(TblContratcionConvocatoriaPublica $tblContratcionConvocatoriaPublica)
    {
        //
    }
}
