<?php

namespace App\Http\Controllers;

use App\Role;
use App\User;
use DB;
use Illuminate\Http\Request;
use App\Models\RecursoHumano;
use App\Models\TblDocumentoTipo;

class RecursosHumanosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function documentoTipos()
    {
        return response()->json(['data'=>TblDocumentoTipo::orderBy('nombre')->get(),'validate'=>true]);
    }
    public function index()
    {
        $data = RecursoHumano::
        select('*',DB::raw('UPPER(concat_ws(" ",nombre_primero,nombre_segundo,apellido_primero,apellido_segundo)) as nombre_completo'))->
        with('tbl_cargos')->
        with('tbl_documento_tipo')->
        orderBy('nombre_completo')->
        get();
        return response()->json(['data' => $data,'validate'=>true]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    private function newUser($request)
    {
        try {
            $roles = Role::where('id', 2)->first();
            $user                    = new User();
            
            $user->nombre_primero    = $request->nombre_primero;
            $user->nombre_segundo    = $request->nombre_segundo;
            $user->apellido_primero  = $request->apellido_primero;
            $user->apellido_segundo  = $request->apellido_segundo;
            $user->documento         = $request->documento;
            $user->activo            = $request->activo?1:0;
            $user->email             = $request->email;
            $user->direccion         = $request->direccion;
            $user->celular           = $request->celular;
            $user->password          = bcrypt($request->documento);
            $user->save();
            $user->roles()->attach($roles);
            return $user->id;
        }
        catch(\Exception $e)
        {
            return response()->json($e->getMessage(),422);
        }
    }
    public function store(Request $request)
    {
        $id_user = $this->newUser($request);
        $RecursoHumano = new RecursoHumano();
        $RecursoHumano->nombre_primero   = $request->nombre_primero;
        $RecursoHumano->nombre_segundo   = $request->nombre_segundo;
        $RecursoHumano->apellido_primero = $request->apellido_primero;
        $RecursoHumano->apellido_segundo = $request->apellido_segundo;
        $RecursoHumano->documento        = $request->documento;
        $RecursoHumano->direccion        = $request->direccion;
        $RecursoHumano->documento_tipo_id= $request->documento_tipo_id;
        $RecursoHumano->celular          = $request->celular;
        $RecursoHumano->activo           = $request->activo?1:0;
        $RecursoHumano->email            = $request->email;
        $RecursoHumano->cargo_id         = $request->cargo_id;
        $RecursoHumano->user_id          = $id_user;
        $RecursoHumano->Save();

        return response()->json(['validate'=>true, 'data'=>$RecursoHumano]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }
    public function searchDocument(Request $request)
    {
        $data = RecursoHumano::where('documento','=',$request->documento)->first();
        return response()->json(['data' => $data,'validate'=>true]);
    }
    

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
