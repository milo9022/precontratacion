<?php

namespace App\Http\Controllers;

use App\Models\EstudiosPreviosEstadoModel;
use Illuminate\Http\Request;

class EstudiosPreviosEstadoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return EstudiosPreviosEstadoModel::all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\EstudiosPreviosEstadoModel  $estudiospreviosEstadoModel
     * @return \Illuminate\Http\Response
     */
    public function show(EstudiosPreviosEstadoModel $estudiospreviosEstadoModel)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\EstudiosPreviosEstadoModel  $estudiospreviosEstadoModel
     * @return \Illuminate\Http\Response
     */
    public function edit(EstudiosPreviosEstadoModel $estudiospreviosEstadoModel)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\EstudiosPreviosEstadoModel  $estudiospreviosEstadoModel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, EstudiosPreviosEstadoModel $estudiospreviosEstadoModel)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\EstudiosPreviosEstadoModel  $estudiospreviosEstadoModel
     * @return \Illuminate\Http\Response
     */
    public function destroy(EstudiosPreviosEstadoModel $estudiospreviosEstadoModel)
    {
        //
    }
}
