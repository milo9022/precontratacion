<?php

namespace App\Http\Controllers;

use Carbon\Carbon;

use App\Models\TblContrato;
use App\Models\TblActaDeSuspension;

use Illuminate\Http\Request;

class ActasDeSuspensionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = TblActaDeSuspension::all();

        return response()->json(['data' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $data = [
        'user_id' => auth()->user()->id,
        'contrato_id' => $request->id_contrato,
        'fecha' => Carbon::parse($request->fecha)->format('Y-m-d'),
        'motivo' => $request->motivo,
        'tiempo' => $request->tiempo,
        'tipo_tiempo' => $request->tipo_tiempo,
      ];

      $result = TblActaDeSuspension::create($data);

      return response()->json($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function contratosValidos($ncontrato)
    {
      $data = TblContrato::where('numero', $ncontrato)->first();
      $count = TblContrato::where('numero', $ncontrato)->count();

      return response()->json(['data' => $data, 'count' => $count]);
    }
}
