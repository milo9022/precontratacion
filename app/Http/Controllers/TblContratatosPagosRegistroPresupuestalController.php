<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\TblContratatosPagosRegistroPresupuestal;
use App\Models\TblContratatosPagosFirmaAdministracion;
use App\Models\TblContratatosPagosRegistroContable;
use App\Http\ControllersTblMensajesAlertController;

class TblContratatosPagosRegistroPresupuestalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }
    public function indexStatus($status)
    {
        if((trim($status)=='all'))
        {
            $data = TblContratatosPagosRegistroPresupuestal::
            whereNotNull('fecha_egreso')->
            orderBy('id');
        }
        else{
            $data = TblContratatosPagosRegistroPresupuestal::
            whereNotNull('fecha_egreso')->
            where('estado','=',$status);

        }
        $data=$data->
        with('tbl_contratatos_pagos_registro_contable.tbl_contratatos_pagos_orden.tbl_revision_tramite.tbl_contratos')->
        get();
        return response()->json(['data'=>$data,'validate'=>true]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(is_null($request->id))
        {
            $data = new TblContratatosPagosRegistroPresupuestal();
        }
        else{
            $data = TblContratatosPagosRegistroPresupuestal::find($request->id);
        }
        $data->contratatos_pagos_registro_contable_id = $request->contratatos_pagos_registro_contable_id;
        $data->codigo_presupuestal                    = trim($request->codigo_presupuestal);
        $data->fecha_ingreso                          = date('Y-m-d',strtotime($request->fecha_ingreso));
        $data->fecha_egreso                           = is_null($request->fecha_egreso)?null:date('Y-m-d',strtotime($request->fecha_egreso));
        $data->estado                                 = 'tramite';
        $data->save();
        TblMensajesAlertController::add('Se ha agregado un nuevo registro presupuestal');
        $registroContable = TblContratatosPagosRegistroContable::find($request->contratatos_pagos_registro_contable_id);
        $registroContable->estado                     = is_null($data->fecha_egreso)?$registroContable->estado:'aprobado';
        $registroContable->save();
        TblMensajesAlertController::add('Se ha aprobado un nuevo registro contable');

        return response()->json(['data'=>$data,'validate'=>true]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $tramite_actual=TblContratatosPagosFirmaAdministracion::where('contratatos_pagos_registro_presupuestal_id','=',$id)->first();
        $data = TblContratatosPagosRegistroPresupuestal::
        with('tbl_contratatos_pagos_registro_contable.tbl_contratatos_pagos_orden.tbl_revision_tramite')->
        where('id','=',$id)->
        firstOrFail();
        $data->tramite_actual = $tramite_actual;
        return response()->json(['data'=>$data,'validate'=>true]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
