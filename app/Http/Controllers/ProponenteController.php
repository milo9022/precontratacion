<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\TblProponentes;
use App\Models\TblContratacionMenorCuantiaXProponente;
use App\Models\TblContratacionConvocatoriaPublicaXProponente;

class ProponenteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function SearchDocumento(Request $request)
    {
        $data = TblProponentes::where('documento','=',$request->documento)->first();
        return ['data'=>$data,'validate'=>true];
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->tipo_contrato!='menorcuantia' && $request->tipo_contrato!='convocatoriapublica')
        {
            return response(['validate'=>false,'msj'=>'Guardado oferente con éxito'], 402);
        }    
        try 
        {
            $data = TblProponentes::where('documento','=',$request->documento)->first();
            if(is_null($data))
            {
                $data = new TblProponentes();
            }
            else{
                $data = TblProponentes::find($data->id);
            }
            $data->nombre_primero   = $request->nombre_primero;
            $data->nombre_segundo   = $request->nombre_segundo;
            $data->apellido_primero = $request->apellido_primero;
            $data->apellido_segundo = $request->apellido_segundo;
            $data->documento        = $request->documento;
            $data->celular          = $request->celular;
            $data->email            = $request->email;
            $data->direccion        = $request->direccion;
            $data->save();
            switch($request->tipo_contrato)
            {
                case 'menorcuantia':
                    $ProponenteNuevo = new TblContratacionMenorCuantiaXProponente();
                    $ProponenteNuevo->proponente_id       = $data->id;
                    $ProponenteNuevo->estudios_previos_id = $request->id_menor_cuantia;
                    
                    $ProponenteNuevo->save();
                break;
                case 'convocatoriapublica':
                    $ProponenteNuevo = new TblContratacionConvocatoriaPublicaXProponente();
                    $ProponenteNuevo->proponente_id       = $data->id;
                    $ProponenteNuevo->estudios_previos_id = $request->id_menor_cuantia;
                    $ProponenteNuevo->save();
                break;
                
            }
            return ['validate'=>true,'msj'=>'Guardado oferente con éxito'];
            
        }
        catch (\Throwable $th)
        {
            return response(['validate'=>false,'msj'=>'Error','error'=>$th->getMessage()], 402);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
