<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\TblListaChequeo; 
use App\Models\TblListaChequeoDetalle; 
use App\Models\TblListaChequeoPlantilla; 

class TblListaChequeoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }
    public function indexPlantilla(Request $request)
    {
        $data = TblListaChequeoDetalle::
                select(
                    'tbl_lista_chequeo_detalle.lista_chequeo_plantilla_id AS id',
                    'tbl_lista_chequeo_detalle.folio_no',
                    'tbl_lista_chequeo_detalle.aplica',
                    'tbl_lista_chequeo_detalle.cumple_requisitos',
                    'tbl_lista_chequeo_plantilla.documentos'

                )->
                where('tbl_lista_chequeo.revisiones_tramites_id','=',$request->revisiones_tramites_id)->
                join('tbl_lista_chequeo','tbl_lista_chequeo_detalle.lista_chequeo_id', '=', 'tbl_lista_chequeo.id')->
                join('tbl_lista_chequeo_plantilla','tbl_lista_chequeo_plantilla.id', '=', 'tbl_lista_chequeo_detalle.lista_chequeo_plantilla_id')->
                get();
        if(count($data)===0)
        {
            $data=TblListaChequeoPlantilla::all();
            foreach($data as $key=>$temp)
            {
                $data[$key]['aplica']               = 1;
                $data[$key]['cumple_requisitos']    = 0;
                $data[$key]['folio_no']             = '';
                unset($data[$key]['updated_at']);
                unset($data[$key]['created_at']);
            }
        }
        return $this->sendResponse($data);
    }
    

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $listado = new TblListaChequeo();
        $listado->revisiones_tramites_id=$request->_revisiones_tramites_id;
        $listado->save();
        foreach($data['detalles'] as $key => $temp)
        {
            $temp=(object)$temp;
            $detalle                                = new TblListaChequeoDetalle();
            $detalle->lista_chequeo_id              = $listado->id;
            $detalle->folio_no                      = $temp->folio;
            $detalle->aplica                        = ($temp->aplica)?1:0;
            $detalle->cumple_requisitos             = ($temp->cumple_requisitos)?1:0;
            $detalle->lista_chequeo_plantilla_id    = $temp->lista_chequeo_plantilla_id;
            $detalle->save();
            unset($detalle);
        }
        return $this->sendResponse($listado);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
