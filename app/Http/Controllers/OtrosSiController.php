<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\TblContrato;
use App\Models\TblOtrosSi;

use Carbon\Carbon;

class OtrosSiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $data = TblOtrosSi::
      select(
        'tbl_otros_si.id',
         'tbl_otros_si.contrato_id',
         'tbl_otros_si.user_id',
         'tbl_otros_si.desde',
         'tbl_otros_si.hasta',
         'tbl_otros_si.valor',
         'tbl_otros_si.no_cdp',
         'tbl_otros_si.fecha_cdp',
         'tbl_otros_si.registro_presupuestal',
         'tbl_otros_si.fecha_registro_presupuestal',
         'tbl_otros_si.otras_modificaciones',
         'tbl_otros_si.created_at',
         'tbl_otros_si.updated_at',
         'tbl_contratos.numero'
      )->
      join('tbl_contratos','tbl_otros_si.contrato_id', '=', 'tbl_contratos.id')->
      get();

      return response()->json(['data' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $data = [
        'user_id' => auth()->user()->id,
        'contrato_id'=> $request->id_contrato,
        'desde' => Carbon::parse($request->desde)->format('Y-m-d'),
        'hasta' => Carbon::parse($request->hasta)->format('Y-m-d'),
        'valor' => $request->valor,
        'no_cdp' => $request->no_cdp,
        'fecha_cdp' => Carbon::parse($request->fecha_cdp)->format('Y-m-d'),
        'registro_presupuestal' => $request->registro_presupuestal,
        'fecha_registro_presupuestal' => Carbon::parse($request->fecha_registro_presupuestal)->format('Y-m-d'),
        'otras_modificaciones' => $request->otras_modificaciones
      ];

      $result = TblOtrosSi::create($data);

      return response()->json($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function contratosValidos($ncontrato)
    {
      $data = TblContrato::where('numero', $ncontrato)->first();
      $count = TblContrato::where('numero', $ncontrato)->count();

      return response()->json(['data' => $data, 'count' => $count]);
    }
}
