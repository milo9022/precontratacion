<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\TblContratista;
use App\Models\TblOferentes;
use App\Models\EstudiosPreviosModel;

class ContratistaController extends Controller
{
    public function search(Request $request)
    {
        try
        {
            $data = TblContratista::where('documento','=',$request->documento)->first();
            if(is_null($data))
            {
                $data = TblOferentes::where('documento','=',$request->documento)->first();
            }    
            return response()->json(['validate'=>true,'data'=>$data],200);
        } catch (\Throwable $th) {
            return response()->json(['validate'=>false,'data'=>$th->getMessage()],422);
        }
    }
    public function verOferentes(Request $request)
    {
        try {
            $data = [];
            $temp = null;
            switch($request->estudios_previos_tipo_id)
            {
                case '2':
                    $temp=EstudiosPreviosModel
                    ::with('tbl_oferentes_menor_cuantia.tbl_oferentes')
                    ->find($request->estudios_previos_id);
                break;
                case '3':
                    $temp=EstudiosPreviosModel
                    ::with('tbl_oferentes_convocatoria_publica.tbl_oferentes')
                    ->find($request->estudios_previos_id);
                break;
            }
            if(!is_null($temp))
            {
                foreach ($temp->tbl_oferentes_menor_cuantia as $key => $value)
                {
                    $data[]=$value['tbl_oferentes'];        
                }
            }
            return response()->json(['validate'=>true,'data'=>$data],200);
        }
        catch (\Throwable $th)
        {
            return response()->json(['validate'=>false,'data'=>$th->getMessage()],422);
        }
    }
}
