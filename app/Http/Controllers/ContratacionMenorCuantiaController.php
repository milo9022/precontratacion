<?php

namespace App\Http\Controllers;

use App\Models\TblContratacionMenorCuantia;
use Illuminate\Http\Request;

class ContratacionMenorCuantiaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\TblContratacionMenorCuantia  $tblContratacionMenorCuantia
     * @return \Illuminate\Http\Response
     */
    public function show(TblContratacionMenorCuantia $tblContratacionMenorCuantia)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\TblContratacionMenorCuantia  $tblContratacionMenorCuantia
     * @return \Illuminate\Http\Response
     */
    public function edit(TblContratacionMenorCuantia $tblContratacionMenorCuantia)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\TblContratacionMenorCuantia  $tblContratacionMenorCuantia
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TblContratacionMenorCuantia $tblContratacionMenorCuantia)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\TblContratacionMenorCuantia  $tblContratacionMenorCuantia
     * @return \Illuminate\Http\Response
     */
    public function destroy(TblContratacionMenorCuantia $tblContratacionMenorCuantia)
    {
        //
    }
}
