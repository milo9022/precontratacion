<?php

namespace App\Http\Controllers;

use App\Models\TblContratacionDirecta;
use Illuminate\Http\Request;

class ContratacionDirectaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }
    public function indexDetail($id)
    {
        return TblContratacionDirecta::where('estudios_previos_id',$id)->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try
        {
            $data = new TblContratacionDirecta();
            $data->causal              = $request->causal;
            $data->observacion         = $request->observacion;
            $data->estudios_previos_id = $request->estudios_previos_id;
            $data->save();
            return ['validate'=>true,'msj'=>'Se guardo con éxito'];    
        }
        catch (\Throwable $th)
        {
            return response(['validate'=>false,'msj'=>$th->getMessage()],402);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\TblContratacionDirecta  $tblContratacionDirecta
     * @return \Illuminate\Http\Response
     */
    public function show(TblContratacionDirecta $tblContratacionDirecta, $directum)
    {
        $data=$tblContratacionDirecta::where('id','=',$directum)->first();
        return is_null($data)?[]:$data;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\TblContratacionDirecta  $tblContratacionDirecta
     * @return \Illuminate\Http\Response
     */
    public function edit(TblContratacionDirecta $tblContratacionDirecta)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\TblContratacionDirecta  $tblContratacionDirecta
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TblContratacionDirecta $tblContratacionDirecta,$directum)
    {
        try{
            $data = TblContratacionDirecta::where('estudios_previos_id','=',$directum)->first();
            $data = (is_null($data))? new TblContratacionDirecta():TblContratacionDirecta::find($data->id);
            $data->causal               = $request->causal;
            $data->observacion          = $request->observacion;
            $data->estudios_previos_id  = $directum;
            $data->save();
            return ['validate'=>true,'msj'=>'Guardado con éxito'];
        }
        catch(\Exception $e) 
        {
            return ['validate'=>false,'msj'=>$e->getMessage()];
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\TblContratacionDirecta  $tblContratacionDirecta
     * @return \Illuminate\Http\Response
     */
    public function destroy(TblContratacionDirecta $tblContratacionDirecta)
    {
        //
    }
}
