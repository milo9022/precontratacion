<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\TblContratoLiquidacionFirmaGerencia;

class TblContratoLiquidacionFirmaGerenciaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = TblContratoLiquidacionFirmaGerencia::where('contrato_liquidacion_revision_inicial_id','=',$request->contrato_liquidacion_revision_inicial_id)->first();
        if(is_null($data))
        {
            $data = new TblContratoLiquidacionFirmaGerencia();
        }
        $data->contrato_liquidacion_revision_inicial_id = $request->contrato_liquidacion_revision_inicial_id;
        $data->fecha_ingreso                            = date('Y-m-d',strtotime($request->fecha_ingreso));
        $data->fecha_egreso                             = is_null($request->fecha_egreso)?null:date('Y-m-d',strtotime($request->fecha_egreso));
        $data->fecha_firma                              = is_null($request->fecha_firma)?null:date('Y-m-d',strtotime($request->fecha_firma));
        $data->radicado                                 = $request->radicado;
        $data->save();
        return $this->sendResponse($data);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
