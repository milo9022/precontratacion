<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\TblContratatosPagosRegistroPresupuestal;
use App\Models\TblContratatosPagosFirmaAdministracion;
use App\Models\TblContratatosPagosFirmaGerencium;
use App\Http\ControllersTblMensajesAlertController;

class TblContratatosPagosFirmaGerenciaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function indexStatus($status)
    {
        if((trim($status)=='all'))
        {
            $data = TblContratatosPagosFirmaGerencium::orderBy('id');
        }
        else{
            $data = TblContratatosPagosFirmaGerencium::
            where('estado','=',$status);
        }
        $data = $data->
        with('tbl_contratatos_pagos_firma_administracion.tbl_contratatos_pagos_registro_presupuestal.tbl_contratatos_pagos_registro_contable.tbl_contratatos_pagos_orden.tbl_revision_tramite.tbl_contratos')->
        get();
        return response()->json(['data'=>$data,'validate'=>true]);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = TblContratatosPagosFirmaGerencium::where('contratatos_pagos_firma_administracion_id','=',$request->contratatos_pagos_firma_administracion_id)->first();
        $data = is_null($data)?new TblContratatosPagosFirmaGerencium():TblContratatosPagosFirmaGerencium::find($data->id);
        $data->contratatos_pagos_firma_administracion_id    = $request->contratatos_pagos_firma_administracion_id;
        $data->fecha_ingreso                                = date('Y-m-d',strtotime($request->fecha_ingreso));
        $data->fecha_egreso                                 = is_null($request->fecha_egreso)?null:date('Y-m-d',strtotime($request->fecha_egreso));
        $data->estado                                       = 'tramite';
        $data->save();
        TblMensajesAlertController::add('Se ha aprobado una nueva firma de gerencia');

        $registroPresupuestal = TblContratatosPagosFirmaAdministracion::find($request->contratatos_pagos_firma_administracion_id);
        $registroPresupuestal->estado                       =is_null($data->fecha_egreso)?$registroPresupuestal->estado:'aprobado';
        $registroPresupuestal->save();
        TblMensajesAlertController::add('Se ha aprobado una nueva firma de administracion');
        return response()->json(['validate'=>true,'data'=>$data]);
    }
    
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = TblContratatosPagosFirmaGerencium::
        with('tbl_contratatos_pagos_firma_administracion.tbl_contratatos_pagos_registro_presupuestal.tbl_contratatos_pagos_registro_contable.tbl_contratatos_pagos_orden.tbl_revision_tramite')->
        where('id','=',$id)->
        firstOrFail();
        return response()->json(['validate'=>true,'data'=>$data]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
