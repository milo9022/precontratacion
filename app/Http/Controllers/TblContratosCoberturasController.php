<?php

namespace App\Http\Controllers;

use Carbon\Carbon;

use App\Models\TblContratosCobertura;
use App\Models\TblContrato;

use Illuminate\Http\Request;

class TblContratosCoberturasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $result=[];
        foreach($request->input('data') as $temp)
        {
            $temp = (object) $temp;
            $data = [
                'user_id' => auth()->user()->id,
                'contratos_id' => $temp->contrato_id,
                'fecha_aprobacion_poliza' => Carbon::parse($request->fecha_poliza)->format('Y-m-d'),
                'clase_cobertura' => $temp->clase_cobertura,
                'fecha_inicio' => Carbon::parse($temp->fecha_inicio)->format('Y-m-d'),
                'fecha_fin' => Carbon::parse($temp->fecha_final)->format('Y-m-d')
            ];
            $result[] = TblContratosCobertura::create($data);
            unset($data);
        }

        return response()->json(['msg' => 'success', 'data' => $result]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getContratos()
    {
        $tbl_contrato = TblContrato::with(['tbl_contratos_cobertura', 'tbl_actas_de_inicio'])->get();
        return response()->json(['data' => $tbl_contrato], 200);
    }
}
