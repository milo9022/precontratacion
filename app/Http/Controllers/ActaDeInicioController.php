<?php

namespace App\Http\Controllers;

use App\Models\TblContrato;
use App\Models\RecursoHumano;
use App\Models\TblContratista;
use App\Models\TblActaInicio;

use Illuminate\Http\Request;

class ActaDeInicioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $data=TblActaInicio::with('tbl_contratos')->get();
      return response()->json(['data'=>$data,'validate'=>true]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $supervisor = [
          'nombre_primero'   => $request->supervisor['nombre_primero'],
          'nombre_segundo'   => $request->supervisor['nombre_segundo'],
          'apellido_primero' => $request->supervisor['apellido_primero'],
          'apellido_segundo' => $request->supervisor['apellido_segundo'],
          'documento'        => $request->supervisor['documento'],
          'celular'          => $request->supervisor['celular'],
          'email'            => $request->supervisor['email'],
          'direccion'        => $request->supervisor['direccion'],
          'activo'           => 1,
          'cargo_id'         => $request->supervisor['cargo_id'],
        ];

        $contratista = [
          'establecimiento_nombre'    => $request->contratista['establecimiento_nombre'],
          'nombre_primero'            => $request->contratista['nombre_primero'],
          'nombre_segundo'            => $request->contratista['nombre_segundo'],
          'apellido_primero'          => $request->contratista['apellido_primero'],
          'apellido_segundo'          => $request->contratista['apellido_segundo'],
          'documento'                 => $request->contratista['documento'],
          'celular'                   => $request->contratista['celular'],
          'email'                     => $request->contratista['email'],
          'direccion'                 => $request->contratista['direccion'],
          'municipio_id'              => $request->contratista['municipio_id'],
        ];

        $supervisor_id = RecursoHumano::updateOrCreate([
          'documento' => $request->supervisor['documento'],
        ], $supervisor);

        $contratista_id = TblContratista::updateOrCreate([
          'documento' => $request->contratista['documento'],
        ], $contratista);

        $contrato = TblContrato::find($request->id);

        $contrato->update(['recursos_humanos_supervisor_id' => $supervisor_id->id,
          'contratista_id' => $contratista_id->id]);

        $data = TblActaInicio::create([
          'user_id' => auth()->user()->id,
          'supervisor_id' => $supervisor_id->id,
          'contratista_id' => $contratista_id->id,
          'contratos_id' => $request->id,
          'fecha_suscripcion' => date('Y-m-d',strtotime($request->suscripcion_fecha))
        ]);

        return response()->json($contrato);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $data=TblActaInicio::
      with('tbl_contratos')
      ->with('tbl_recursos_humanos_supervisor')
      ->with('tbl_recursos_humanos_supervisor.tbl_cargos')
      ->with('tbl_contratista')
      ->with('tbl_contratos.tbl_contratos_cobertura')
      ->with('tbl_contratos.tbl_recursos_humanos_supervisor')
      ->where('id','=',$id)
      ->first();
      return response()->json(['data'=>$data,'validate'=>true]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getContrato($id)
    {
      $data = TblContrato::
      with('tbl_recursos_humanos_supervisor')->
      with('tbl_contratistas')->
      where('id','=',$id)->
      first();
      return $this->sendResponse($data);
    }
}
