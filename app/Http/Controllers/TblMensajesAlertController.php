<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\TblMensajesAlert;
use App\User;
use App\Http\ControllersTblMensajesAlertController;

class TblMensajesAlertController extends Controller
{
    public static function add($mensaje,$tipo='success')
    {
        $user           = User::with('roles')->first();
        $data           = $data = new TblMensajesAlert();
        $data->mensaje  = $mensaje;
        $data->user_id  = $user->id;
        $data->role_id  = $user->roles[0]->id;
        $data->tipo     = $tipo;
        $data->save();

    }
    public function showAlert(Request $request)
    {
        if(is_null($request->status))
        {
            $data = TblMensajesAlert::where('noleido','=',1)->orderBy('id','DESC')->get();
        }else{
            $data = TblMensajesAlert::orderBy('id','DESC')->get();
            foreach($data as $temp)
            {
                TblMensajesAlert::
                where('id', $temp->id)->
                update(['noleido' => 0]);
            }
        }
        return $this->sendResponse($data);
    }
    public function show($id)
    {
        $data = TblMensajesAlert::find($id);
        return $this->sendResponse($data);
    }
}
