<?php

namespace App\Http\Controllers;

use App\Models\TblSupervision;
use Illuminate\Http\Request;

class SupervisionController extends Controller
{
    
    protected $model;

    public function __construct(TblSupervision $supervision)
    {
    	$this->model = $supervision;
    }

    public function store(Request $request)
    {
    	$data = $request->all();

    	try {
    		$this->model->create($data);

    		return $this->sendResponse(null, 'Detalle de supervisión creado');
    	} catch (\Throwable $th) {
    		return $this->sendError('Error', $th->getMessage(), 422);
    	}
    }

    public function supervisionesContrato($id)
    {
    	$supervisiones = $this->model
    					->contrato($id)
    					->get();

        return $this->sendResponse($supervisiones, 'Lista de presentaciones');

    }
}
