<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\TblPlanAnualAdquisicion;

class TblPlanAnualAdquisicionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = TblPlanAnualAdquisicion::orderBy('nombre')->get();
        return response()->json(['validate'=>true,'data'=>$data]);
    }
    public function search(Request $request)
    {
        $data = TblPlanAnualAdquisicion::
        select('nombre as name','id','codigos')
        ->orderBy('nombre')
        ->where('codigos','like','%'.trim($request->data).'%')
        ->orWhere('nombre','like','%'.trim($request->data).'%')
        ->get();
        return response()->json($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $fecha = is_null($request->fecha_estimada_inicio_seleccion) ? null : explode('T',$request->fecha_estimada_inicio_seleccion);
        $data = new TblPlanAnualAdquisicion();
        $data->nombre                              = $request->nombre;
        $data->codigos                             = implode(';', $request->codigos);
        $data->fecha_estimada_inicio_seleccion     = $fecha[0];
        $data->duracion_estimada_contrato          = $request->duracion_estimada_contrato;
        $data->modalidad_seleccion                 = $request->modalidad_seleccion;
        $data->fuente_recursos                     = $request->fuente_recursos;
        $data->valor_estimado                      = $request->valor_estimado;
        $data->valor_estimado_vigencia_actual      = $request->valor_estimado_vigencia_actual;
        $data->require_vigencia_futura             = $request->require_vigencia_futura;
        $data->estado_solicitud_vigencia_futura    = $request->estado_solicitud_vigencia_futura;
        $data->datos_contacto_responsable          = $request->datos_contacto_responsable;
        $data->save();
        return response()->json(['validate'=>true,'data'=>$data]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = TblPlanAnualAdquisicion::find($id);
        return response()->json(['data'=>$data,'validate'=>true]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data =TblPlanAnualAdquisicion::find($id);
        $data->nombre                              = $request->nombre;
        $data->codigos                             = implode(';', $request->codigos);
        $data->fecha_estimada_inicio_seleccion     = $request->fecha_estimada_inicio_seleccion;
        $data->duracion_estimada_contrato          = $request->duracion_estimada_contrato;
        $data->modalidad_seleccion                 = $request->modalidad_seleccion;
        $data->fuente_recursos                     = $request->fuente_recursos;
        $data->valor_estimado                      = $request->valor_estimado;
        $data->valor_estimado_vigencia_actual      = $request->valor_estimado_vigencia_actual;
        $data->require_vigencia_futura             = $request->require_vigencia_futura;
        $data->estado_solicitud_vigencia_futura    = $request->estado_solicitud_vigencia_futura;
        $data->datos_contacto_responsable          = $request->datos_contacto_responsable;
        $data->save();
        return response()->json(['validate'=>true,'data'=>$data]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        TblPlanAnualAdquisicion::find($id)->delete();
        return response()->json(['validate'=>true]);
    }
}
