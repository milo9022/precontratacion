<?php

namespace App\Http\Controllers;

use App\Models\EstudiosPreviosModel;
use App\Models\TblContratacionDirecta;
use App\Models\TblContratacionMenorCuantiaXOferente;
use App\Models\TblContratacionMenorCuantiaXProponente;
use App\Models\TblContratacionConvocatoriaPublicaXMiembroComiteEvaluador;
use App\Models\TblContratacionMenorCuantiaXMiembroComiteEvaluador;
use App\Models\TblContratacionConvocatoriaPublicaXProponente;
use App\Models\TblContratacionConvocatoriaPublicaXOferente;
use App\Models\TblEstudiosPreviosCuotas;
use App\Models\TblEstudiosPreviosMunicipios;
use Illuminate\Http\Request;
use App\User;

use App\Models\TblEstdiosPreviosApoyoJuridico;
use App\Models\TblEstdiosPreviosApoyosTecnico;
use App\Models\TblEstdiosPreviosQuienSuscribe;
use Illuminate\Support\Facades\Auth;

class EstudiosPreviosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $userRol = User::where('id','=',Auth::user()->id)->with('roles')->with('tbl_recursos_humanos')->get()->first();
        $data = EstudiosPreviosModel::
        with('tbl_contratos_cdp')->
        with('tbl_formas_de_pago')->
        with('tbl_estudios_previos_tipo')->
        with('tbl_estudios_previos_estado')->
        with('tbl_estudios_previos_municipios.tbl_municipios')->
        with('tbl_plan_anual_adquisicion')->
        with('tbl_estdios_previos_apoyos_tecnicos.tbl_recursos_humano')->
        with('tbl_estdios_previos_apoyo_juridico.tbl_recursos_humano')->
        with('tbl_estdios_previos_quien_suscribe.tbl_recursos_humano');

        //with('tbl_recursos_humano_quien_suscribe')->
        //with('tbl_recursos_humano_apoyo_tecnicos')->
        //with('tbl_recursos_humano_apoyo_juridico');
        if($userRol->roles[0]->id==2)
        {
            $data=$data->get();
            foreach($data as $key => $temp)
            {
                $exist = false;
                foreach($temp->tbl_estdios_previos_apoyos_tecnicos as $temp2)
                {
                    if(!is_null($userRol->tbl_recursos_humanos))
                    {
                        if($temp2->recursos_humanos_id==$userRol->tbl_recursos_humanos->id)
                        {
                            $exist=true;
                        }
                    }
                }
                foreach($temp->tbl_estdios_previos_apoyo_juridico as $temp2)
                {
                    if(!is_null($userRol->tbl_recursos_humanos))
                    {
                        if($temp2->recursos_humanos_id==$userRol->tbl_recursos_humanos->id)
                        {
                            $exist=true;
                        }
                    }
                }
                foreach($temp->tbl_estdios_previos_quien_suscribe as $temp2)
                {
                    if(!is_null($userRol->tbl_recursos_humanos))
                    {
                        if($temp2->recursos_humanos_id==$userRol->tbl_recursos_humanos->id)
                        {
                            $exist=true;
                        }
                    }
                }
                if(!$exist)
                {
                    unset($data[$key]);
                }    
            }
        }
        else{
            $data = $data->get();
        }
        return $data;
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    private function oferentesMenorCuantia($estudios_previos_id)
    {
        return TblContratacionMenorCuantiaXOferente::
        where('estudios_previos_id','=',$estudios_previos_id)->
        with('tbl_oferentes')->
        //with('')->
        get();
    }
    private function proponentesMenorCuantia($estudios_previos_id)
    {
        return TblContratacionMenorCuantiaXProponente::
        where('estudios_previos_id','=',$estudios_previos_id)->
        with('tbl_proponente')->
        //with('')->
        get();
    }
    private function comiteEvaluadorMenorCuantia($estudios_previos_id)
    {
        return TblContratacionMenorCuantiaXMiembroComiteEvaluador::
        where('estudios_previos_id','=',$estudios_previos_id)->
        with('tbl_comite_evaluador')->
        get();
    }
    private function oferentesConvocatoriaPublica($estudios_previos_id)
    {
        return TblContratacionConvocatoriaPublicaXOferente::
        where('estudios_previos_id','=',$estudios_previos_id)->
        with('tbl_oferentes')->
        //with('')->
        get();
    }
    private function proponentesConvocatoriaPublica($estudios_previos_id)
    {
        $data =  TblContratacionConvocatoriaPublicaXProponente::
        where('estudios_previos_id','=',$estudios_previos_id)->
        with('tbl_proponente')->
        //with('')->
        get();
        return $data;
    }
    private function comiteEvaluadorConvocatoriaPublica($estudios_previos_id)
    {
        return TblContratacionConvocatoriaPublicaXMiembroComiteEvaluador::
        where('estudios_previos_id','=',$estudios_previos_id)->
        with('tbl_comite_evaluador')->
        get();
    }
    private function Causales($estudios_previos_id)
    {
        return TblContratacionDirecta::where('estudios_previos_id',$estudios_previos_id)->first();
    }
    public function tiposContrato($name)
    {
        $data = [];
        switch($name)
        {
            case'directa':
                $res = EstudiosPreviosModel::
                    with('tbl_estudios_previos_tipo')->
                    with('tbl_estudios_previos_estado')->
                    with('tbl_estudios_previos_municipios.tbl_municipios')->
                    with('tbl_estdios_previos_apoyos_tecnicos.tbl_recursos_humano')->
                    with('tbl_estdios_previos_apoyo_juridico.tbl_recursos_humano')->
                    with('tbl_estdios_previos_quien_suscribe.tbl_recursos_humano')->
                    where('estudios_previos_tipo_id','=',1)->
                    get();
                    foreach($res as $key => $temp)
                    {
                        $res[$key]->causales=$this->Causales($temp->id);
                    }
                return $res;
            break;

            case'menorcuantia':
                $res = EstudiosPreviosModel::
                with('tbl_estudios_previos_municipios.tbl_municipios')->
                    with('tbl_plan_anual_adquisicion')->
                    with('tbl_estdios_previos_apoyos_tecnicos.tbl_recursos_humano')->
                    with('tbl_estdios_previos_apoyo_juridico.tbl_recursos_humano')->
                    with('tbl_estdios_previos_quien_suscribe.tbl_recursos_humano')->
                    where('estudios_previos_tipo_id','=',2)->
                    get();
                foreach($res as $key => $temp)
                {
                    $res[$key]->tbl_oferentes=$this->oferentesMenorCuantia($temp->id);
                    $res[$key]->tbl_proponentes=$this->proponentesMenorCuantia($temp->id);
                    $res[$key]->tbl_comite_evaluador=$this->comiteEvaluadorMenorCuantia($temp->id);
                }
                $data = $res;
            break;

            case'convocatoriapublica':
                $res = EstudiosPreviosModel::
                    with('tbl_estudios_previos_municipios.tbl_municipios')->
                    with('tbl_estdios_previos_apoyos_tecnicos.tbl_recursos_humano')->
                    with('tbl_estdios_previos_apoyo_juridico.tbl_recursos_humano')->
                    with('tbl_estdios_previos_quien_suscribe.tbl_recursos_humano')->
                    where('estudios_previos_tipo_id','=',3)->
                    get();
                foreach($res as $key => $temp)
                {
                    $res[$key]->tbl_oferentes=$this->oferentesConvocatoriaPublica($temp->id);
                    $res[$key]->tbl_proponentes=$this->proponentesConvocatoriaPublica($temp->id);
                    $res[$key]->tbl_comite_evaluador=$this->comiteEvaluadorConvocatoriaPublica($temp->id);
                }
                $data = $res;                
            break;

            default: 
                $data = response(['validate'=>false,'msj'=>'Opcion "'.$name.'" no disponible'],402);
            break;
        }
        return $data;
    }
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $status=0;
        try 
        {
            $results = new EstudiosPreviosModel();
            $results->vigencia                             = $request->vigencia;
            $results->estudios_previos_tipo_id             = $request->estudios_previos_tipo_id;
            $results->plan_anual_adquisicion_id            = $request->plan_anual_adquisicion_id;
            //$results->recursos_humano_quien_suscribe_id    = $request->recursos_humano_quien_suscribe_id;
            //$results->recursos_humano_apoyo_tecnicos_id    = $request->recursos_humano_apoyo_tecnicos_id;
            //$results->recursos_humano_apoyo_juridico_id    = $request->recursos_humano_apoyo_juridico_id;
            $results->fecha_estudios_previos               = $request->fecha_estudios_previos;
            $results->objeto                               = $request->objeto;
            $results->experiencia                          = $request->experiencia;
            $results->idoneidad                            = $request->idoneidad;
            $results->otros                                = $request->otros;
            $results->tipologias_contractuales_id          = $request->tipologias_contractuales_id;
            $results->plazo_ejecucion                      = $request->plazo_ejecucion;
            $results->valor_letras                         = $request->valor_letras;
            $results->forma_de_pago_id                     = $request->forma_de_pago_id;
            $results->numero_cuotas                        = $request->numero_cuotas;
            $results->garantias_id                         = $request->garantias_id;
            $results->lugar_ejecucion                      = $request->lugar_ejecucion;
            $results->valor_numero                         = str_replace(',','',$request->valor_numero);
            $results->estudios_previos_estado_id           = 1;
            $results->codigo                               = null;
            $results->save();
            $id=$results->id;
            $res = EstudiosPreviosModel::find($id);
            $res->codigo = str_pad($id, 6, "0", STR_PAD_LEFT);
            $res->save();
            foreach($request->cuotas as $temp)
            {
                $cuota = new TblEstudiosPreviosCuotas();
                $cuota->estudios_previos_id=$id;
                $cuota->cuota = $temp['cuota'];
                $cuota->valor = (double)str_replace(',','',$temp['valor']);
                $cuota->save();
                unset($cuota);
            }
            $status=$status+1;
            foreach($request->municipio_id as $temp)
            {
                $municipios=new TblEstudiosPreviosMunicipios();
                $municipios->estudios_previos_id = $id;
                $municipios->municipio_id        = $temp;
                $municipios->save();

            }
            $status=$status+1;
            /*========================== RECURSOS HUMANOS ==========================*/
            foreach($request->recursos_humano_quien_suscribe_id as $temp)
            {
                $municipios=new TblEstdiosPreviosQuienSuscribe();
                $municipios->estudios_previos_id = $id;
                $municipios->recursos_humanos_id = $temp;
                $municipios->save();

            }
            $status=$status+1;
            foreach($request->recursos_humano_apoyo_tecnicos_id as $temp)
            {
                $municipios=new TblEstdiosPreviosApoyosTecnico();
                $municipios->estudios_previos_id = $id;
                $municipios->recursos_humanos_id = $temp;
                $municipios->save();

            }
            $status=$status+1;
            foreach($request->recursos_humano_apoyo_juridico_id as $temp)
            {
                $municipios=new TblEstdiosPreviosApoyoJuridico();
                $municipios->estudios_previos_id = $id;
                $municipios->recursos_humanos_id = $temp;
                $municipios->save();

            }
            $status=$status+1;
            /*========================== RECURSOS HUMANOS ==========================*/
            $url= public_path().DIRECTORY_SEPARATOR.'ef'.DIRECTORY_SEPARATOR.'files'.DIRECTORY_SEPARATOR.$id.DIRECTORY_SEPARATOR.'archivos';
            if(!is_dir($url))
            {
                if(!mkdir($url, 0777))
                {
                    return response(['validate'=>false,'msj'=>'No se pudo crear la carpeta'],402);
                }
            }
            return ['validate'=>true,'msj'=>'guardado con exito','id'=>$id];
        } catch (\Throwable $th) {
            return response(['validate'=>false,'msj'=>$th->getMessage().'['.$status.']'],402);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\EstudiosPreviosModel  $estudios_previosModel
     * @return \Illuminate\Http\Response
     */
    public function show(EstudiosPreviosModel $estudiosPreviosModel, $estudiosprevios)
    {
        try {

            $data = EstudiosPreviosModel::
            with('tbl_estudios_previos_cuotas')->
            with('tbl_plan_anual_adquisicion')->
            with('tbl_estudios_previos_municipios')->
            where('id','=',$estudiosprevios)->
            firstOrFail();
            $recursos_humano_quien_suscribe_id=TblEstdiosPreviosApoyoJuridico::where('estudios_previos_id','=',$data->id)->pluck('recursos_humanos_id');
            $recursos_humano_apoyo_tecnicos_id=TblEstdiosPreviosApoyosTecnico::where('estudios_previos_id','=',$data->id)->pluck('recursos_humanos_id');
            $recursos_humano_apoyo_juridico_id=TblEstdiosPreviosQuienSuscribe::where('estudios_previos_id','=',$data->id)->pluck('recursos_humanos_id');
            $data->recursos_humano_quien_suscribe_id=$recursos_humano_quien_suscribe_id;
            $data->recursos_humano_apoyo_tecnicos_id=$recursos_humano_apoyo_tecnicos_id;
            $data->recursos_humano_apoyo_juridico_id=$recursos_humano_apoyo_juridico_id;
            return $data;
        } catch (\Throwable $th) {
            return response(['validate'=>false,'msj'=>$th->getMessage()],402);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\EstudiosPreviosModel  $estudiosPreviosModel
     * @return \Illuminate\Http\Response
     */
    public function edit(EstudiosPreviosModel $estudiosPreviosModel)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\EstudiosPreviosModel  $estudiosPreviosModel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, EstudiosPreviosModel $estudiosPreviosModel)
    {
        try 
        {
            $id      = $request->id;
            $results = EstudiosPreviosModel::where('id','=',$id)->firstOrFail();
            $results = EstudiosPreviosModel::find($id);
            $results->vigencia                             = $request->vigencia;
            $results->estudios_previos_tipo_id             = $request->estudios_previos_tipo_id;
            $results->plan_anual_adquisicion_id            = $request->plan_anual_adquisicion_id;
            $results->recursos_humano_quien_suscribe_id    = $request->recursos_humano_quien_suscribe_id;
            $results->recursos_humano_apoyo_tecnicos_id    = $request->recursos_humano_apoyo_tecnicos_id;
            $results->recursos_humano_apoyo_juridico_id    = $request->recursos_humano_apoyo_juridico_id;
            $results->fecha_estudios_previos               = $request->fecha_estudios_previos;
            $results->objeto                               = $request->objeto;
            $results->experiencia                          = $request->experiencia;
            $results->idoneidad                            = $request->idoneidad;
            $results->otros                                = $request->otros;
            $results->tipologias_contractuales_id          = $request->tipologias_contractuales_id;
            $results->plazo_ejecucion                      = $request->plazo_ejecucion;
            $results->valor_letras                         = $request->valor_letras;
            $results->forma_de_pago_id                     = $request->forma_de_pago_id;
            $results->numero_cuotas                        = $request->numero_cuotas;
            $results->garantias_id                         = $request->garantias_id;
            $results->lugar_ejecucion                      = $request->lugar_ejecucion;
            $results->valor_numero                         = (double)str_replace(',','',$request->valor_numero);
            $results->save();
            $new=count($request->cuotas);
            $old=count(TblEstudiosPreviosCuotas::where('estudios_previos_id','=',$id)->get());
            if($new>$old)
            {
                foreach($request->cuotas as $temp)
                {
                    $cuota = TblEstudiosPreviosCuotas::where('estudios_previos_id','=',$id)->where('cuota','=',$temp['cuota'])->first();
                    $cuota =(is_null($cuota))? new TblEstudiosPreviosCuotas(): TblEstudiosPreviosCuotas::find($cuota->id);
                    $cuota->valor               = (double)str_replace(',','',$temp['valor']);
                    $cuota->cuota               = $temp['cuota'];
                    $cuota->estudios_previos_id = $id;
                    $cuota->save();
                    unset($cuota);
                }
            }
            else if($new<$old)
            {
                $max=-1;
                foreach($request->cuotas as $temp)
                {
                    $max=$temp['cuota']>$max?$temp['cuota']:$max;
                    $cuota = TblEstudiosPreviosCuotas::where('estudios_previos_id','=',$id)->where('cuota','=',$temp['cuota'])->first();
                    $cuota =(is_null($cuota))? new TblEstudiosPreviosCuotas(): TblEstudiosPreviosCuotas::find($cuota->id);
                    $cuota->valor               = (double)str_replace(',','',$temp['valor']);
                    $cuota->cuota               = $temp['cuota'];
                    $cuota->estudios_previos_id = $id;
                    $cuota->save();
                    unset($cuota);
                }
                $deletedRows=TblEstudiosPreviosCuotas::where('cuota','>', $max)->delete();
            }else{
                foreach($request->cuotas as $temp)
                {
                    $cuota = TblEstudiosPreviosCuotas::where('estudios_previos_id','=',$id)->where('cuota','=',$temp['cuota'])->first();
                    $cuota = TblEstudiosPreviosCuotas::find($cuota->id);
                    $cuota->valor= (double)str_replace(',','',$temp['valor']);
                    $cuota->save();
                    unset($cuota);
                }
            }
            return ['validate'=>true,'msj'=>'guardado con exito','data'=>$results];
        } catch (\Throwable $th) {
            return response(['validate'=>false,'msj'=>$th->getMessage()],402);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\EstudiosPreviosModel  $estudiosPreviosModel
     * @return \Illuminate\Http\Response
     */
    public function destroy(EstudiosPreviosModel $estudiosPreviosModel)
    {
        //
    }
}
