<?php

namespace App\Http\Controllers;

use App\Models\TblRevisionTramite;
use App\Models\TblContrato;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use DB;

class RevisionesTramitesController extends Controller
{
    protected $model;
    protected $contrato;

    public function __construct(TblRevisionTramite $revision, TblContrato $contrato)
    {
    	$this->model = $revision; 
        $this->contrato = $contrato;
    }

    public function presentacionesContrato($contrato)
    {

    	$presentaciones = $this->model->where('contrato_id', $contrato)->get();
        $contrato = $this->contrato
					->with('tbl_contratistas')
					->with('tbl_estudios_previos.tbl_contratos_cdp')
                    ->with('tbl_recursos_humanos_supervisor')
                    ->with('tbl_recursos_humanos_supervisor.tbl_cargos')
                    ->find($contrato);

        $data = [
            'presentaciones' =>$presentaciones,
            'detalle_contrato' => $contrato
        ];

        return $this->sendResponse($data, 'Lista de presentaciones');
    }

    public function guardarPresentacion(Request $request)
    {
    	try {
    		$this->model->create($request->all());
        	
        	return $this->sendResponse(null, 'Presentacion guardada');
    	} catch(\Throwable $th) {
    		return $this->sendError('Error', $th->getMessage(), 422);
    	}
    }

    public function editarPresentacion(Request $request, $id)
    {
    	try {

    		$presentacion = $this->model->find($id);

    		$presentacion->update($request->all());
        	
        	return $this->sendResponse(null, 'Presentacion actualizada');

    	} catch(\Throwable $th) {
    		return $this->sendError('Error', $th->getMessage(), 422);
    	}
	}

	public function index(Request $request)
	{

		try {
			$data = $this->model::
			with('tbl_contratos')->
			whereNotNull('fecha_egreso')->
			get();
			$response=[];
			foreach($data as $temp)
			{
				$response[$temp->tbl_contratos->numero][]=$temp;
			}
			return $this->sendResponse($response, 'Presentacion actualizada');
		} catch(\Throwable $th) {
			return $this->sendError('Error', $th->getMessage(), 422);
		}
	}

}
