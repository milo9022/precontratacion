<?php

namespace App\Http\Controllers;
use App\Models\TblOferentes;
use App\Models\TblContratacionMenorCuantiaXOferente;
use App\Models\TblContratacionConvocatoriaPublicaXOferente;
use Illuminate\Http\Request;

class OferenteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        if($request->tipo_contrato!='menorcuantia' && $request->tipo_contrato!='convocatoriapublica')
        {
            return response(['validate'=>false,'msj'=>'Guardado oferente con éxito'], 402);
        }
        try 
        {
            $data = TblOferentes::where('documento','=',$request->documento)->first();
            if(is_null($data))
            {
                $data = new TblOferentes();
            }
            else{
                $data = TblOferentes::find($data->id);
            }
            $data->nombre_primero   = $request->nombre_primero;
            $data->nombre_segundo   = $request->nombre_segundo;
            $data->apellido_primero = $request->apellido_primero;
            $data->apellido_segundo = $request->apellido_segundo;
            $data->documento        = $request->documento;
            $data->celular          = $request->celular;
            $data->email            = $request->email;
            $data->direccion        = $request->direccion;
            $data->save();
            
            switch($request->tipo_contrato)
            {
                case 'menorcuantia':
                    $oferenteNuevo = new TblContratacionMenorCuantiaXOferente();
                    $oferenteNuevo->oferente_id                     = $data->id;
                    $oferenteNuevo->fecha_remision_invitaciones     = $request->fecha_remision;
                    $oferenteNuevo->estudios_previos_id             = $request->id_menor_cuantia;
                    $oferenteNuevo->save();
                break;
                case 'convocatoriapublica':
                    $OferenteNuevo = new TblContratacionConvocatoriaPublicaXOferente();
                    $OferenteNuevo->oferente_id         = $data->id;
                    $OferenteNuevo->estudios_previos_id = (int)$request->id_menor_cuantia;
                    $OferenteNuevo->save();
                break;
                
            }
            return ['validate'=>true,'msj'=>'Guardado oferente con éxito'];
            
        }
        catch (\Throwable $th)
        {
            return response(['validate'=>false,'msj'=>'Error al guardar','error'=>$th->errorInfo], 402);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return TblOferentes::find($id);   
    }
    
    public function SearchDocumento(Request $request)
    {
        $data = TblOferentes::where('documento','=',$request->documento)->first();
        return ['data'=>$data,'validate'=>true];
    }
    

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
