<?php

namespace App\Http\Controllers;

use App\Models\EstudiosPreviosTipoModel;
use Illuminate\Http\Request;

class EstudiosPreviosTipoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return  EstudiosPreviosTipoModel::all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\EstudiosPreviosTipoModel  $estudiospreviosTipoModel
     * @return \Illuminate\Http\Response
     */
    public function show(EstudiosPreviosTipoModel $estudiospreviosTipoModel)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\EstudiosPreviosTipoModel  $estudiospreviosTipoModel
     * @return \Illuminate\Http\Response
     */
    public function edit(EstudiosPreviosTipoModel $estudiospreviosTipoModel)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\EstudiosPreviosTipoModel  $estudiospreviosTipoModel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, EstudiosPreviosTipoModel $estudiospreviosTipoModel)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\EstudiosPreviosTipoModel  $estudiospreviosTipoModel
     * @return \Illuminate\Http\Response
     */
    public function destroy(EstudiosPreviosTipoModel $estudiospreviosTipoModel)
    {
        //
    }
}
