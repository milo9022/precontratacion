<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\User;
use App\Models\TblContrato;
use App\Models\TblOrdenPago;
use App\Models\RecursoHumano;
use App\Models\TblActaInicio;
use App\Models\TblContratista;
use App\Models\TblContratosCdp;
use App\Models\TblContratosCuota;
use App\Models\TblActaDeReinicio;
use App\Models\TblRevisionTramite;
use App\Models\TblContratosXApoyo;
use App\Models\TblActaDeSuspension;
use Illuminate\Support\Facades\Auth;
use App\Models\EstudiosPreviosModel;
use App\Models\TblContratoLiquidacion;
use App\Models\TblContratosXSupervision;
use App\Models\TblContratatosPagosFirmaGerencium;
use App\Models\TblContratatosPagosRegistroContable;
use App\Models\TblContratoLiquidacionFirmaGerencia;
use App\Models\TblContratoLiquidacionRevisionInicial;
use App\Models\TblContratoLiquidacionObservacionesPostcontracuales;
use App\Models\TblContratatosPagosOrden;

class contratosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function allContratos(Request $request)
    {
        $userRol = User::where('id','=',Auth::user()->id)->with('roles')->get()->first();
        $data = TblContrato::
        with('tbl_contratos_estados')->
        with('tbl_contratos_cuotas')->
        with('tbl_contratistas')->
        with('tbl_contratistas')->
        //with('tbl_recursos_humanos_supervisor')->
        //with('tbl_recursos_humanos_apoyo')->
        with('tbl_contrato_liquidacion.tbl_contrato_liquidacion_revision_inicial.tbl_contrato_liquidacion_firma_gerencia')->
        with('tbl_contrato_liquidacion_observaciones_postcontracuales');
        if($userRol->roles[0]->id==2)
        {
            $user = RecursoHumano::select('id')->where('user_id','=',Auth::user()->id)->firstOrFail();
            $data = $data->where('tbl_contratos.recursos_humanos_supervisor_id', '=', $user->id);
        }
        $data = $data->get();
        foreach($data as $key=>$temp)
        {
            $res=TblContratatosPagosFirmaGerencium::
            select(
                'tbl_contratatos_pagos_orden.numero_orden_pago',
                'tbl_contratatos_pagos_orden.valor'
            )->
            where('tbl_revisiones_tramites.contrato_id','=',$temp->id)->
            join('tbl_contratatos_pagos_tesoreria','tbl_contratatos_pagos_firma_gerencia.id','=','tbl_contratatos_pagos_tesoreria.contratatos_pagos_firma_gerencia_id')->
            join('tbl_contratatos_pagos_firma_administracion','tbl_contratatos_pagos_firma_administracion.id','=','tbl_contratatos_pagos_firma_gerencia.contratatos_pagos_firma_administracion_id')->
            join('tbl_contratatos_pagos_registro_presupuestal','tbl_contratatos_pagos_firma_administracion.contratatos_pagos_registro_presupuestal_id','=','tbl_contratatos_pagos_registro_presupuestal.id')->
            join('tbl_contratatos_pagos_registro_contable','tbl_contratatos_pagos_registro_contable.id','=','tbl_contratatos_pagos_registro_presupuestal.contratatos_pagos_registro_contable_id')->
            join('tbl_contratatos_pagos_orden','tbl_contratatos_pagos_orden.id','=','tbl_contratatos_pagos_registro_contable.contratatos_pagos_orden_id')->
            join('tbl_revisiones_tramites','tbl_contratatos_pagos_orden.revisiones_tramites_id','=','tbl_revisiones_tramites.id')->
            get();
            $data[$key]->ordenesPago=$res;

            //APOYOS TECNICOS
            $data[$key]->apoyos=TblContratosXApoyo::where('contrato_id','=',$temp->id)->
            join('tbl_recursos_humanos'  , 'tbl_contratos_x_apoyo.contrato_id', '=', 'tbl_recursos_humanos.id')->
            get();
            //SUPERVISORES
            $data[$key]->supervisores=TblContratosXSupervision::where('contrato_id','=',$temp->id)->
            join('tbl_recursos_humanos'  , 'tbl_contratos_x_supervision.contrato_id', '=', 'tbl_recursos_humanos.id')->
            get();

        }
        return $this->sendResponse($data);
    }
    public function index()
    {
        $data=
        EstudiosPreviosModel::with('tbl_contratos_cdp')
        ->get();
        return ['validate'=>true,'data'=>$data];
    }
    public function indexCDPaprobados(Request $request)
    {
        $data=TblContratosCdp::
        with('tbl_contratos_cdp')->
        with('tbl_contratos_cdp.tbl_estudios_previos_tipo')->
        with('tbl_contratos_cdp.tbl_estudios_previos_estado')->
        with('tbl_contratos_cdp.tbl_estudios_previos_municipios.tbl_municipios')->
        with('tbl_contratos_cdp.tbl_plan_anual_adquisicion')->
        with('tbl_contratos_cdp.tbl_recursos_humano_quien_suscribe')->
        with('tbl_contratos_cdp.tbl_recursos_humano_apoyo_tecnicos')->
        with('tbl_contratos_cdp.tbl_recursos_humano_apoyo_juridico')->
        get();
        return
        [
            'validate'=>true,
            'data'=>$data
        ];
    }
    private function newNumeroContrato()
    {
        $data = TblContrato::max('numero');
        return ($data+1);
    }
    public function datosCDP(Request $request)
    {
        $data = TblContratosCdp::
        with('tbl_contratos_cdp')->
        with('tbl_estudios_previos')->
        with('tbl_estudios_previos.tbl_estudios_previos_cuotas')->
        with('tbl_contratos_cdp.tbl_estudios_previos_tipo')->
        with('tbl_contratos_cdp.tbl_estudios_previos_estado')->
        with('tbl_contratos_cdp.tbl_estudios_previos_municipios.tbl_municipios')->
        with('tbl_contratos_cdp.tbl_plan_anual_adquisicion')->
        with('tbl_contratos_cdp.tbl_recursos_humano_quien_suscribe')->
        with('tbl_contratos_cdp.tbl_recursos_humano_apoyo_tecnicos')->
        with('tbl_contratos_cdp.tbl_recursos_humano_apoyo_juridico')->
        where('id','=',$request->id)->
        first();
        $data->numero_contrato=$this->newNumeroContrato();
        return response()->json(['validate'=>true,'data'=>$data],200);
    }
    public function saveContrato(Request $request)
    {
        $cdp         = TblContratosCdp::where('estado','=','tramite')->where('id','=', $request->cdp_id)->first();
        $older=$cdp;
        try
        {
            try
            {
                $cdp->estado = 'aprobado';
                $cdp->save();
            }
            catch (\Throwable $th) {
                return response()->json(['validate'=>false,'data'=>$th->getMessage()],422);
            }

            $contratista = TblContratista::where('documento','=',$request->contratista['documento'])->first();
            $contratista                            = (is_null($contratista)?new TblContratista():TblContratista::find($contratista->id));
            $contratista->establecimiento_nombre    = $request->contratista['establecimiento_nombre'];
            $contratista->nombre_primero            = $request->contratista['nombre_primero'];
            $contratista->nombre_segundo            = $request->contratista['nombre_segundo'];
            $contratista->apellido_primero          = $request->contratista['apellido_primero'];
            $contratista->apellido_segundo          = $request->contratista['apellido_segundo'];
            $contratista->documento                 = $request->contratista['documento'];
            $contratista->documento                 = isset($request->contratista['documento_tipo'])?$request->contratista['documento_tipo']:null;
            $contratista->celular                   = $request->contratista['celular'];
            $contratista->email                     = $request->contratista['email'];
            $contratista->direccion                 = $request->contratista['direccion'];
            $contratista->municipio_id              = $request->contratista['municipio_id'];
            $contratista->save();

//supervisor y apoyo
//            $supervisor = RecursoHumano::where('documento','=', $request->supervisor['documento'])->get('id')->first();
//            $supervisor = (is_null($supervisor))?new RecursoHumano():RecursoHumano::find($supervisor->id);
//            $supervisor->nombre_primero   = $request->supervisor['nombre_primero'];
//            $supervisor->nombre_segundo   = $request->supervisor['nombre_segundo'];
//            $supervisor->apellido_primero = $request->supervisor['apellido_primero'];
//            $supervisor->apellido_segundo = $request->supervisor['apellido_segundo'];
//            $supervisor->documento        = $request->supervisor['documento'];
//            $supervisor->celular          = $request->supervisor['celular'];
//            $supervisor->email            = $request->supervisor['email'];
//            $supervisor->direccion        = $request->supervisor['direccion'];
//            $supervisor->activo           = 1;
//            $supervisor->cargo_id         = $request->supervisor['cargo_id'];
//            $supervisor->save();
//
//            $apoyo      = RecursoHumano::where('documento','=', $request->apoyo['documento'])->get('id')->first();
//            $apoyo      = (is_null($apoyo))?new RecursoHumano():RecursoHumano::find($apoyo->id);
//            $apoyo->nombre_primero   = $request->apoyo['nombre_primero'];
//            $apoyo->nombre_segundo   = $request->apoyo['nombre_segundo'];
//            $apoyo->apellido_primero = $request->apoyo['apellido_primero'];
//            $apoyo->apellido_segundo = $request->apoyo['apellido_segundo'];
//            $apoyo->documento        = $request->apoyo['documento'];
//            $apoyo->celular          = $request->apoyo['celular'];
//            $apoyo->email            = $request->apoyo['email'];
//            $apoyo->direccion        = $request->apoyo['direccion'];
//            $apoyo->cargo_id         = $request->apoyo['cargo_id'];
//            $apoyo->activo           = 1;
//            $apoyo->save();
//supervisor y apoyo

            $contrato=TblContrato::where('numero','=',$request->numero)->first();
            $contrato = is_null($contrato)?new TblContrato():TblContrato::find($contrato->id);
            $contrato->numero                           = $request->numero;
            $contrato->objeto                           = $request->objeto;
            $contrato->contratista_id                   = $contratista->id;
            $contrato->personeria_tipo                  = $request->personeria_tipo;
            $contrato->suscripcion_fecha                = date('Y-m-d',strtotime($request->suscripcion_fecha));
            $contrato->valor_contrato                   = str_replace(',','', $request->valor_contrato);
            $contrato->vigencia_desde                   = date('Y-m-d',strtotime($request->vigencia_desde));
            $contrato->vigencia_hasta                   = date('Y-m-d',strtotime($request->vigencia_hasta));
            $contrato->fecha_liquidacion                = is_null($request->fecha_liquidacion)?null:date('Y-m-d',strtotime($request->fecha_liquidacion));
            $contrato->forma_de_pago_id                 = $request->forma_de_pago_id;
            $contrato->fecha_corte                      = $request->fecha_corte;
            $contrato->procede_liquidacion              = $request->procede_liquidacion;
            //$contrato->recursos_humanos_supervisor_id   = $supervisor->id;
            //$contrato->recursos_humanos_apoyo_id        = $apoyo->id;
            $contrato->estudios_previos_id              = $cdp->estudios_previos_id;

            $contrato->save();
            $cuotas=$request->cuotas;
            foreach($cuotas as $temp)
            {
                $cuota = TblContratosCuota::where('acta','=',$temp['cuota'])->where('contratos_id','=',$contrato->id)->first();
                $cuota = (is_null($cuota))?new TblContratosCuota():TblContratosCuota::find($cuota->id);
                $cuota->cuota        = $temp['cuota'];
                $cuota->fecha        = date('Y-m-d',strtotime($temp['fecha']));
                $cuota->valor        = str_replace(',','', $temp['valor']);
                $cuota->acta         = $temp['acta'];
                $cuota->contratos_id = $contrato->id;
                $cuota->save();
            }

            foreach($request->supervisorsupervisores_id as $temp)
            {
                $data = new TblContratosXSupervision();
                $data->contrato_id=$contrato->id;
                $data->recursos_humanos_id=$temp;
                $data->save();
                unset($data);
            }
            foreach($request->apoyos_id as $temp)
            {
                $data = new TblContratosXApoyo();
                $data->contrato_id=$contrato->id;
                $data->recursos_humanos_id=$temp;
                $data->save();
                unset($data);
            }

            $estudio=EstudiosPreviosModel::find($cdp->estudios_previos_id);
            $estudio->estudios_previos_estado_id=2;
            $estudio->save();
            return response()->json(['validate'=>true,'data'=>$contrato],200);
        }
        catch (\Throwable $th) {
            $cdp         = TblContratosCdp::where('estado','=','aprobado')->where('id','=',$cdp->id)->first();
            $cdp->estado = 'tramite';
            $cdp->save();
            return response()->json(['validate'=>false,'data'=>$th->getMessage()],422);
        }
    }
    public function searchEstudioNecesidad(Request $request)
    {
        $data=EstudiosPreviosModel::
        where('codigo','LIKE','%'.trim($request->codigo).'%')->
        //whereNotIn('id',TblContratosCdp::pluck('estudios_previos_id'))->
        with('tbl_contratos_cdp')->
        with('tbl_estudios_previos_cuotas')->
        with('tbl_estudios_previos_tipo')->
        get();
        return response()->json(['validate'=>true,'data'=>$data],200);
    }

    public function todosContratos(Request $request)
    {
        if($request->buscar) {
            $valor = $request->buscar;
            $contratos = TblContrato::where('numero','LIKE','%'.$valor.'%')->orWhere('id','LIKE','%'.$valor.'%')
                ->orderBy('created_at', 'DESC')->get();
        } else {
            $contratos = TblContrato::orderBy('created_at', 'DESC')->get();
        }

        return $this->sendResponse($contratos, 'Lista de contratos');
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try
        {
            $search=TblContratosCdp::where('cdp_no','=',$request->cdp_no)->first();
            if(is_null($search))
            {
                $data = TblContratosCdp::where('estudios_previos_id','=',$request->estudios_previos_id)->first();
                $data = (is_null($data)?new TblContratosCdp():TblContratosCdp::find($data->id));
                $data->estudios_previos_id   = $request->estudios_previos_id;
                $data->contrato_id           = $request->contrato_id;
                $data->acta_autorizacion     = $request->acta_autorizacion;
                $data->acuerdo_numero        = $request->acuerdo_numero;
                $data->acuerdo_fecha         = is_null($request->acuerdo_fecha)?null:date('Y-m-d',strtotime($request->acuerdo_fecha));
                $data->solicitud_fecha       = is_null($request->solicitud_fecha)?null:date('Y-m-d',strtotime($request->solicitud_fecha));
                $data->cdp_no                = $request->cdp_no;
                $data->cdp_rubro             = $request->cdp_rubro;
                $data->cdp_fecha             = date('Y-m-d',strtotime($request->cdp_fecha));
                $data->cdp_valor             = str_replace(',', '', $request->cdp_valor);
                $data->save();
                return response()->json(['data'=>$data,'validate'=>true]);
            }
            else
            {
                return response()->json(['data'=>'El CDP <strong>'.$request->cdp_no.'</strong> ya se encuentra asignado','validate'=>false],200);
            }
        }
        catch (\Throwable $th)
        {
            return response()->json(['data'=>$th->getMessage(),'validate'=>false],422);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function showCDP($id)
    {
        try
        {
            $data=EstudiosPreviosModel::where('id','=',$id)->firstOrFail();
            return response()->json(['data'=>$data,'validate'=>true]);
        }
        catch (\Throwable $th)
        {
            return response()->json(['data'=>$th->getMessage(),'validate'=>false], 422);
        }
    }
    public function show($id)
    {
        try
        {
            $data=TblContrato::
            with('tbl_otros_si')->
            with('tbl_contratistas')->
            with('tbl_formas_de_pago')->
			with('tbl_contratos_cobertura')->
            //with('tbl_recursos_humanos_supervisor')->
            with('tbl_estudios_previos.tbl_contratos_cdp')->
			//with('tbl_recursos_humanos_supervisor.tbl_cargos')->
            where('id','=',$id)->
            firstOrFail();
            $data->supervisores=TblContratosXSupervision::where('contrato_id','=',$id)->with('tbl_recursos_humanos')->get();

            return response()->json(['data'=>$data,'validate'=>true]);
        }
        catch (\Throwable $th)
        {
            return response()->json(['data'=>$th->getMessage(),'validate'=>false], 422);
        }
    }
    public function porcentajeEjecucion(Request $request)
    {
        $data=TblContratatosPagosOrden::
        select(DB::raw('max(porcentaje) as porcentaje'),'valor_ejecutado')->
        join('tbl_revisiones_tramites','tbl_contratatos_pagos_orden.revisiones_tramites_id' , '=', 'tbl_revisiones_tramites.id')->
        where('tbl_revisiones_tramites.contrato_id', '=',$request->id_contrato)->
        groupBy('valor_ejecutado')->
        first();
        return $this->sendResponse($data);
    }
    public function liquidacionShow(Request $request)
    {
        $data=TblContrato::
        with('tbl_contratos_estados')->
        with('tbl_contratos_liquidacion')->
        get();
        return $this->sendResponse($data);
    }
    private function liquidacion($contrato_id)
    {
        return (object)
        [
            'liquidacion'=>TblContratoLiquidacion::
            where('contrato_id','=',$contrato_id)->
            first(),
            'revision_inicial'=>
            TblContratoLiquidacionRevisionInicial::
            select('tbl_contrato_liquidacion_revision_inicial.*')->
            join('tbl_contrato_liquidacion','tbl_contrato_liquidacion_revision_inicial.contrato_liquidacion_id', '=', 'tbl_contrato_liquidacion.id')->
            where('tbl_contrato_liquidacion.contrato_id','=',$contrato_id)->
            first(),
            'firma_gerencia'=>
            TblContratoLiquidacionRevisionInicial::
            select('tbl_contrato_liquidacion_firma_gerencia.*')->
            join('tbl_contrato_liquidacion','tbl_contrato_liquidacion_revision_inicial.contrato_liquidacion_id', '=', 'tbl_contrato_liquidacion.id')->
            join('tbl_contrato_liquidacion_firma_gerencia','tbl_contrato_liquidacion_firma_gerencia.contrato_liquidacion_revision_inicial_id', '=', 'tbl_contrato_liquidacion_revision_inicial.id')->
            where('tbl_contrato_liquidacion.contrato_id','=',$contrato_id)->
            first(),
            'observaciones_post_contracuales'=>TblContratoLiquidacionObservacionesPostcontracuales::
            where('contrato_id','=',$contrato_id)->
            first(),
        ];
    }
    public function detalleContrato($contrato_id)
    {
        $liquidaciones  = $this->liquidacion($contrato_id);
        $actaInicio     = TblActaInicio::where('contratos_id','=',$contrato_id)->first();
        $ActaSuspecion  = TblActaDeSuspension::where('contrato_id','=',$contrato_id)->get();
        $ActaReinicio   = TblActaDeReinicio::where('contrato_id','=',$contrato_id)->get();
        $ordenPago=TblRevisionTramite::
        select('tbl_contratatos_pagos_orden.*')->
        where('tbl_revisiones_tramites.contrato_id','=',$contrato_id)->
        join('tbl_contratatos_pagos_orden','tbl_revisiones_tramites.id', '=', 'tbl_contratatos_pagos_orden.revisiones_tramites_id')->
        get();

        $registroContable=TblRevisionTramite::
        select('tbl_contratatos_pagos_registro_contable.*','tbl_contratatos_pagos_orden.numero_orden_pago','tbl_contratatos_pagos_orden.valor')->
        where('tbl_revisiones_tramites.contrato_id','=',$contrato_id)->
        join('tbl_contratatos_pagos_orden','tbl_revisiones_tramites.id', '=', 'tbl_contratatos_pagos_orden.revisiones_tramites_id')->
        join('tbl_contratatos_pagos_registro_contable', 'tbl_contratatos_pagos_registro_contable.contratatos_pagos_orden_id', '=', 'tbl_contratatos_pagos_orden.id')->
        get();

        $registroPresupuestal=TblRevisionTramite::
        select('tbl_contratatos_pagos_registro_presupuestal.*','tbl_contratatos_pagos_orden.numero_orden_pago','tbl_contratatos_pagos_orden.valor','tbl_contratatos_pagos_registro_contable.numero_registro_contable')->
        where('tbl_revisiones_tramites.contrato_id','=',$contrato_id)->
        join('tbl_contratatos_pagos_orden','tbl_revisiones_tramites.id', '=', 'tbl_contratatos_pagos_orden.revisiones_tramites_id')->
        join('tbl_contratatos_pagos_registro_contable', 'tbl_contratatos_pagos_registro_contable.contratatos_pagos_orden_id', '=', 'tbl_contratatos_pagos_orden.id')->
        join('tbl_contratatos_pagos_registro_presupuestal','tbl_contratatos_pagos_registro_presupuestal.contratatos_pagos_registro_contable_id', '=', 'tbl_contratatos_pagos_registro_contable.id' )->
        get();

        $firmaAdministracion = TblRevisionTramite::
        select('tbl_contratatos_pagos_registro_presupuestal.*','tbl_contratatos_pagos_firma_administracion.*','tbl_contratatos_pagos_orden.numero_orden_pago','tbl_contratatos_pagos_orden.valor','tbl_contratatos_pagos_registro_contable.numero_registro_contable')->
        where('tbl_revisiones_tramites.contrato_id','=',$contrato_id)->
        join('tbl_contratatos_pagos_orden','tbl_revisiones_tramites.id', '=', 'tbl_contratatos_pagos_orden.revisiones_tramites_id')->
        join('tbl_contratatos_pagos_registro_contable', 'tbl_contratatos_pagos_registro_contable.contratatos_pagos_orden_id', '=', 'tbl_contratatos_pagos_orden.id')->
        join('tbl_contratatos_pagos_registro_presupuestal','tbl_contratatos_pagos_registro_presupuestal.contratatos_pagos_registro_contable_id', '=', 'tbl_contratatos_pagos_registro_contable.id' )->
        join('tbl_contratatos_pagos_firma_administracion','tbl_contratatos_pagos_firma_administracion.contratatos_pagos_registro_presupuestal_id', '=', 'tbl_contratatos_pagos_registro_presupuestal.id')->
        get();

        $firmaGerencia = TblRevisionTramite::
        select('tbl_contratatos_pagos_registro_presupuestal.*','tbl_contratatos_pagos_firma_gerencia.*','tbl_contratatos_pagos_orden.numero_orden_pago','tbl_contratatos_pagos_orden.valor','tbl_contratatos_pagos_registro_contable.numero_registro_contable')->
        where('tbl_revisiones_tramites.contrato_id','=',$contrato_id)->
        join('tbl_contratatos_pagos_orden','tbl_revisiones_tramites.id', '=', 'tbl_contratatos_pagos_orden.revisiones_tramites_id')->
        join('tbl_contratatos_pagos_registro_contable', 'tbl_contratatos_pagos_registro_contable.contratatos_pagos_orden_id', '=', 'tbl_contratatos_pagos_orden.id')->
        join('tbl_contratatos_pagos_registro_presupuestal','tbl_contratatos_pagos_registro_presupuestal.contratatos_pagos_registro_contable_id', '=', 'tbl_contratatos_pagos_registro_contable.id' )->
        join('tbl_contratatos_pagos_firma_administracion','tbl_contratatos_pagos_firma_administracion.contratatos_pagos_registro_presupuestal_id', '=', 'tbl_contratatos_pagos_registro_presupuestal.id')->
        join('tbl_contratatos_pagos_firma_gerencia', 'tbl_contratatos_pagos_firma_gerencia.contratatos_pagos_firma_administracion_id', '=', 'tbl_contratatos_pagos_firma_administracion.id')->
        get();

        $tesoreria = TblRevisionTramite::
        select('tbl_contratatos_pagos_registro_presupuestal.*','tbl_contratatos_pagos_firma_gerencia.*','tbl_contratatos_pagos_orden.numero_orden_pago','tbl_contratatos_pagos_orden.valor','tbl_contratatos_pagos_registro_contable.numero_registro_contable')->
        where('tbl_revisiones_tramites.contrato_id','=',$contrato_id)->
        join('tbl_contratatos_pagos_orden','tbl_revisiones_tramites.id', '=', 'tbl_contratatos_pagos_orden.revisiones_tramites_id')->
        join('tbl_contratatos_pagos_registro_contable', 'tbl_contratatos_pagos_registro_contable.contratatos_pagos_orden_id', '=', 'tbl_contratatos_pagos_orden.id')->
        join('tbl_contratatos_pagos_registro_presupuestal','tbl_contratatos_pagos_registro_presupuestal.contratatos_pagos_registro_contable_id', '=', 'tbl_contratatos_pagos_registro_contable.id' )->
        join('tbl_contratatos_pagos_firma_administracion','tbl_contratatos_pagos_firma_administracion.contratatos_pagos_registro_presupuestal_id', '=', 'tbl_contratatos_pagos_registro_presupuestal.id')->
        join('tbl_contratatos_pagos_firma_gerencia', 'tbl_contratatos_pagos_firma_gerencia.contratatos_pagos_firma_administracion_id', '=', 'tbl_contratatos_pagos_firma_administracion.id')->
        join('tbl_contratatos_pagos_tesoreria','tbl_contratatos_pagos_tesoreria.contratatos_pagos_firma_gerencia_id', '=', 'tbl_contratatos_pagos_firma_gerencia.id')->
        get();

        $tramites=TblRevisionTramite::
        orderBy('created_at')->
        where('contrato_id','=',$contrato_id)->
        get();



        $data=[
            'actaInicio'           => $actaInicio,
            'ActaSuspecion'        => $ActaSuspecion,
            'ActaReinicio'         => $ActaReinicio,
            'tramites'             => $tramites,
            'ordenPago'            => $ordenPago,
            'registroContable'     => $registroContable,
            'registroPresupuestal' => $registroPresupuestal,
            'firmaAdministracion'  => $firmaAdministracion,
            'firmaGerencia'        => $firmaGerencia,
            'tesoreria'            => $tesoreria,
            'liquidaciones'        => $liquidaciones,
        ];
        return $this->sendResponse($data);
    }

    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
