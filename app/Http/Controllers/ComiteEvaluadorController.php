<?php

namespace App\Http\Controllers;

use App\Models\TblComiteEvaluador;
use App\Models\TblContratacionMenorCuantiaXMiembroComiteEvaluador;
use App\Models\TblContratacionConvocatoriaPublicaXMiembroComiteEvaluador;
use Illuminate\Http\Request;

class ComiteEvaluadorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function SearchDocumento(Request $request)
    {
        return
        [
            'validate'=>true,
            'data'=>TblComiteEvaluador::where('documento','=',$request->documento)->first()
        ];
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $data = new TblComiteEvaluador();
            $data->nombre_primero   = $request->nombre_primero;                          
            $data->nombre_segundo   = $request->nombre_segundo;                          
            $data->apellido_primero = $request->apellido_primero;                            
            $data->apellido_segundo = $request->apellido_segundo;                            
            $data->documento        = $request->documento;                           
            $data->celular          = $request->celular;                         
            $data->email            = $request->email;                           
            $data->direccion        = $request->direccion;           
            $data->save();
            switch($request->tipo_contrato)
            {
                case 'convocatoriapublica':
                    $res = new TblContratacionConvocatoriaPublicaXMiembroComiteEvaluador();
                    $res->comite_evaluador_id = $data->id;
                    $res->estudios_previos_id = $request->id_menor_cuantia;
                    $res->save();
                break;
                case 'menorcuantia':
                    $res = new TblContratacionMenorCuantiaXMiembroComiteEvaluador();
                    $res->comite_evaluador_id = $data->id;
                    $res->estudios_previos_id = $request->id_menor_cuantia;
                    $res->save();
                break;
                default: return response( ['validate'=>false,'msj'=>'No exite'], 401); break;
            }
            return ['validate'=>true,'msj'=>'Guardado con éxito'];
        }
        catch (\Throwable $th)
        {
            return response( ['validate'=>false,'msj'=>$th->getMessage()], 401);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
