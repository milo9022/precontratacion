<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class TblContratosCobertura
 *
 * @property int $id
 * @property int $contratos_id
 * @property Carbon $fecha_aprobacion_poliza
 * @property string $clase_cobertura
 * @property Carbon $fecha_inicio
 * @property Carbon $fecha_fin
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @package App\Models
 */
class TblContratosCobertura extends Model
{
	protected $table = 'tbl_contratos_coberturas';

	protected $casts = [
		'contratos_id' => 'int'
	];

	protected $dates = [
		'fecha_aprobacion_poliza',
		'fecha_inicio',
		'fecha_fin'
	];

	protected $fillable = [
		'contratos_id',
		'user_id',
		'fecha_aprobacion_poliza',
		'clase_cobertura',
		'fecha_inicio',
		'fecha_fin'
	];

	public function tbl_contratos()
	{
		return $this->belongsTo('App\Models\TblContrato');
	}
}
