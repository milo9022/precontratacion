<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TblEstudiosPreviosCuotas extends Model
{
    protected $table = 'tbl_estudios_previos_cuotas';
    protected $fillable = ['id','estudios_previos_id','cuota','valor'];
}
