<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class TblListaChequeo
 * 
 * @property int $id
 * @property int $contratos_id
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @package App\Models
 */
class TblListaChequeo extends Model
{
	protected $table = 'tbl_lista_chequeo';

	protected $casts = [
		'contratos_id' => 'int'
	];

	protected $fillable = [
		'contratos_id'
	];
}
