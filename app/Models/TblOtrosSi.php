<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TblOtrosSi extends Model
{
    protected $table = 'tbl_otros_si';

    protected $fillable = [
      'user_id',
      'contrato_id',
      'desde',
      'hasta',
      'valor',
      'no_cdp',
      'fecha_cdp',
      'registro_presupuestal',
      'fecha_registro_presupuestal',
      'otras_modificaciones'
    ];
}
