<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class TblContrato
 * 
 * @property int $id
 * @property string $numero
 * @property Carbon $suscripcion_fecha
 * @property string $objeto
 * @property string $personeria_tipo
 * @property int $contratista_id
 * @property float $valor_contrato
 * @property Carbon $vigencia_desde
 * @property Carbon $vigencia_hasta
 * @property int $forma_de_pago_id
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @package App\Models
 */
class TblContrato extends Model
{
	protected $table = 'tbl_contratos';

	protected $casts = [
		'contratista_id' => 'int',
		'valor_contrato' => 'float',
		'forma_de_pago_id' => 'int'
	];

	protected $dates = [
		'suscripcion_fecha',
		'vigencia_desde',
		'vigencia_hasta',
		'fecha_liquidacion'
	];

	protected $fillable = [
		'numero',
		'suscripcion_fecha',
		'objeto',
		'personeria_tipo',
		'contratista_id',
		'valor_contrato',
		'vigencia_desde',
		'vigencia_hasta',
		'fecha_liquidacion',
		'forma_de_pago_id',
		'contratos_estados_id'
	];

	public function tbl_contratos_estados()
	{
        return $this->belongsTo('App\Models\TblContratosEstados','contratos_estados_id');
	}
	
	public function tbl_contratos_liquidacion()
	{
        return $this->hasOne('App\Models\TblContratoLiquidacion','contrato_id','id');
	}
	

	public function tbl_formas_de_pago()
	{
        return $this->belongsTo('App\Models\TblFomasPago','forma_de_pago_id');
	}
	
	public function tbl_estudios_previos()
	{
        return $this->hasOne('App\Models\EstudiosPreviosModel','id','estudios_previos_id');
	}

	public function tbl_contratos_cobertura()
    {
		return $this->hasOne('App\Models\TblContratosCobertura','contratos_id', 'id');
    }
	public function tbl_recursos_humanos_supervisor()
	{
	  return $this->hasOne('App\Models\RecursoHumano','id', 'recursos_humanos_supervisor_id');
	}

	public function tbl_recursos_humanos_apoyo()
  	{
		return $this->hasOne('App\Models\RecursoHumano','id', 'recursos_humanos_apoyo_id');
  	}

	public function tbl_contratistas()
  	{
		return $this->hasOne('App\Models\TblContratista','id', 'contratista_id');
  	}

	public function tbl_actas_de_inicio()
	{
	  return $this->hasOne('App\Models\TblActaInicio','contratos_id', 'id');
	}
  

	//Liquidaciones
	public function tbl_contrato_liquidacion()
	{
		return $this->hasOne('App\Models\TblContratoLiquidacion','contrato_id', 'id');
	}
	public function tbl_contrato_liquidacion_observaciones_postcontracuales()
	{
		return $this->hasOne('App\Models\TblContratoLiquidacionObservacionesPostcontracuales','contrato_id', 'id');
	}
	
	//Liquidaciones



	
  
  
  
	  

	
	public function tbl_revisiones_tramites()
	{
        return $this->hasMany('App\Models\TblRevisionTramite','contrato_id');
	}
	public function tbl_contratos_cuotas()
	{
        return $this->hasMany('App\Models\TblContratosCuota','contratos_id');
	}
	public function tbl_otros_si()
	{
        return $this->hasMany('App\Models\TblOtrosSi','contrato_id');
	}
	
	
  
}
