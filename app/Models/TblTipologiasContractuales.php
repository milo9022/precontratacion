<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TblTipologiasContractuales extends Model
{
    protected $table = 'tbl_tipologias_contractuales';
    protected $fillable = ['id','nombre'];
    
}
