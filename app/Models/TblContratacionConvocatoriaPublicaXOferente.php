<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TblContratacionConvocatoriaPublicaXOferente extends Model
{
    protected $table = 'tbl_contratacion_convocatoria_publica_x_oferente';
    protected $fillable = ['oferente_id','estudios_previos_id'];
    protected $hidden   = ['deleted_at','created_at','updated_at'];

    public function tbl_oferentes()
    {
        return $this->belongsTo('App\Models\TblOferentes','oferente_id');
    }
    public function tbl_estudios_previos()
    {
        return $this->belongsTo('App\Models\EstudiosPreviosModel','estudios_previos_id');
    }
}
