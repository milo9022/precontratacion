<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EstudiosPreviosEstadoModel extends Model
{
    protected $table      = 'tbl_estudios_previos_estado';
    protected $primaryKey = 'id';
    protected $fillable   = ['nombre'];
    protected $hidden     = ['created_at', 'updated_at'];
}
