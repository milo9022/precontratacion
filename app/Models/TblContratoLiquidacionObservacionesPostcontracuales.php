<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TblContratoLiquidacionObservacionesPostcontracuales extends Model
{
    protected $table = 'tbl_contrato_liquidacion_observaciones_postcontracuales';
    protected $fillable = [
        'contrato_id',
        'saldoliberar',
        'observaciones',
        'conceptocumplimiento',
    ];

    public function tbl_contratos()
    {
        return $this->belongsTo('App\Models\TblContrato','contrato_id');
    }
}
