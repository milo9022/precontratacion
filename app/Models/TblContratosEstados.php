<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class TblContratosEstados extends Model
{
    protected $table = 'tbl_contratos_estados';

	protected $fillable = [
		'id',
		'nombre'
	];
}
