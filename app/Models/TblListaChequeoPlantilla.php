<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class TblListaChequeoPlantilla
 * 
 * @property int $id
 * @property string $documentos
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @package App\Models
 */
class TblListaChequeoPlantilla extends Model
{
	protected $table = 'tbl_lista_chequeo_plantilla';

	protected $fillable = [
		'documentos'
	];
}
