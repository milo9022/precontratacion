<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class TblContratosXApoyo
 * 
 * @property int $id
 * @property int $contrato_id
 * @property int $recursos_humanos_id
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @package App\Models
 */
class TblContratosXApoyo extends Model
{
	protected $table = 'tbl_contratos_x_apoyo';

	protected $casts = [
		'contrato_id' => 'int',
		'recursos_humanos_id' => 'int'
	];

	protected $fillable = [
		'contrato_id',
		'recursos_humanos_id'
	];

	public function tbl_contrato()
    {
		return $this->belongsTo('App\Models\TblContrato','contrato_id');
	}
	public function tbl_recursos_humanos()
    {
		return $this->belongsTo('App\Models\RecursoHumano','recursos_humanos_id');
	}
	
}
