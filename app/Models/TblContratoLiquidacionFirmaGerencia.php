<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TblContratoLiquidacionFirmaGerencia extends Model
{
    protected $table = 'tbl_contrato_liquidacion_firma_gerencia';
    protected $fillable = [
        'contrato_liquidacion_revision_inicial_id',
        'fecha_ingreso',
        'fecha_egreso',
        'fecha_firma',
        'radicado',
        'estado'
    ];
}
