<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class TblListaChequeoDetalle
 * 
 * @property int $id
 * @property int $lista_chequeo_id
 * @property string $documentos
 * @property string $folio_no
 * @property int $aplica
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @package App\Models
 */
class TblListaChequeoDetalle extends Model
{
	protected $table = 'tbl_lista_chequeo_detalle';

	protected $casts = [
		'lista_chequeo_id' => 'int',
		'aplica' => 'int'
	];

	protected $fillable = [
		'lista_chequeo_id',
		'documentos',
		'folio_no',
		'aplica',
		'cumple_requisitos'
	];
}
