<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class TblContratatosPagosOrden
 * 
 * @property int $id
 * @property Carbon $fecha_ingreso
 * @property int $revisiones_tramites_id
 * @property float $valor
 * @property string $numero_orden_pago
 * @property float $valor_contrato
 * @property float $valor_ejecutado
 * @property float $valor_pagado
 * @property float $saldo_contrato
 * @property float $porcentaje
 * @property float $observaciones
 * @property float $fecha_egreso
 * @property string $estado
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property string $deleted_at
 *
 * @package App\Models
 */
class TblContratatosPagosOrden extends Model
{
	use SoftDeletes;
	protected $table = 'tbl_contratatos_pagos_orden';

	protected $casts = [
		'numero_orden_pago' => 'int',
		'revisiones_tramites_id' => 'int',
		'valor' => 'float',
		'valor_contrato' => 'float',
		'valor_ejecutado' => 'float',
		'valor_pagado' => 'float',
		'saldo_contrato' => 'float',
		'porcentaje' => 'float',
		'fecha_egreso' => 'float'
	];

	protected $dates = [
		'fecha_ingreso'
	];

	protected $fillable = [
		'numero_orden_pago',
		'fecha_ingreso',
		'revisiones_tramites_id',
		'valor',
		'numero_orden_pago',
		'valor_contrato',
		'valor_ejecutado',
		'valor_pagado',
		'saldo_contrato',
		'porcentaje',
		'observaciones',
		'fecha_egreso',
		'estado'//'aprobado','rechazado','tramite'
	];
	public function tbl_revision_tramite()
    {
        return $this->belongsTo('App\Models\TblRevisionTramite','revisiones_tramites_id');
	}
	public function tbl_revisiones_tramites()
	{
        return $this->hasMany('App\Models\TblRevisionTramite','contrato_id');
    }
}
