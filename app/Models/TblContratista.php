<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TblContratista extends Model
{
	use SoftDeletes;
	protected $table = 'tbl_contratistas';

	protected $casts = [
		'municipio_id' => 'int'
	];

	protected $fillable = [
		'establecimiento_nombre',
		'nombre_primero',
		'nombre_segundo',
		'apellido_primero',
		'apellido_segundo',
		'documento',
		'direccion',
		'celular',
		'email',
		'municipio_id'
	];

	public function tbl_contrato()
  {
		return $this->belongsTo('App\Models\TblContrato');
  }
}
