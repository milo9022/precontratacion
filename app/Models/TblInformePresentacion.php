<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TblInformePresentacion extends Model
{
    protected $table = 'tbl_informes_supervision';
    
    protected $primaryKey = 'id';

    protected $fillable = ['id','fecha_presentacion','motivo_devolucion','fecha_devolucion','fecha_reingreso','informe_id'];
}
