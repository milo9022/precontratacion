<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TblContratoLiquidacionRevisionInicial extends Model
{
    protected $table = 'tbl_contrato_liquidacion_revision_inicial';
    protected $fillable = [
		'contrato_liquidacion_id',
        'fecha_egreso',
        'fecha_ingreso',
        'radicado',
        'estado',
    ];
    public function tbl_contrato_liquidacion()
    {
        return $this->belongsTo('App\Models\TblContratoLiquidacion','contrato_liquidacion_id');
    }
    public function tbl_contrato_liquidacion_firma_gerencia()
	{
		return $this->hasOne('App\Models\TblContratoLiquidacionFirmaGerencia','contrato_liquidacion_revision_inicial_id', 'id');
	}
    
}
