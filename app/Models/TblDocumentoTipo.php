<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
class TblDocumentoTipo extends Model
{

    protected $table = 'tbl_documento_tipos';
    protected $hidden = ['created_at', 'updated_at'];
    protected $fillable = ['id','nombre','nombre_corto'];
}
