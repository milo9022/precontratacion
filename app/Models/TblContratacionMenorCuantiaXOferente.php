<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TblContratacionMenorCuantiaXOferente extends Model
{
    protected $table = 'tbl_contratacion_menor_cuantia_x_oferente';
    protected $fillable = ['fecha_remision_invitaciones','oferente_id','estudios_previos_id'];
    protected $hidden   = ['deleted_at','created_at','updated_at'];

    public function tbl_oferentes()
    {
        return $this->belongsTo('App\Models\TblOferentes','oferente_id');
    }
}
