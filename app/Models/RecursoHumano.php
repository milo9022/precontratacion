<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RecursoHumano extends Model
{
    protected $table = 'tbl_recursos_humanos';

    protected $fillable = [
        'nombre_primero',
        'nombre_segundo',
        'apellido_primero',
        'apellido_segundo',
        'documento',
        'direccion',
        'documento_tipo_id',
        'celular',
        'activo',
        'email',
        'cargo_id',
        'user_id'
    ];

    public function tbl_cargos()
    {
        return $this->belongsTo('App\Models\Cargo','cargo_id');
    }
    public function tbl_documento_tipo()
    {
        return $this->belongsTo('App\Models\TblDocumentoTipo','documento_tipo_id');
    }

    public function tbl_contrato()
    {
  		return $this->belongsTo('App\Models\TblContrato');
    }
}
