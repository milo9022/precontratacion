<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EstudiosPreviosModel extends Model
{
    protected $table = 'tbl_estudios_previos';
    protected $fillable = ['id','codigo','vigencia','garantias_id','objeto','estudios_previos_tipo_id','plan_anual_adquisicion_id','recursos_humano_quien_suscribe_id','recursos_humano_apoyo_tecnicos_id','recursos_humano_apoyo_juridico_id','fecha_estudios_previos','experiencia','idoneidad','otros','tipologias_contractuales_id','plazo_ejecucion','valor_numero','valor_letras','forma_de_pago_id','numero_cuotas','lugar_ejecucion','estudios_previos_estado_id'];
    protected $hidden   = ['created_at', 'updated_at'];
    #region belongsTo
    public function tbl_estudios_previos_tipo()
    {
        return $this->belongsTo('App\Models\EstudiosPreviosTipoModel','estudios_previos_tipo_id');
    }
    public function tbl_estudios_previos_estado()
    {
        return $this->belongsTo('App\Models\EstudiosPreviosEstadoModel','estudios_previos_estado_id');
    }
    public function tbl_plan_anual_adquisicion()
    {
        return $this->belongsTo('App\Models\TblPlanAnualAdquisicion','plan_anual_adquisicion_id');
    }
    public function tbl_formas_de_pago()
    {
        return $this->belongsTo('App\Models\TblFormasDePago','forma_de_pago_id');
    }
    public function tbl_recursos_humano_quien_suscribe()
    {
        return $this->belongsTo('App\Models\RecursoHumano','recursos_humano_quien_suscribe_id');
    }
    public function tbl_recursos_humano_apoyo_tecnicos()
    {
        return $this->belongsTo('App\Models\RecursoHumano','recursos_humano_apoyo_tecnicos_id');
    }
    public function tbl_recursos_humano_apoyo_juridico()
    {
        return $this->belongsTo('App\Models\RecursoHumano','recursos_humano_apoyo_juridico_id');
    }
    #endregion
    
    #region hashMany
    public function tbl_estudios_previos_cuotas()
    {
        return $this->hasMany('App\Models\TblEstudiosPreviosCuotas','estudios_previos_id');
    }
    public function tbl_estudios_previos_municipios()
    {
        return $this->hasMany('App\Models\TblEstudiosPreviosMunicipios','estudios_previos_id');
    }
    public function tbl_oferentes_menor_cuantia()
    {
        return $this->hasMany('App\Models\TblContratacionMenorCuantiaXOferente','estudios_previos_id');
    }
    public function tbl_oferentes_convocatoria_publica()
    {
        return $this->hasMany('App\Models\TblContratacionConvocatoriaPublicaXOferente','estudios_previos_id');
    }
    

    public function tbl_estdios_previos_apoyo_juridico()
    {
        return $this->hasMany('App\Models\TblEstdiosPreviosApoyoJuridico','estudios_previos_id');
    }
    public function tbl_estdios_previos_apoyos_tecnicos()
    {
        return $this->hasMany('App\Models\TblEstdiosPreviosApoyosTecnico','estudios_previos_id');
    }
    public function tbl_estdios_previos_quien_suscribe()
    {
        return $this->hasMany('App\Models\TblEstdiosPreviosQuienSuscribe','estudios_previos_id');
    }
    
    
    
    #endregion

    #region oneToOne
    
    public function tbl_contratos_cdp()
    {
        return $this->hasOne('App\Models\TblContratosCdp', 'estudios_previos_id','id');
    }
    #endregion

    
}
