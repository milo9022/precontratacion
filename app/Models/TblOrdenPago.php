<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TblOrdenPago extends Model
{
    protected $table = 'tbl_ordenes_pago';
    
    protected $primaryKey = 'id';

    protected $fillable = ['id','fecha_orden','numero_orden','valor_neto','valor_descuento'];
    
    public function tbl_departamento()
    {
        return $this->belongsTo('App\Models\TblDepartamento','id_departamento');
    }
}
