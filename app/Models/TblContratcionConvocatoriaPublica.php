<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TblContratcionConvocatoriaPublica extends Model
{
    protected $table = 'tbl_contratacion_convocatoria_publica';
    protected $fillable = ['estudios_previos_id','recomendacion_a_contratar','observaciones','fecha_remision_invitaciones','fecha_evaluacion_inicial','fecha_evaluacion_final'];
    protected $hidden   = ['created_at', 'updated_at','deleted_at'];

    public function tbl_estudios_previos()
    {
        return $this->belongsTo('App\Models\EstudiosPreviosModel','estudios_previos_id');
    }
}
