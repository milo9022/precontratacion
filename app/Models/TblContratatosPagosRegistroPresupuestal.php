<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class TblContratatosPagosRegistroPresupuestal
 * 
 * @property int $id
 * @property int $contratatos_pagos_registro_contable_id
 * @property Carbon $fecha_ingreso
 * @property string $codigo_presupuestal
 * @property Carbon $fecha_egreso
 * @property string $estado
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property string $deleted_at
 *
 * @package App\Models
 */
class TblContratatosPagosRegistroPresupuestal extends Model
{
	use SoftDeletes;
	protected $table = 'tbl_contratatos_pagos_registro_presupuestal';

	protected $casts = [
		'contratatos_pagos_registro_contable_id' => 'int'
	];

	protected $dates = [
		'fecha_ingreso',
		'fecha_egreso'
	];

	protected $fillable = [
		'contratatos_pagos_registro_contable_id',
		'fecha_ingreso',
		'codigo_presupuestal',
		'fecha_egreso',
		'estado'
	];

	public function tbl_contratatos_pagos_registro_contable()
    {
        return $this->belongsTo('App\Models\TblContratatosPagosRegistroContable','contratatos_pagos_registro_contable_id');
    }
}
