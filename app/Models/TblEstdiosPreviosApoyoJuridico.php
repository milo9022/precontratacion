<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class TableTblEstdiosPreviosApoyoJuridico
 * 
 * @property int $id
 * @property int $recursos_humanos_id
 * @property int $estudios_previos_id
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @package App\Models
 */
class TblEstdiosPreviosApoyoJuridico extends Model
{
	protected $table = 'tbl_estdios_previos_apoyo_juridico';

	protected $casts = [
		'recursos_humanos_id' => 'int',
		'estudios_previos_id' => 'int'
	];

	protected $fillable = [
		'recursos_humanos_id',
		'estudios_previos_id'
	];
	public function tbl_recursos_humano()
    {
        return $this->belongsTo('App\Models\RecursoHumano','recursos_humanos_id');
    }
}
