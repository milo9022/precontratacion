<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class TblContratatosPagosTesorerium
 * 
 * @property int $id
 * @property int $contratatos_pagos_firma_gerencia_id
 * @property Carbon $fecha_ingreso
 * @property Carbon $fecha_egreso
 * @property string $estado
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property string $deleted_at
 *
 * @package App\Models
 */
class TblContratatosPagosTesorerium extends Model
{
	use SoftDeletes;
	protected $table = 'tbl_contratatos_pagos_tesoreria';

	protected $casts = [
		'contratatos_pagos_firma_gerencia_id' => 'int'
	];

	protected $dates = [
		'fecha_ingreso',
		'fecha_egreso'
	];

	protected $fillable = [
		'contratatos_pagos_firma_gerencia_id',
		'fecha_ingreso',
		'fecha_egreso',
		'estado'
	];
	public function tbl_contratatos_pagos_firma_gerencia()
    {
        return $this->belongsTo('App\Models\TblContratatosPagosFirmaGerencium','contratatos_pagos_firma_gerencia_id');
    }
}
