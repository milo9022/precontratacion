<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class TblContratosCuota
 * 
 * @property int $id
 * @property int $cuota
 * @property int $contratos_id
 * @property Carbon $fecha
 * @property float $valor
 * @property string $acta
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @package App\Models
 */
class TblContratosCuota extends Model
{
	protected $table = 'tbl_contratos_cuotas';

	protected $casts = [
		'cuota' => 'int',
		'contratos_id' => 'int',
		'valor' => 'float'
	];

	protected $dates = [
		'fecha'
	];

	protected $fillable = [
		'cuota',
		'contratos_id',
		'fecha',
		'valor',
		'acta'
	];
}
