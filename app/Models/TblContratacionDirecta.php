<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TblContratacionDirecta extends Model
{
    protected $table = 'tbl_contratacion_directa';
    protected $fillable = ['causal','observacion','estudios_previos_id'];
    protected $hidden   = ['deleted_at','created_at','updated_at'];
    public function tbl_estudios_previos()
    {
        return $this->belongsTo('App\Models\EstudiosPreviosModel','estudios_previos_id');
    }
}
