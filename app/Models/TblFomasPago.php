<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TblFomasPago extends Model
{
    protected $table = 'tbl_formas_de_pago';
    protected $fillable = ['id','nombre'];
}
