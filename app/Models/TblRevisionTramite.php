<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TblRevisionTramite extends Model
{
    protected $table = 'tbl_revisiones_tramites';
    
    protected $primaryKey = 'id';

    protected $fillable = ['id','fecha_presentacion','motivo_devolucion','fecha_devolucion','fecha_reingreso','fecha_egreso','contrato_id'];

    public function tbl_contratos()
    {
  		return $this->belongsTo('App\Models\TblContrato','contrato_id');
    }
}
