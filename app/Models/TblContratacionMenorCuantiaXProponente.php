<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TblContratacionMenorCuantiaXProponente extends Model
{
    protected $table = 'tbl_contratacion_menor_cuantia_x_proponente';
    protected $fillable = ['proponente_id','estudios_previos_id'];
    protected $hidden   = ['deleted_at','created_at','updated_at'];

    public function tbl_proponente()
    {
        return $this->belongsTo('App\Models\TblProponentes','proponente_id');
    }
    public function tbl_estudios_previos()
    {
        return $this->belongsTo('App\Models\EstudiosPreviosModel','estudios_previos_id');
    }
}
