<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TblComiteEvaluador extends Model
{
    protected $table = 'tbl_comite_evaluador';
    protected $primaryKey = 'id';
    protected $fillable = ['nombre_primero','nombre_segundo','apellido_primero','apellido_segundo','documento','celular','email','direccion'];
    protected $hidden     = ['created_at', 'updated_at'];
}
