<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TblActaDeReinicio extends Model
{
    protected $table = 'tbl_actas_de_reinicio';

    protected $fillable = ['user_id','contrato_id', 'fecha', 'motivo'];
}
