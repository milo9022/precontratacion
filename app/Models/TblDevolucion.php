<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TblDevolucion extends Model
{
    protected $table = 'tbl_devoluciones';
    
    protected $primaryKey = 'id';

    protected $fillable = ['id','fecha_ingreso','motivo_devolucion','fecha_devolucion','fecha_egreso','tipo_from_id','tipo_to_id','user_id','id_tercero'];
    public function tbl_tipo_devolucion_to()
    {
        return $this->belongsTo('App\Models\TblTipoDevolucion','tipo_to_id');
    }
    public function tbl_tipo_devolucion_from()
    {
        return $this->belongsTo('App\Models\TblTipoDevolucion','tipo_from_id');
    }
    
    
}
