<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TblSupervision extends Model
{
    protected $table = 'tbl_informes_supervision';
    
    protected $primaryKey = 'id';

    protected $fillable = ['id','fecha_suscripcion','periodo','valor_avalado','observaciones','contrato_id','cuota_id'];

    public function scopeContrato($query, $value)
    {
    	return $query->where('contrato_id', $value);
    }
}
