<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TblPermisionRoles extends Model
{
    protected $table      = 'tbl_permisions_roles';
    protected $primaryKey = 'id';
    protected $fillable   = ['id_rol','id_permisions'];
    protected $hidden     = ['created_at', 'updated_at'];
}
