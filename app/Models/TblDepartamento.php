<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TblDepartamento extends Model
{
    protected $table      = 'tbl_departamentos';
    protected $primaryKey = 'id';
    protected $fillable   = ['nombre'];
    protected $hidden     = ['created_at', 'updated_at'];
}
