<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TblGarantias extends Model
{
    protected $table = 'tbl_garantias';
    protected $fillable = ['id','nombre'];
}
