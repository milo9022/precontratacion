<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TblMunicipios extends Model
{
    protected $table      = 'tbl_municipios';
    protected $primaryKey = 'id';
    protected $hidden     = ['created_at', 'updated_at'];
    protected $fillable   = ['nombre','id_departamento'];
    public function tbl_departamento()
    {
        return $this->belongsTo('App\Models\TblDepartamento','id_departamento');
    }
}
