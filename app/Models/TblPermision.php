<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TblPermision extends Model
{
    protected $table      = 'tbl_permisions';
    protected $primaryKey = 'id';
    protected $fillable   = ['nombre'];
    protected $hidden     = ['created_at', 'updated_at'];
}
