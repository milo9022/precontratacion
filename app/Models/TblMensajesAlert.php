<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class TblMensajesAlert
 * 
 * @property int $id
 * @property string $mensaje
 * @property int $role_id
 * @property string $tipo
 * @property int $leido
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @package App\Models
 */
class TblMensajesAlert extends Model
{
	protected $table = 'tbl_mensajes_alert';

	protected $casts = [
		'role_id' => 'int',
		'leido' => 'int'
	];

	protected $fillable = [
		'mensaje',
		'role_id',
		'user_id',
		'tipo',
		'leido'
	];
}
