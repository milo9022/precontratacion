<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EstudiosPreviosTipoModel extends Model
{
    protected $table      = 'tbl_estudios_previos_tipo';
    protected $primaryKey = 'id';
    protected $fillable   = ['nombre'];
    protected $hidden     = ['created_at', 'updated_at'];
}
