<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class TblImpuesto
 * 
 * @property int $id
 * @property string $nombre
 * @property float $valor
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @package App\Models
 */
class TblImpuesto extends Model
{
	protected $table = 'tbl_impuestos';

	protected $casts = [
		'valor' => 'float'
	];

	protected $fillable = [
		'nombre',
		'valor'
	];
}
