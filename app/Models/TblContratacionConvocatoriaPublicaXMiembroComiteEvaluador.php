<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TblContratacionConvocatoriaPublicaXMiembroComiteEvaluador extends Model
{
    protected $table = 'tbl_contratacion_convocatoria_publica_x_miembro_comite_evaluador';
    protected $fillable = ['comite_evaluador_id','estudios_previos_id'];
    protected $hidden   = ['deleted_at','created_at','updated_at'];

    public function tbl_comite_evaluador()
    {
        return $this->belongsTo('App\Models\TblComiteEvaluador','comite_evaluador_id');
    }
}
