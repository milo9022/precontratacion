<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TblTipoDevolucion extends Model
{
    protected $table = 'tbl_tipo_devolucion';
    
    protected $primaryKey = 'id';

    protected $fillable = ['id','nombre'];
}
