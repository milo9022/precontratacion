<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TblActaInicio extends Model
{
    protected $table = 'tbl_actas_de_inicio';

    protected $fillable = [
      'user_id',
      'supervisor_id',
      'contratista_id',
      'contratos_id',
      'fecha_suscripcion'
    ];

    public function tbl_recursos_humanos_supervisor()
    {
  		return $this->belongsTo('App\Models\RecursoHumano','supervisor_id');
    }
    public function tbl_contratista()
    {
  		return $this->belongsTo('App\Models\TblContratista','contratista_id');
    }
    public function tbl_contratos()
    {
  		return $this->belongsTo('App\Models\TblContrato','contratos_id');
    }
}
