<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TblPlanAnualAdquisicion extends Model
{
    use SoftDeletes; //Implementamos 
    protected $table = 'tbl_plan_anual_adquisicion';
    protected $fillable = [
        'id',
        'codigos',
        'nombre',
        'fecha_estimada_inicio_seleccion',
        'duracion_estimada_contrato',
        'modalidad_seleccion',
        'fuente_recursos',
        'valor_estimado',
        'valor_estimado_vigencia_actual',
        'require_vigencia_futura',
        'estado_solicitud_vigencia_futura',
        'datos_contacto_responsable'
    ];
}
