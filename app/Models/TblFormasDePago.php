<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class TblFormasDePago
 * 
 * @property int $id
 * @property string $nombre
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @package App\Models
 */
class TblFormasDePago extends Model
{
	protected $table = 'tbl_formas_de_pago';

	protected $fillable = [
		'nombre'
	];
}
