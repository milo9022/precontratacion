<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class TblContratosCdp
 * 
 * @property int $id
 * @property int $estudios_previos_id
 * @property int $contrato_id
 * @property string $acta_autorizacion
 * @property string $acuerdo_numero
 * @property Carbon $acuerdo_fecha
 * @property Carbon $solicitud_fecha
 * @property int $cdp_no
 * @property string $cdp_rubro
 * @property Carbon $cdp_fecha
 * @property float $cdp_valor
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @package App\Models
 */
class TblContratosCdp extends Model
{
	protected $table = 'tbl_contratos_cdp';

	protected $casts = [
		'estudios_previos_id' => 'int',
		'contrato_id' => 'int',
		'cdp_no' => 'int',
		'cdp_valor' => 'float'
	];

	protected $dates = [
		'acuerdo_fecha',
		'solicitud_fecha',
		'cdp_fecha'
	];

	protected $fillable = [
		'estudios_previos_id',
		'contrato_id',
		'acta_autorizacion',
		'acuerdo_numero',
		'acuerdo_fecha',
		'solicitud_fecha',
		'cdp_no',
		'cdp_rubro',
		'cdp_fecha',
		'cdp_valor'
	];
    #region oneToOne
    public function tbl_contratos_cdp()
    {
		return $this->hasOne('App\Models\EstudiosPreviosModel', 'id','estudios_previos_id');
    }
	#endregion
	
	#region belongsTo
	public function tbl_estudios_previos()
    {
		return $this->belongsTo('App\Models\EstudiosPreviosModel','estudios_previos_id');
    }
	#endregion
}
