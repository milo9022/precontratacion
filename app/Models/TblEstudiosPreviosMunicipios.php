<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TblEstudiosPreviosMunicipios extends Model
{
    protected $table = 'tbl_estudios_previos_municipios';
    protected $fillable = ['id','estudios_previos_id','municipio_id'];
    public function tbl_municipios()
    {
        return $this->belongsTo('App\Models\TblMunicipios','municipio_id');
    }
}
