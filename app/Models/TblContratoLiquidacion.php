<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TblContratoLiquidacion extends Model
{
	protected $table = 'tbl_contrato_liquidacion';
    protected $fillable = [
		'contrato_id',
		'tipo_liquidacion',
		'observacion'
	];

	public function tbl_contrato_liquidacion_revision_inicial()
	{
		return $this->hasOne('App\Models\TblContratoLiquidacionRevisionInicial','contrato_liquidacion_id', 'id');
	}


}
