<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TblActaDeSuspension extends Model
{
    protected $table = 'tbl_actas_de_suspension';

    protected $fillable = [
      'user_id',
      'contrato_id',
      'fecha',
      'motivo',
      'tiempo',
      'tipo_tiempo',
    ];
}
