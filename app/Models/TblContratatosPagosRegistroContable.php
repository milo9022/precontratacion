<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class TblContratatosPagosRegistroContable
 * 
 * @property int $id
 * @property Carbon $fecha_ingreso
 * @property int $contratatos_pagos_orden_id
 * @property string $numero_registro_contable
 * @property Carbon $fecha_egreso
 * @property string $estado
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @package App\Models
 */
class TblContratatosPagosRegistroContable extends Model
{
	protected $table = 'tbl_contratatos_pagos_registro_contable';

	protected $casts = [
		'contratatos_pagos_orden_id' => 'int'
	];

	protected $dates = [
		'fecha_ingreso',
		'fecha_egreso'
	];

	protected $fillable = [
		'fecha_ingreso',
		'contratatos_pagos_orden_id',
		'numero_registro_contable',
		'fecha_egreso',
		'estado'//'aprobado', 'tramite', 'devuelto'
	];
	public function tbl_contratatos_pagos_orden()
    {
        return $this->belongsTo('App\Models\TblContratatosPagosOrden','contratatos_pagos_orden_id');
    }
}
