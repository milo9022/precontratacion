<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TblProponentes extends Model
{
    protected $table      = 'tbl_proponente';
    protected $primaryKey = 'id';
    protected $fillable   = ['nombre_primero','nombre_segundo','apellido_primero','apellido_segundo','documento','celular','email','direccion'];
    protected $hidden     = ['created_at', 'updated_at'];
}
