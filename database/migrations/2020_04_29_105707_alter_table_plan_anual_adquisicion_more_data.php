<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTablePlanAnualAdquisicionMoreData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tbl_plan_anual_adquisicion', function (Blueprint $table) {
            $table->text('codigos')->change();
            $table->text('nombre')->change();
            $table->date  ('fecha_estimada_inicio_seleccion');
            $table->integer('duracion_estimada_contrato');
            $table->string('modalidad_seleccion');
            $table->string('fuente_recursos');
            $table->double('valor_estimado')->nullable();
            $table->double('valor_estimado_vigencia_actual')->nullable();
            $table->string('require_vigencia_futura');
            $table->string('estado_solicitud_vigencia_futura');
            $table->string('datos_contacto_responsable');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tbl_plan_anual_adquisicion', function (Blueprint $table) {
            //
        });
    }
}
