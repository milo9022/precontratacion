<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblRevisionesTramitesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_revisiones_tramites', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->date('fecha_presentacion');
            $table->text('motivo_devolucion')->nullable();
            $table->date('fecha_devolucion')->nullable();
            $table->date('fecha_reingreso')->nullable();
            $table->date('fecha_egreso')->nullable();
            $table->unsignedBigInteger('contrato_id')->nullable();
            $table->unsignedBigInteger('user_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_revisiones_tramites');
    }
}
