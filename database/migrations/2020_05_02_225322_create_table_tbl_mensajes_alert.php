<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTblMensajesAlert extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_mensajes_alert', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('mensaje');
            $table->integer('user_id');
            $table->integer('role_id');
            $table->enum('tipo',['warning','success','danger']);
            $table->integer('noleido')->default(1);//1 leido, 0 no leido
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_mensajes_alert');
    }
}
