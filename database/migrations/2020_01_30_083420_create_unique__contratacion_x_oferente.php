<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUniqueContratacionXOferente extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tbl_contratacion_menor_cuantia_x_oferente', function (Blueprint $table) {
            $table->unique('oferente_id', 'estudios_previos_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tbl_contratacion_menor_cuantia_x_oferente', function (Blueprint $table) {
            //
        });
    }
}
