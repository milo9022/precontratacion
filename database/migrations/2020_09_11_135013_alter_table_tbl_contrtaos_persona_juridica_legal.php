<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableTblContrtaosPersonaJuridicaLegal extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tbl_contratos', function (Blueprint $table) {
            $table->integer('persona_juridica_id')->comment('id a tbl_persona_juridica_representante')->nullable();
            $table->integer('representante_legal_id')->comment('id a tbl_persona_juridica_representante')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tbl_contratos', function (Blueprint $table) {
            $table->dropColumn('persona_juridica_id');
            $table->dropColumn('representante_legal_id');

        });
    }
}
