<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TblContratacionConvocatoriaPublicaXMiembroComiteEvaluador extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_contratacion_convocatoria_publica_x_miembro_comite_evaluador', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('comite_evaluador_id');
            $table->unsignedBigInteger('estudios_previos_id');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_contratacion_convocatoria_publica_x_miembro_comite_evaluador');
    }
}
