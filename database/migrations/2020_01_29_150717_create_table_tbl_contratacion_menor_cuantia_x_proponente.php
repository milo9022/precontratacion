<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTblContratacionMenorCuantiaXProponente extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_contratacion_menor_cuantia_x_proponente', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('proponente_id');
            $table->unsignedBigInteger('estudios_previos_id');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_contratacion_menor_cuantia_x_proponente');
    }
}
