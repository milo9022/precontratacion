<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOtrosSiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_otros_si', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('contrato_id');
            $table->unsignedBigInteger('user_id');
            $table->date('desde')->nullable();
            $table->date('hasta')->nullable();
            $table->float('valor', 12,2)->nullable();
            $table->string('no_cdp')->nullable();
            $table->date('fecha_cdp')->nullable();
            $table->integer('registro_presupuestal')->nullable();
            $table->date('fecha_registro_presupuestal')->nullable();
            $table->text('otras_modificaciones')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_otros_si');
    }
}
