<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TblMunicipiosTable extends Migration
{
    public $set_schema_table = 'tbl_municipios';
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('nombre');
            $table->unsignedBigInteger('id_departamento');

            $table->index(["id_departamento"], 'id_departamento');
            $table->nullableTimestamps();
        });
    }
    
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
