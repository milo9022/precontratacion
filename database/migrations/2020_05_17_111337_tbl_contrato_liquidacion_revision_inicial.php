<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TblContratoLiquidacionRevisionInicial extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_contrato_liquidacion_revision_inicial', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('contrato_liquidacion_id');
            $table->date('fecha_ingreso');
            $table->date('fecha_egreso')->nullable();
            $table->integer('radicado')->nullable();
            $table->enum('estado',['aprobado','rechazado','tramite','devuelto'])->default('tramite');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_contrato_liquidacion_revision_inicial');
    }
}
