<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableTblContratosContratistasSupervisor extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tbl_contratos', function (Blueprint $table) {
            $table->unsignedBigInteger('recursos_humanos_supervisor_id')->nullable();
            $table->unsignedBigInteger('recursos_humanos_apoyo_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tbl_contratos', function (Blueprint $table) {
            if (Schema::hasColumn('tbl_contratos', 'recursos_humanos_supervisor_id'))
            {
                $table->dropColumn('recursos_humanos_supervisor_id');
            }
            if (Schema::hasColumn('tbl_contratos', 'recursos_humanos_apoyo_id'))
            {
                $table->dropColumn('recursos_humanos_apoyo_id');
            }
            
        });
    }
}
