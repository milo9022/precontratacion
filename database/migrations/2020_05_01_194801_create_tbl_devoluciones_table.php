<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblDevolucionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_devoluciones', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->date('fecha_ingreso')->nullable();
            $table->text('motivo_devolucion')->nullable();
            $table->date('fecha_devolucion')->nullable();
            $table->date('fecha_egreso')->nullable();
            $table->unsignedBigInteger('tipo_from_id')->nullable();//Quien lo devuelve
            $table->unsignedBigInteger('tipo_to_id')->nullable();//Quien lo recive
            $table->unsignedBigInteger('user_id')->nullable();
            $table->unsignedBigInteger('user_to_id')->nullable();//Usuario a quien le llegaría
            $table->unsignedBigInteger('id_tercero')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_devoluciones');
    }
}
