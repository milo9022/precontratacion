<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToTblRevisionesTramitesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('tbl_revisiones_tramites', function(Blueprint $table)
		{
			$table->foreign('contrato_id')->references('id')->on('tbl_contratos')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('tbl_revisiones_tramites', function(Blueprint $table)
		{
			$table->dropForeign('tbl_revisiones_tramites_contrato_id_foreign');
		});
	}

}
