<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTblContratosCobertura extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_contratos_coberturas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('contratos_id')->unique();
            $table->unsignedBigInteger('user_id');
            $table->date('fecha_aprobacion_poliza');
            $table->string('clase_cobertura');
            $table->date('fecha_inicio');
            $table->date('fecha_fin');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_contratos_coberturas');
    }
}
