<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TblContratacionConvocatoriaPublica extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_contratacion_convocatoria_publica', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('estudios_previos_id');
            $table->text('recomendacion_a_contratar')->nullable();
            $table->text('observaciones')->nullable();
            $table->date('fecha_remision_invitaciones');
            $table->date('fecha_evaluacion_inicial')->nullable();
            $table->date('fecha_evaluacion_final')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_contratacion_convocatoria_publica');
    }
}
