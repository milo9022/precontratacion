<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTblContratosPagosRegistroPresupuestal extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_contratatos_pagos_registro_presupuestal', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('contratatos_pagos_registro_contable_id');
            $table->date('fecha_ingreso');
            $table->string('codigo_presupuestal');
            $table->date('fecha_egreso')->nullable();
            $table->enum('estado',['aprobado','rechazado','tramite','devuelto'])->default('tramite');
            $table->timestamps();
			$table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_contratatos_pagos_registro_presupuestal');
    }
}
