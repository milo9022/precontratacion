<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTblContratosCuotas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_contratos_cuotas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('cuota');
            $table->unsignedBigInteger('contratos_id');
            $table->date('fecha');
            $table->double('valor');
            $table->text('acta')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_contratos_cuotas');
    }
}
