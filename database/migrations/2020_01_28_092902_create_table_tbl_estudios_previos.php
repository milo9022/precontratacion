<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTblEstudiosPrevios extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_estudios_previos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('codigo')->nullable();
            $table->text('vigencia')->nullable();
            $table->unsignedBigInteger('garantias_id')->nullable();
            $table->unsignedBigInteger('estudios_previos_tipo_id');
            $table->unsignedBigInteger('plan_anual_adquisicion_id');
            $table->unsignedBigInteger('recursos_humano_quien_suscribe_id')->nullable();//QUIEN SUSCRIBE EL ESTUDIO
            $table->unsignedBigInteger('recursos_humano_apoyo_tecnicos_id')->nullable();//APOYOS TÉCNICOS
            $table->unsignedBigInteger('recursos_humano_apoyo_juridico_id')->nullable();//APOYO JURÍDICO
            $table->date('fecha_estudios_previos'); //fecha de elaoración de estudios previos
            $table->text('experiencia')->nullable(); //nro de meses o años
            $table->text('idoneidad')->nullable(); //titulos academicos
            $table->text('objeto');
            $table->string('otros')->nullable();
            $table->unsignedBigInteger('tipologias_contractuales_id')->nullable();
            $table->text('plazo_ejecucion')->nullable(); //Tiempo estimado
            $table->decimal('valor_numero', 12,2);
            $table->text('lugar_ejecucion')->nullable();
            $table->text('valor_letras');
            $table->unsignedBigInteger('forma_de_pago_id');
            $table->integer('numero_cuotas')->nullable();
            $table->unsignedBigInteger('estudios_previos_estado_id');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_estudios_previos');
    }
}
