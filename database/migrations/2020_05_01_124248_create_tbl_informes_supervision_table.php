<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblInformesSupervisionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_informes_supervision', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->date('fecha_suscripcion');
            $table->string('periodo')->nullable();
            $table->double('valor_avalado', 12, 2)->nullable();
            $table->text('observaciones')->nullable();
            $table->enum('estado',['aprobado','rechazado','tramite','devuelto'])->default('tramite');
            $table->unsignedBigInteger('contrato_id');
            $table->unsignedBigInteger('cuota_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_informes_supervision');
    }
}
