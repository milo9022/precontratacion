<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToTblActasDeInicioTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('tbl_actas_de_inicio', function(Blueprint $table)
		{
			$table->foreign('contratista_id', 'lnk_tbl_contratistas_tbl_actas_de_inicio')->references('id')->on('tbl_contratistas')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('contratos_id', 'lnk_tbl_contratos_tbl_actas_de_inicio')->references('id')->on('tbl_contratos')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('supervisor_id', 'lnk_tbl_recursos_humanos_tbl_actas_de_inicio')->references('id')->on('tbl_recursos_humanos')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('user_id', 'lnk_users_tbl_actas_de_inicio')->references('id')->on('users')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('tbl_actas_de_inicio', function(Blueprint $table)
		{
			$table->dropForeign('lnk_tbl_contratistas_tbl_actas_de_inicio');
			$table->dropForeign('lnk_tbl_contratos_tbl_actas_de_inicio');
			$table->dropForeign('lnk_tbl_recursos_humanos_tbl_actas_de_inicio');
			$table->dropForeign('lnk_users_tbl_actas_de_inicio');
		});
	}

}
