<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTblEstdiosPreviosApoyosTecnicos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_estdios_previos_apoyos_tecnicos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('recursos_humanos_id');
            $table->unsignedBigInteger('estudios_previos_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_estdios_previos_apoyos_tecnicos');
    }
}
