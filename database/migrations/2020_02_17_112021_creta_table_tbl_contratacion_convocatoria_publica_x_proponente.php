<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CretaTableTblContratacionConvocatoriaPublicaXProponente extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_contratacion_convocatoria_publica_x_proponente', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('proponente_id');
            $table->integer('estudios_previos_id');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_contratacion_convocatoria_publica_x_proponente');
    }
}
