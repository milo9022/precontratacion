<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTblContratosPagosRegistroContable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_contratatos_pagos_registro_contable', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('contratatos_pagos_orden_id');
            $table->string('numero_registro_contable')->nullable();
            $table->date('fecha_ingreso');
            $table->date('fecha_egreso')->nullable();
            $table->enum('estado',['aprobado','rechazado','tramite','devuelto'])->default('tramite');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_contratatos_pagos_registro_contable');
    }
}
