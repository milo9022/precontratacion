<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblOrdenesPagoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_ordenes_pago', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->date('fecha_orden');
            $table->string('numero_orden');
            $table->double('valor_neto', 12, 2);
            $table->double('valor_descuento', 12, 2);
            $table->unsignedBigInteger('informe_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_ordenes_pago');
    }
}
