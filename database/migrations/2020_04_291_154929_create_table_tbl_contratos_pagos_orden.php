<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTblContratosPagosOrden extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_contratatos_pagos_orden', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->date('fecha_ingreso');
            $table->unsignedBigInteger('revisiones_tramites_id');
            $table->double('valor')->nullable();
            $table->string('numero_orden_pago')->nullable();
            $table->double('valor_contrato')->nullable();
            $table->double('valor_ejecutado')->nullable();
            $table->double('valor_pagado')->nullable();
            $table->double('saldo_contrato')->nullable();
            $table->double('porcentaje')->nullable();
            $table->text('observaciones')->nullable();
            $table->date('fecha_egreso')->nullable();
            $table->unsignedBigInteger('contratatos_pagos_orden_estados_id')->nullable();
            $table->enum('estado',['aprobado','rechazado','tramite','devuelto'])->default('tramite');
            
            $table->timestamps();
			$table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_contratatos_pagos_orden');
    }
}
