<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTblContratosPagosTesoreria extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_contratatos_pagos_tesoreria', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('contratatos_pagos_firma_gerencia_id');
            $table->date('fecha_ingreso');
            //$table->date('fecha_egreso')->nullable();
            $table->date('fecha_cancelacion')->nullable();
            $table->date('fecha_envio_area_juridica')->nullable();
            $table->text('quien_recibe_area_juridica')->nullable();
            $table->enum('estado',['aprobado','rechazado','tramite','devuelto'])->default('tramite');
            $table->timestamps();
			$table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_contratatos_pagos_tesoreria');
    }
}
