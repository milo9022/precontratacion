<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToTblDevolucionesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('tbl_devoluciones', function(Blueprint $table)
		{
			$table->foreign('tipo_from_id')->references('id')->on('tbl_tipo_devolucion')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('tipo_to_id')->references('id')->on('tbl_tipo_devolucion')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('tbl_devoluciones', function(Blueprint $table)
		{
			$table->dropForeign('tbl_devoluciones_tipo_from_id_foreign');
			$table->dropForeign('tbl_devoluciones_tipo_to_id_foreign');
		});
	}

}
