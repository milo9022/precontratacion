<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTblRecursosHumanos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_recursos_humanos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nombre_primero');
            $table->string('nombre_segundo')->nullable();
            $table->string('apellido_primero');
            $table->string('apellido_segundo')->nullable();
            $table->string('documento')->unique();
            $table->string('direccion')->nullable();
            $table->unsignedBigInteger('documento_tipo_id')->nullable();
            $table->string('celular')->nullable();
            $table->integer('user_id')->nullable();
            $table->unsignedBigInteger('activo')->default(1);//1 esta laborando, 0 no está laborando
            $table->string('email')->nullable();
            $table->unsignedBigInteger('cargo_id');
			$table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_recursos_humanos');
    }
}
