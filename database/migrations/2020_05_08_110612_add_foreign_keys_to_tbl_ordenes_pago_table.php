<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToTblOrdenesPagoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('tbl_ordenes_pago', function(Blueprint $table)
		{
			$table->foreign('informe_id')->references('id')->on('tbl_informes_supervision')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('tbl_ordenes_pago', function(Blueprint $table)
		{
			$table->dropForeign('tbl_ordenes_pago_informe_id_foreign');
		});
	}

}
