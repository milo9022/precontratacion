<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTblContratosCdp extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_contratos_cdp', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('estudios_previos_id')->unique();
            $table->integer('contrato_id')->unique()->nullable();
            $table->text('acta_autorizacion')->nullable();
            $table->string('acuerdo_numero')->nullable();
            $table->date('acuerdo_fecha')->nullable();
            $table->date('solicitud_fecha')->nullable();
            $table->unsignedBigInteger('cdp_no')->unique();
            $table->string('cdp_rubro');
            $table->date('cdp_fecha');
            $table->double('cdp_valor');
            $table->enum('estado',['aprobado','rechazado','tramite','devuelto'])->default('tramite');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_contratos_cdp');
    }
}
