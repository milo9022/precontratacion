<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTblContratosPagosFirmaAdministracion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_contratatos_pagos_firma_administracion', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('contratatos_pagos_registro_presupuestal_id');
            $table->date('fecha_ingreso');
            $table->date('fecha_egreso')->nullable();
            $table->enum('estado',['aprobado','rechazado','tramite','devuelto'])->default('tramite');
            $table->timestamps();
			$table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_contratatos_pagos_firma_administracion');
    }
}
