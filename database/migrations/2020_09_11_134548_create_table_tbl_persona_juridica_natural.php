<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTblPersonaJuridicaNatural extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_persona_juridica_representante', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nombre_primero')->nullable();
            $table->string('nombre_segundo');
            $table->string('apellido_primero')->nullable();
            $table->string('apellido_segundo');
            $table->string('documento')->nullable();
            $table->integer('tipo')->comment('1 Juridica 2, representante legal');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_persona_juridica_representante');
    }
}
