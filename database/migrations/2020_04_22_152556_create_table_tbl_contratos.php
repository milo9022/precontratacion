<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTblContratos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_contratos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string  ('numero')->unique();
            $table->date    ('suscripcion_fecha');
            $table->text    ('objeto');
            $table->enum    ('personeria_tipo',['juridica','natural']);
            $table->unsignedBigInteger('contratista_id');
            $table->double  ('valor_contrato');
            $table->date    ('vigencia_desde');
            $table->date    ('vigencia_hasta');
            $table->unsignedBigInteger('forma_de_pago_id');
            $table->unsignedBigInteger('estudios_previos_id');
            $table->unsignedBigInteger('contratos_estados_id')->default(1)->comment('1 si, 0 no');
            $table->integer('fecha_corte')->nullable();
            $table->integer('procede_liquidacion')->default(1);//1 si, 0 no
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_contratos');
    }
}
