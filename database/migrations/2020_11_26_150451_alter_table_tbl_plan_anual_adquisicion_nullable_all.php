<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableTblPlanAnualAdquisicionNullableAll extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tbl_plan_anual_adquisicion', function (Blueprint $table) {
            $table->date  ('fecha_estimada_inicio_seleccion')->nullable()->change();
            $table->integer('duracion_estimada_contrato')->nullable()->change();
            $table->string('modalidad_seleccion')->nullable()->change();
            $table->string('fuente_recursos')->nullable()->change();
            $table->string('require_vigencia_futura')->nullable()->change();
            $table->string('estado_solicitud_vigencia_futura')->nullable()->change();
            $table->string('datos_contacto_responsable')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tbl_plan_anual_adquisicion', function (Blueprint $table) {
            //
        });
    }
}
