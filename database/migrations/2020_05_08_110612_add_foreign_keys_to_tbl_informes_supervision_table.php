<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToTblInformesSupervisionTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('tbl_informes_supervision', function(Blueprint $table)
		{
			$table->foreign('contrato_id')->references('id')->on('tbl_contratos')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('cuota_id')->references('id')->on('tbl_contratos_cuotas')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('tbl_informes_supervision', function(Blueprint $table)
		{
			$table->dropForeign('tbl_informes_supervision_contrato_id_foreign');
			$table->dropForeign('tbl_informes_supervision_cuota_id_foreign');
		});
	}

}
