<?php

use Illuminate\Database\Seeder;

class TblEstudiosPreviosTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('tbl_estudios_previos')->delete();
        
        \DB::table('tbl_estudios_previos')->insert(array (
            0 => 
            array (
                'id' => 1,
                'codigo' => '000001',
                'vigencia' => '2018',
                'garantias_id' => 1,
                'estudios_previos_tipo_id' => 1,
                'plan_anual_adquisicion_id' => 4,
                'recursos_humano_quien_suscribe_id' => 1,
                'recursos_humano_apoyo_tecnicos_id' => 2,
                'recursos_humano_apoyo_juridico_id' => 3,
                'fecha_estudios_previos' => '2020-04-29',
                'experiencia' => 'expericneia  años',
                'idoneidad' => 'no',
                'objeto' => 'objeto',
                'otros' => 'no',
                'tipologias_contractuales_id' => 8,
                'plazo_ejecucion' => '20',
                'valor_numero' => '2015800.00',
                'lugar_ejecucion' => 'Popayán',
                'valor_letras' => 'dos millones quince mil ochocientos',
                'forma_de_pago_id' => 2,
                'numero_cuotas' => 2,
                'estudios_previos_estado_id' => 1,
                'deleted_at' => NULL,
                'created_at' => '2020-04-29 18:20:14',
                'updated_at' => '2020-04-29 18:20:14',
            ),
        ));
        
        
    }
}