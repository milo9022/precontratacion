<?php

use Illuminate\Database\Seeder;

class TblContratosCdpTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('tbl_contratos_cdp')->delete();
        
        \DB::table('tbl_contratos_cdp')->insert(array (
            0 => 
            array (
                'id' => 1,
                'estudios_previos_id' => 1,
                'contrato_id' => NULL,
                'acta_autorizacion' => 'Prueba de acta',
                'acuerdo_numero' => '13524',
                'acuerdo_fecha' => '2020-04-01',
                'solicitud_fecha' => '2020-04-30',
                'cdp_no' => 4125,
                'cdp_rubro' => '1565',
                'cdp_fecha' => '2020-04-01',
                'cdp_valor' => 5460000.0,
                'created_at' => '2020-04-29 18:20:53',
                'updated_at' => '2020-04-29 18:20:53',
            ),
        ));
        
        
    }
}