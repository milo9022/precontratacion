<?php

use Illuminate\Database\Seeder;

class TblGarantiasTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('tbl_garantias')->delete();
        
        \DB::table('tbl_garantias')->insert(array (
            0 => 
            array (
                'id' => 1,
                'nombre' => 'Seriedad de la oferta',
                "created_at"=> date('Y-m-d H:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'nombre' => 'Buen manejo y correcta inversión del anticipo',
                "created_at"=> date('Y-m-d H:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'nombre' => 'Devolucion del pago anticipado',
                "created_at"=> date('Y-m-d H:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'nombre' => 'Cumplimiento del contrato',
                "created_at"=> date('Y-m-d H:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            4 => 
            array (
                'id' => 5,
                'nombre' => 'Pago de salarios, prestaciones sociales e indemnizaciones laborales',
                "created_at"=> date('Y-m-d H:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            5 => 
            array (
                'id' => 6,
                'nombre' => 'Estabilidad y calidad de la obra',
                "created_at"=> date('Y-m-d H:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            6 => 
            array (
                'id' => 7,
                'nombre' => 'Calidad del servicio',
                "created_at"=> date('Y-m-d H:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            7 => 
            array (
                'id' => 8,
                'nombre' => 'Calidad y correcto funcionamiento de los bienes',
                "created_at"=> date('Y-m-d H:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            8 => 
            array (
                'id' => 9,
                'nombre' => 'Responsabilidad civil extracontractual',
                "created_at"=> date('Y-m-d H:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            9 => 
            array (
                'id' => 10,
                'nombre' => 'N/A',
                "created_at"=> date('Y-m-d H:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            10 => 
            array (
                'id' => 11,
                'nombre' => 'Transporte de valores',
                "created_at"=> date('Y-m-d H:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            11 => 
            array (
                'id' => 12,
                'nombre' => 'Manejo de bienes, dinero y títulos valores',
                "created_at"=> date('Y-m-d H:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}