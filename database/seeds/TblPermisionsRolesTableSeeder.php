<?php

use Illuminate\Database\Seeder;

class TblPermisionsRolesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('tbl_permisions_roles')->delete();
        
        \DB::table('tbl_permisions_roles')->insert(array (
            0 => 
            array (
                'id' => 7,
                'id_rol' => 2,
                'id_permisions' => 3,
                'created_at' => '2020-01-24 21:41:13',
                'updated_at' => '2020-01-24 21:41:13',
            ),
            1 => 
            array (
                'id' => 8,
                'id_rol' => 1,
                'id_permisions' => 1,
                'created_at' => '2020-01-24 21:50:21',
                'updated_at' => '2020-01-24 21:50:21',
            ),
            2 => 
            array (
                'id' => 9,
                'id_rol' => 1,
                'id_permisions' => 2,
                'created_at' => '2020-01-24 21:50:25',
                'updated_at' => '2020-01-24 21:50:25',
            ),
            3 => 
            array (
                'id' => 10,
                'id_rol' => 1,
                'id_permisions' => 3,
                'created_at' => '2020-01-24 21:50:28',
                'updated_at' => '2020-01-24 21:50:28',
            ),
            4 => 
            array (
                'id' => 11,
                'id_rol' => 1,
                'id_permisions' => 4,
                'created_at' => '2020-01-24 21:50:29',
                'updated_at' => '2020-01-24 21:50:29',
            ),
            5 => 
            array (
                'id' => 12,
                'id_rol' => 1,
                'id_permisions' => 8,
                'created_at' => '2020-01-24 21:50:29',
                'updated_at' => '2020-01-24 21:50:29',
            ),
            6 => 
            array (
                'id' => 13,
                'id_rol' => 1,
                'id_permisions' => 7,
                'created_at' => '2020-01-24 21:50:30',
                'updated_at' => '2020-01-24 21:50:30',
            ),
            7 => 
            array (
                'id' => 15,
                'id_rol' => 1,
                'id_permisions' => 5,
                'created_at' => '2020-01-24 21:50:33',
                'updated_at' => '2020-01-24 21:50:33',
            ),
            8 => 
            array (
                'id' => 16,
                'id_rol' => 1,
                'id_permisions' => 9,
                'created_at' => '2020-01-24 21:50:33',
                'updated_at' => '2020-01-24 21:50:33',
            ),
            9 => 
            array (
                'id' => 17,
                'id_rol' => 1,
                'id_permisions' => 10,
                'created_at' => '2020-01-24 21:50:34',
                'updated_at' => '2020-01-24 21:50:34',
            ),
            10 => 
            array (
                'id' => 18,
                'id_rol' => 1,
                'id_permisions' => 11,
                'created_at' => '2020-01-24 21:50:35',
                'updated_at' => '2020-01-24 21:50:35',
            ),
            11 => 
            array (
                'id' => 19,
                'id_rol' => 1,
                'id_permisions' => 12,
                'created_at' => '2020-01-24 21:50:36',
                'updated_at' => '2020-01-24 21:50:36',
            ),
            12 => 
            array (
                'id' => 20,
                'id_rol' => 1,
                'id_permisions' => 16,
                'created_at' => '2020-01-24 21:50:36',
                'updated_at' => '2020-01-24 21:50:36',
            ),
            13 => 
            array (
                'id' => 21,
                'id_rol' => 1,
                'id_permisions' => 15,
                'created_at' => '2020-01-24 21:50:37',
                'updated_at' => '2020-01-24 21:50:37',
            ),
            14 => 
            array (
                'id' => 22,
                'id_rol' => 1,
                'id_permisions' => 14,
                'created_at' => '2020-01-24 21:50:38',
                'updated_at' => '2020-01-24 21:50:38',
            ),
            15 => 
            array (
                'id' => 23,
                'id_rol' => 1,
                'id_permisions' => 13,
                'created_at' => '2020-01-24 21:50:39',
                'updated_at' => '2020-01-24 21:50:39',
            ),
            16 => 
            array (
                'id' => 24,
                'id_rol' => 1,
                'id_permisions' => 17,
                'created_at' => '2020-01-24 21:50:39',
                'updated_at' => '2020-01-24 21:50:39',
            ),
            17 => 
            array (
                'id' => 25,
                'id_rol' => 1,
                'id_permisions' => 18,
                'created_at' => '2020-01-24 21:50:40',
                'updated_at' => '2020-01-24 21:50:40',
            ),
            18 => 
            array (
                'id' => 26,
                'id_rol' => 1,
                'id_permisions' => 19,
                'created_at' => '2020-01-24 21:50:41',
                'updated_at' => '2020-01-24 21:50:41',
            ),
            19 => 
            array (
                'id' => 27,
                'id_rol' => 3,
                'id_permisions' => 1,
                'created_at' => '2020-01-24 22:53:58',
                'updated_at' => '2020-01-24 22:53:58',
            ),
            20 => 
            array (
                'id' => 28,
                'id_rol' => 3,
                'id_permisions' => 2,
                'created_at' => '2020-01-24 22:53:59',
                'updated_at' => '2020-01-24 22:53:59',
            ),
            21 => 
            array (
                'id' => 29,
                'id_rol' => 3,
                'id_permisions' => 3,
                'created_at' => '2020-01-24 22:54:00',
                'updated_at' => '2020-01-24 22:54:00',
            ),
            22 => 
            array (
                'id' => 31,
                'id_rol' => 3,
                'id_permisions' => 17,
                'created_at' => '2020-01-24 22:54:13',
                'updated_at' => '2020-01-24 22:54:13',
            ),
            23 => 
            array (
                'id' => 33,
                'id_rol' => 3,
                'id_permisions' => 19,
                'created_at' => '2020-01-24 22:54:15',
                'updated_at' => '2020-01-24 22:54:15',
            ),
            24 => 
            array (
                'id' => 34,
                'id_rol' => 4,
                'id_permisions' => 3,
                'created_at' => '2020-01-24 22:54:35',
                'updated_at' => '2020-01-24 22:54:35',
            ),
            25 => 
            array (
                'id' => 35,
                'id_rol' => 4,
                'id_permisions' => 19,
                'created_at' => '2020-01-24 22:54:54',
                'updated_at' => '2020-01-24 22:54:54',
            ),
            26 => 
            array (
                'id' => 36,
                'id_rol' => 4,
                'id_permisions' => 18,
                'created_at' => '2020-01-24 22:54:55',
                'updated_at' => '2020-01-24 22:54:55',
            ),
            27 => 
            array (
                'id' => 37,
                'id_rol' => 4,
                'id_permisions' => 17,
                'created_at' => '2020-01-24 22:54:56',
                'updated_at' => '2020-01-24 22:54:56',
            ),
            28 => 
            array (
                'id' => 38,
                'id_rol' => 1,
                'id_permisions' => 6,
                'created_at' => '2020-05-28 11:05:40',
                'updated_at' => '2020-05-28 11:05:40',
            ),
            29 => 
            array (
                'id' => 39,
                'id_rol' => 1,
                'id_permisions' => 25,
                'created_at' => '2020-05-28 11:05:41',
                'updated_at' => '2020-05-28 11:05:41',
            ),
            30 => 
            array (
                'id' => 40,
                'id_rol' => 1,
                'id_permisions' => 47,
                'created_at' => '2020-05-28 11:05:42',
                'updated_at' => '2020-05-28 11:05:42',
            ),
            31 => 
            array (
                'id' => 41,
                'id_rol' => 1,
                'id_permisions' => 31,
                'created_at' => '2020-05-28 11:05:43',
                'updated_at' => '2020-05-28 11:05:43',
            ),
            32 => 
            array (
                'id' => 42,
                'id_rol' => 1,
                'id_permisions' => 35,
                'created_at' => '2020-05-28 11:05:43',
                'updated_at' => '2020-05-28 11:05:43',
            ),
            33 => 
            array (
                'id' => 43,
                'id_rol' => 1,
                'id_permisions' => 23,
                'created_at' => '2020-05-28 11:05:44',
                'updated_at' => '2020-05-28 11:05:44',
            ),
            34 => 
            array (
                'id' => 44,
                'id_rol' => 1,
                'id_permisions' => 43,
                'created_at' => '2020-05-28 11:05:44',
                'updated_at' => '2020-05-28 11:05:44',
            ),
            35 => 
            array (
                'id' => 45,
                'id_rol' => 1,
                'id_permisions' => 39,
                'created_at' => '2020-05-28 11:05:45',
                'updated_at' => '2020-05-28 11:05:45',
            ),
            36 => 
            array (
                'id' => 46,
                'id_rol' => 1,
                'id_permisions' => 37,
                'created_at' => '2020-05-28 11:05:46',
                'updated_at' => '2020-05-28 11:05:46',
            ),
            37 => 
            array (
                'id' => 47,
                'id_rol' => 1,
                'id_permisions' => 41,
                'created_at' => '2020-05-28 11:05:46',
                'updated_at' => '2020-05-28 11:05:46',
            ),
            38 => 
            array (
                'id' => 48,
                'id_rol' => 1,
                'id_permisions' => 21,
                'created_at' => '2020-05-28 11:05:47',
                'updated_at' => '2020-05-28 11:05:47',
            ),
            39 => 
            array (
                'id' => 49,
                'id_rol' => 1,
                'id_permisions' => 33,
                'created_at' => '2020-05-28 11:05:47',
                'updated_at' => '2020-05-28 11:05:47',
            ),
            40 => 
            array (
                'id' => 50,
                'id_rol' => 1,
                'id_permisions' => 29,
                'created_at' => '2020-05-28 11:05:48',
                'updated_at' => '2020-05-28 11:05:48',
            ),
            41 => 
            array (
                'id' => 51,
                'id_rol' => 1,
                'id_permisions' => 45,
                'created_at' => '2020-05-28 11:05:49',
                'updated_at' => '2020-05-28 11:05:49',
            ),
            42 => 
            array (
                'id' => 52,
                'id_rol' => 1,
                'id_permisions' => 26,
                'created_at' => '2020-05-28 11:05:49',
                'updated_at' => '2020-05-28 11:05:49',
            ),
            43 => 
            array (
                'id' => 53,
                'id_rol' => 1,
                'id_permisions' => 27,
                'created_at' => '2020-05-28 11:05:50',
                'updated_at' => '2020-05-28 11:05:50',
            ),
            44 => 
            array (
                'id' => 54,
                'id_rol' => 1,
                'id_permisions' => 46,
                'created_at' => '2020-05-28 11:05:50',
                'updated_at' => '2020-05-28 11:05:50',
            ),
            45 => 
            array (
                'id' => 55,
                'id_rol' => 1,
                'id_permisions' => 30,
                'created_at' => '2020-05-28 11:05:51',
                'updated_at' => '2020-05-28 11:05:51',
            ),
            46 => 
            array (
                'id' => 56,
                'id_rol' => 1,
                'id_permisions' => 34,
                'created_at' => '2020-05-28 11:05:52',
                'updated_at' => '2020-05-28 11:05:52',
            ),
            47 => 
            array (
                'id' => 57,
                'id_rol' => 1,
                'id_permisions' => 22,
                'created_at' => '2020-05-28 11:05:53',
                'updated_at' => '2020-05-28 11:05:53',
            ),
            48 => 
            array (
                'id' => 58,
                'id_rol' => 1,
                'id_permisions' => 42,
                'created_at' => '2020-05-28 11:05:53',
                'updated_at' => '2020-05-28 11:05:53',
            ),
            49 => 
            array (
                'id' => 59,
                'id_rol' => 1,
                'id_permisions' => 38,
                'created_at' => '2020-05-28 11:05:54',
                'updated_at' => '2020-05-28 11:05:54',
            ),
            50 => 
            array (
                'id' => 60,
                'id_rol' => 1,
                'id_permisions' => 40,
                'created_at' => '2020-05-28 11:05:55',
                'updated_at' => '2020-05-28 11:05:55',
            ),
            51 => 
            array (
                'id' => 61,
                'id_rol' => 1,
                'id_permisions' => 44,
                'created_at' => '2020-05-28 11:05:55',
                'updated_at' => '2020-05-28 11:05:55',
            ),
            52 => 
            array (
                'id' => 62,
                'id_rol' => 1,
                'id_permisions' => 20,
                'created_at' => '2020-05-28 11:05:56',
                'updated_at' => '2020-05-28 11:05:56',
            ),
            53 => 
            array (
                'id' => 63,
                'id_rol' => 1,
                'id_permisions' => 24,
                'created_at' => '2020-05-28 11:05:57',
                'updated_at' => '2020-05-28 11:05:57',
            ),
            54 => 
            array (
                'id' => 64,
                'id_rol' => 1,
                'id_permisions' => 36,
                'created_at' => '2020-05-28 11:05:58',
                'updated_at' => '2020-05-28 11:05:58',
            ),
            55 => 
            array (
                'id' => 65,
                'id_rol' => 1,
                'id_permisions' => 32,
                'created_at' => '2020-05-28 11:05:59',
                'updated_at' => '2020-05-28 11:05:59',
            ),
            56 => 
            array (
                'id' => 66,
                'id_rol' => 1,
                'id_permisions' => 48,
                'created_at' => '2020-05-28 11:05:59',
                'updated_at' => '2020-05-28 11:05:59',
            ),
            57 => 
            array (
                'id' => 67,
                'id_rol' => 1,
                'id_permisions' => 28,
                'created_at' => '2020-05-28 11:06:00',
                'updated_at' => '2020-05-28 11:06:00',
            ),
            58 => 
            array (
                'id' => 69,
                'id_rol' => 2,
                'id_permisions' => 26,
                'created_at' => '2020-05-28 11:06:16',
                'updated_at' => '2020-05-28 11:06:16',
            ),
            59 => 
            array (
                'id' => 70,
                'id_rol' => 2,
                'id_permisions' => 9,
                'created_at' => '2020-05-28 11:06:21',
                'updated_at' => '2020-05-28 11:06:21',
            ),
            60 => 
            array (
                'id' => 71,
                'id_rol' => 2,
                'id_permisions' => 45,
                'created_at' => '2020-05-28 11:06:23',
                'updated_at' => '2020-05-28 11:06:23',
            ),
            61 => 
            array (
                'id' => 72,
                'id_rol' => 2,
                'id_permisions' => 29,
                'created_at' => '2020-05-28 11:06:24',
                'updated_at' => '2020-05-28 11:06:24',
            ),
            62 => 
            array (
                'id' => 73,
                'id_rol' => 2,
                'id_permisions' => 33,
                'created_at' => '2020-05-28 11:06:25',
                'updated_at' => '2020-05-28 11:06:25',
            ),
            63 => 
            array (
                'id' => 74,
                'id_rol' => 2,
                'id_permisions' => 21,
                'created_at' => '2020-05-28 11:06:25',
                'updated_at' => '2020-05-28 11:06:25',
            ),
            64 => 
            array (
                'id' => 76,
                'id_rol' => 2,
                'id_permisions' => 41,
                'created_at' => '2020-05-28 11:06:29',
                'updated_at' => '2020-05-28 11:06:29',
            ),
            65 => 
            array (
                'id' => 77,
                'id_rol' => 2,
                'id_permisions' => 37,
                'created_at' => '2020-05-28 11:06:30',
                'updated_at' => '2020-05-28 11:06:30',
            ),
            66 => 
            array (
                'id' => 78,
                'id_rol' => 1,
                'id_permisions' => 49,
                'created_at' => '2020-06-10 09:48:56',
                'updated_at' => '2020-06-10 09:48:56',
            ),
            67 => 
            array (
                'id' => 79,
                'id_rol' => 1,
                'id_permisions' => 50,
                'created_at' => '2020-06-10 09:48:56',
                'updated_at' => '2020-06-10 09:48:56',
            ),
            68 => 
            array (
                'id' => 80,
                'id_rol' => 1,
                'id_permisions' => 51,
                'created_at' => '2020-06-10 09:48:57',
                'updated_at' => '2020-06-10 09:48:57',
            ),
            69 => 
            array (
                'id' => 81,
                'id_rol' => 1,
                'id_permisions' => 52,
                'created_at' => '2020-06-10 09:48:59',
                'updated_at' => '2020-06-10 09:48:59',
            ),
            70 => 
            array (
                'id' => 82,
                'id_rol' => 3,
                'id_permisions' => 49,
                'created_at' => '2020-06-10 09:49:06',
                'updated_at' => '2020-06-10 09:49:06',
            ),
            71 => 
            array (
                'id' => 83,
                'id_rol' => 3,
                'id_permisions' => 50,
                'created_at' => '2020-06-10 09:49:07',
                'updated_at' => '2020-06-10 09:49:07',
            ),
            72 => 
            array (
                'id' => 84,
                'id_rol' => 3,
                'id_permisions' => 51,
                'created_at' => '2020-06-10 09:49:08',
                'updated_at' => '2020-06-10 09:49:08',
            ),
            73 => 
            array (
                'id' => 85,
                'id_rol' => 3,
                'id_permisions' => 52,
                'created_at' => '2020-06-10 09:49:09',
                'updated_at' => '2020-06-10 09:49:09',
            ),
            74 => 
            array (
                'id' => 86,
                'id_rol' => 3,
                'id_permisions' => 36,
                'created_at' => '2020-06-10 09:49:10',
                'updated_at' => '2020-06-10 09:49:10',
            ),
            75 => 
            array (
                'id' => 87,
                'id_rol' => 3,
                'id_permisions' => 34,
                'created_at' => '2020-06-10 09:49:11',
                'updated_at' => '2020-06-10 09:49:11',
            ),
            76 => 
            array (
                'id' => 88,
                'id_rol' => 3,
                'id_permisions' => 33,
                'created_at' => '2020-06-10 09:49:12',
                'updated_at' => '2020-06-10 09:49:12',
            ),
            77 => 
            array (
                'id' => 89,
                'id_rol' => 3,
                'id_permisions' => 35,
                'created_at' => '2020-06-10 09:49:13',
                'updated_at' => '2020-06-10 09:49:13',
            ),
            78 => 
            array (
                'id' => 90,
                'id_rol' => 3,
                'id_permisions' => 23,
                'created_at' => '2020-06-10 09:49:14',
                'updated_at' => '2020-06-10 09:49:14',
            ),
            79 => 
            array (
                'id' => 91,
                'id_rol' => 3,
                'id_permisions' => 21,
                'created_at' => '2020-06-10 09:49:15',
                'updated_at' => '2020-06-10 09:49:15',
            ),
            80 => 
            array (
                'id' => 92,
                'id_rol' => 3,
                'id_permisions' => 22,
                'created_at' => '2020-06-10 09:49:16',
                'updated_at' => '2020-06-10 09:49:16',
            ),
            81 => 
            array (
                'id' => 93,
                'id_rol' => 3,
                'id_permisions' => 24,
                'created_at' => '2020-06-10 09:49:17',
                'updated_at' => '2020-06-10 09:49:17',
            ),
            82 => 
            array (
                'id' => 95,
                'id_rol' => 3,
                'id_permisions' => 18,
                'created_at' => '2020-06-10 09:49:27',
                'updated_at' => '2020-06-10 09:49:27',
            ),
            83 => 
            array (
                'id' => 96,
                'id_rol' => 3,
                'id_permisions' => 20,
                'created_at' => '2020-06-10 09:49:28',
                'updated_at' => '2020-06-10 09:49:28',
            ),
            84 => 
            array (
                'id' => 97,
                'id_rol' => 3,
                'id_permisions' => 44,
                'created_at' => '2020-06-10 09:49:29',
                'updated_at' => '2020-06-10 09:49:29',
            ),
            85 => 
            array (
                'id' => 98,
                'id_rol' => 3,
                'id_permisions' => 40,
                'created_at' => '2020-06-10 09:49:29',
                'updated_at' => '2020-06-10 09:49:29',
            ),
            86 => 
            array (
                'id' => 99,
                'id_rol' => 3,
                'id_permisions' => 42,
                'created_at' => '2020-06-10 09:49:30',
                'updated_at' => '2020-06-10 09:49:30',
            ),
            87 => 
            array (
                'id' => 100,
                'id_rol' => 3,
                'id_permisions' => 38,
                'created_at' => '2020-06-10 09:49:31',
                'updated_at' => '2020-06-10 09:49:31',
            ),
            88 => 
            array (
                'id' => 101,
                'id_rol' => 3,
                'id_permisions' => 41,
                'created_at' => '2020-06-10 09:49:32',
                'updated_at' => '2020-06-10 09:49:32',
            ),
            89 => 
            array (
                'id' => 102,
                'id_rol' => 3,
                'id_permisions' => 37,
                'created_at' => '2020-06-10 09:49:33',
                'updated_at' => '2020-06-10 09:49:33',
            ),
            90 => 
            array (
                'id' => 103,
                'id_rol' => 3,
                'id_permisions' => 43,
                'created_at' => '2020-06-10 09:49:34',
                'updated_at' => '2020-06-10 09:49:34',
            ),
            91 => 
            array (
                'id' => 104,
                'id_rol' => 3,
                'id_permisions' => 39,
                'created_at' => '2020-06-10 09:49:34',
                'updated_at' => '2020-06-10 09:49:34',
            ),
            92 => 
            array (
                'id' => 105,
                'id_rol' => 3,
                'id_permisions' => 15,
                'created_at' => '2020-06-10 09:49:40',
                'updated_at' => '2020-06-10 09:49:40',
            ),
            93 => 
            array (
                'id' => 106,
                'id_rol' => 3,
                'id_permisions' => 13,
                'created_at' => '2020-06-10 09:49:43',
                'updated_at' => '2020-06-10 09:49:43',
            ),
            94 => 
            array (
                'id' => 107,
                'id_rol' => 3,
                'id_permisions' => 14,
                'created_at' => '2020-06-10 09:49:44',
                'updated_at' => '2020-06-10 09:49:44',
            ),
            95 => 
            array (
                'id' => 108,
                'id_rol' => 3,
                'id_permisions' => 16,
                'created_at' => '2020-06-10 09:49:45',
                'updated_at' => '2020-06-10 09:49:45',
            ),
            96 => 
            array (
                'id' => 109,
                'id_rol' => 3,
                'id_permisions' => 25,
                'created_at' => '2020-06-10 09:49:46',
                'updated_at' => '2020-06-10 09:49:46',
            ),
            97 => 
            array (
                'id' => 110,
                'id_rol' => 3,
                'id_permisions' => 26,
                'created_at' => '2020-06-10 09:49:48',
                'updated_at' => '2020-06-10 09:49:48',
            ),
            98 => 
            array (
                'id' => 111,
                'id_rol' => 3,
                'id_permisions' => 27,
                'created_at' => '2020-06-10 09:49:49',
                'updated_at' => '2020-06-10 09:49:49',
            ),
            99 => 
            array (
                'id' => 112,
                'id_rol' => 3,
                'id_permisions' => 28,
                'created_at' => '2020-06-10 09:49:50',
                'updated_at' => '2020-06-10 09:49:50',
            ),
            100 => 
            array (
                'id' => 113,
                'id_rol' => 3,
                'id_permisions' => 11,
                'created_at' => '2020-06-10 09:49:51',
                'updated_at' => '2020-06-10 09:49:51',
            ),
            101 => 
            array (
                'id' => 114,
                'id_rol' => 3,
                'id_permisions' => 12,
                'created_at' => '2020-06-10 09:49:52',
                'updated_at' => '2020-06-10 09:49:52',
            ),
            102 => 
            array (
                'id' => 115,
                'id_rol' => 3,
                'id_permisions' => 9,
                'created_at' => '2020-06-10 09:49:53',
                'updated_at' => '2020-06-10 09:49:53',
            ),
            103 => 
            array (
                'id' => 116,
                'id_rol' => 3,
                'id_permisions' => 10,
                'created_at' => '2020-06-10 09:49:55',
                'updated_at' => '2020-06-10 09:49:55',
            ),
            104 => 
            array (
                'id' => 117,
                'id_rol' => 3,
                'id_permisions' => 47,
                'created_at' => '2020-06-10 09:49:55',
                'updated_at' => '2020-06-10 09:49:55',
            ),
            105 => 
            array (
                'id' => 118,
                'id_rol' => 3,
                'id_permisions' => 45,
                'created_at' => '2020-06-10 09:49:56',
                'updated_at' => '2020-06-10 09:49:56',
            ),
            106 => 
            array (
                'id' => 119,
                'id_rol' => 3,
                'id_permisions' => 46,
                'created_at' => '2020-06-10 09:49:57',
                'updated_at' => '2020-06-10 09:49:57',
            ),
            107 => 
            array (
                'id' => 120,
                'id_rol' => 3,
                'id_permisions' => 48,
                'created_at' => '2020-06-10 09:49:58',
                'updated_at' => '2020-06-10 09:49:58',
            ),
            108 => 
            array (
                'id' => 121,
                'id_rol' => 3,
                'id_permisions' => 32,
                'created_at' => '2020-06-10 09:49:59',
                'updated_at' => '2020-06-10 09:49:59',
            ),
            109 => 
            array (
                'id' => 122,
                'id_rol' => 3,
                'id_permisions' => 30,
                'created_at' => '2020-06-10 09:50:00',
                'updated_at' => '2020-06-10 09:50:00',
            ),
            110 => 
            array (
                'id' => 123,
                'id_rol' => 3,
                'id_permisions' => 29,
                'created_at' => '2020-06-10 09:50:01',
                'updated_at' => '2020-06-10 09:50:01',
            ),
            111 => 
            array (
                'id' => 124,
                'id_rol' => 3,
                'id_permisions' => 31,
                'created_at' => '2020-06-10 09:50:02',
                'updated_at' => '2020-06-10 09:50:02',
            ),
            112 => 
            array (
                'id' => 125,
                'id_rol' => 3,
                'id_permisions' => 4,
                'created_at' => '2020-06-10 09:50:03',
                'updated_at' => '2020-06-10 09:50:03',
            ),
            113 => 
            array (
                'id' => 126,
                'id_rol' => 1,
                'id_permisions' => 53,
                'created_at' => '2020-06-24 12:11:33',
                'updated_at' => '2020-06-24 12:11:33',
            ),
            114 => 
            array (
                'id' => 127,
                'id_rol' => 1,
                'id_permisions' => 54,
                'created_at' => '2020-06-24 12:11:34',
                'updated_at' => '2020-06-24 12:11:34',
            ),
            115 => 
            array (
                'id' => 128,
                'id_rol' => 1,
                'id_permisions' => 55,
                'created_at' => '2020-06-24 12:11:35',
                'updated_at' => '2020-06-24 12:11:35',
            ),
            116 => 
            array (
                'id' => 129,
                'id_rol' => 1,
                'id_permisions' => 56,
                'created_at' => '2020-06-24 12:11:36',
                'updated_at' => '2020-06-24 12:11:36',
            ),
            117 => 
            array (
                'id' => 130,
                'id_rol' => 5,
                'id_permisions' => 10,
                'created_at' => '2020-11-06 10:02:11',
                'updated_at' => '2020-11-06 10:02:11',
            ),
            118 => 
            array (
                'id' => 131,
                'id_rol' => 5,
                'id_permisions' => 9,
                'created_at' => '2020-11-06 10:02:12',
                'updated_at' => '2020-11-06 10:02:12',
            ),
            119 => 
            array (
                'id' => 132,
                'id_rol' => 5,
                'id_permisions' => 12,
                'created_at' => '2020-11-06 10:02:13',
                'updated_at' => '2020-11-06 10:02:13',
            ),
            120 => 
            array (
                'id' => 133,
                'id_rol' => 5,
                'id_permisions' => 11,
                'created_at' => '2020-11-06 10:02:13',
                'updated_at' => '2020-11-06 10:02:13',
            ),
        ));
        
        
    }
}