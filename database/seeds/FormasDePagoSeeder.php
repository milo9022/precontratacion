<?php

use Illuminate\Database\Seeder;

class FormasDePagoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('tbl_formas_de_pago')->insert([
            "nombre" => "Cuotas parciales",
            "created_at"=> date('Y-m-d H:i:s')
        ]);

        \DB::table('tbl_formas_de_pago')->insert([
            "nombre" => "Cuotas mensuales",
            "created_at"=> date('Y-m-d H:i:s')
        ]);

        \DB::table('tbl_formas_de_pago')->insert([
            "nombre" => "Pago único",
            "created_at"=> date('Y-m-d H:i:s')
        ]);
    }
}
