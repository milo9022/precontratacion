<?php

use Illuminate\Database\Seeder;

class TblMunicipiosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('tbl_municipios')->delete();
        
        \DB::table('tbl_municipios')->insert(array (
            array (
                'id' => 235,
                'nombre' => 'CALDONO',
                'id_departamento' => 19,
                'activo'=>1,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            235 => 
            array (
                'id' => 236,
                'nombre' => 'POPAYAN',
                'id_departamento' => 19,
                'activo'=>1,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            241 => 
            array (
                'id' => 242,
                'nombre' => 'PURACE',
                'id_departamento' => 19,
                'activo'=>1,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            246 => 
            array (
                'id' => 247,
                'nombre' => 'CORINTO',
                'activo'=>0,
                'id_departamento' => 19,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            247 => 
            array (
                'id' => 248,
                'nombre' => 'TIMBIO',
                'id_departamento' => 19,
                'activo'=>0,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            248 => 
            array (
                'id' => 249,
                'nombre' => 'ALMAGUER',
                'id_departamento' => 19,
                'activo'=>0,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            249 => 
            array (
                'id' => 250,
                'nombre' => 'LA VEGA',
                'id_departamento' => 19,
                'activo'=>0,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            250 => 
            array (
                'id' => 251,
                'nombre' => 'SAN SEBASTIAN',
                'id_departamento' => 19,
                'activo'=>0,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            251 => 
            array (
                'id' => 252,
                'nombre' => 'SILVIA',
                'id_departamento' => 19,
                'activo'=>0,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            252 => 
            array (
                'id' => 253,
                'nombre' => 'INZA',
                'id_departamento' => 19,
                'activo'=>0,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            253 => 
            array (
                'id' => 254,
                'nombre' => 'SANTANDER DE QUILICHAO',
                'id_departamento' => 19,
                'activo'=>0,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            254 => 
            array (
                'id' => 255,
                'nombre' => 'SANTA ROSA',
                'id_departamento' => 19,
                'activo'=>0,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            255 => 
            array (
                'id' => 256,
                'nombre' => 'JAMBALO',
                'id_departamento' => 19,
                'activo'=>0,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            256 => 
            array (
                'id' => 257,
                'nombre' => 'TOTORO',
                'id_departamento' => 19,
                'activo'=>1,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            257 => 
            array (
                'id' => 258,
                'nombre' => 'TORIBIO',
                'id_departamento' => 19,
                'activo'=>0,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            258 => 
            array (
                'id' => 259,
                'nombre' => 'TIMBIQUI',
                'id_departamento' => 19,
                'activo'=>0,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            259 => 
            array (
                'id' => 260,
                'nombre' => 'LOPEZ',
                'id_departamento' => 19,
                'activo'=>0,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            260 => 
            array (
                'id' => 261,
                'nombre' => 'MERCADERES',
                'id_departamento' => 19,
                'activo'=>0,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),



            261 => 
            array (
                'id' => 262,
                'nombre' => 'CALOTO',
                'id_departamento' => 19,
                'activo'=>0,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            262 => 
            array (
                'id' => 263,
                'nombre' => 'GUAPI',
                'id_departamento' => 19,
                'activo'=>0,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            263 => 
            array (
                'id' => 264,
                'nombre' => 'PADILLA',
                'id_departamento' => 19,
                'activo'=>0,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            264 => 
            array (
                'id' => 265,
                'nombre' => 'BALBOA',
                'id_departamento' => 19,
                'activo'=>0,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            265 => 
            array (
                'id' => 266,
                'nombre' => 'PAEZ  (BELALCAZAR )',
                'id_departamento' => 19,
                'activo'=>0,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            266 => 
            array (
                'id' => 267,
                'nombre' => 'VILLA RICA',
                'id_departamento' => 19,
                'activo'=>0,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            267 => 
            array (
                'id' => 268,
                'nombre' => 'PIENDAMO',
                'id_departamento' => 19,
                'activo'=>0,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            268 => 
            array (
                'id' => 269,
                'nombre' => 'EL TAMBO',
                'id_departamento' => 19,
                'activo'=>0,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            269 => 
            array (
                'id' => 270,
                'nombre' => 'SUAREZ',
                'id_departamento' => 19,
                'activo'=>0,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            270 => 
            array (
                'id' => 271,
                'nombre' => 'FLORENCIA',
                'id_departamento' => 19,
                'activo'=>0,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            271 => 
            array (
                'id'=>272,
                'nombre' => 'PIAMONTE',
                'id_departamento' => 19,
                'activo'=>1,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            )
        ));
        
        
    }
}
