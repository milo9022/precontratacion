<?php

use Illuminate\Database\Seeder;

class TblContratatosPagosOrdenEstadosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('tbl_contratatos_pagos_orden_estados')->delete();
        $data=array();
        $data[]=['id'=>1,'nombre'=>'Presentacion de las cuentas y demas soportes, Revisión y Tramite'];
        $data[]=['id'=>2,'nombre'=>'Elaboración de orden de pago y trámite de cancelación'];
        $data[]=['id'=>3,'nombre'=>'Registro contable'];
        $data[]=['id'=>4,'nombre'=>'Registro presupuestal'];
        $data[]=['id'=>5,'nombre'=>'Firma de profesional universitario de administracion'];
        $data[]=['id'=>6,'nombre'=>'Firma de Gerencia'];
        $data[]=['id'=>7,'nombre'=>'Tesorería'];
        $data[]=['id'=>8,'nombre'=>'Pago ejecutado'];
        \DB::table('tbl_contratos_estados')->insert($data);
    }
}
