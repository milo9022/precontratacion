<?php

use Illuminate\Database\Seeder;

class TblContratistasTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('tbl_contratistas')->delete();
        
        \DB::table('tbl_contratistas')->insert(array (
            0 => 
            array (
                'id' => 1,
                'establecimiento_nombre' => 'crv',
                'nombre_primero' => 'Camilo',
                'nombre_segundo' => 'Ernesto',
                'apellido_primero' => 'Ruiz',
                'apellido_segundo' => 'Vidal',
                'documento' => '1061716139',
                'direccion' => 'casa',
                'celular' => '3186234042',
                'email' => 'camiloruizvidal@gmail.com',
                'municipio_id' => 236,
                'deleted_at' => NULL,
                'created_at' => '2020-04-29 18:22:23',
                'updated_at' => '2020-04-29 18:22:23',
            ),
        ));
        
        
    }
}