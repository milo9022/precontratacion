<?php

use Illuminate\Database\Seeder;

class TblActasDeInicioTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('tbl_actas_de_inicio')->delete();
        
        \DB::table('tbl_actas_de_inicio')->insert(array (
            0 => 
            array (
                'id' => 1,
                'supervisor_id' => 1,
                'contratista_id' => 1,
                'contratos_id' => 1,
                'fecha_suscripcion' => '2020-04-29',
                'created_at' => '2020-04-29 18:37:13',
                'user_id'=>1,
                'updated_at' => NULL,
            ),
        ));
        
        
    }
}