<?php

use Illuminate\Database\Seeder;

class TblPermisionsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('tbl_permisions')->delete();
        
        \DB::table('tbl_permisions')->insert(array (
            0 => 
            array (
                'id' => 1,
                'nombre' => 'estudios_previos_create',
                'created_at' => '2020-05-28 10:05:00',
                'updated_at' => '2020-05-28 10:05:00',
            ),
            1 => 
            array (
                'id' => 2,
                'nombre' => 'estudios_previos_update',
                'created_at' => '2020-05-28 10:05:00',
                'updated_at' => '2020-05-28 10:05:00',
            ),
            2 => 
            array (
                'id' => 3,
                'nombre' => 'estudios_previos_index',
                'created_at' => '2020-05-28 10:05:00',
                'updated_at' => '2020-05-28 10:05:00',
            ),
            3 => 
            array (
                'id' => 4,
                'nombre' => 'estudios_previos_delete',
                'created_at' => '2020-05-28 10:05:00',
                'updated_at' => '2020-05-28 10:05:00',
            ),
            4 => 
            array (
                'id' => 5,
                'nombre' => 'Usuarios_create',
                'created_at' => '2020-05-28 10:05:00',
                'updated_at' => '2020-05-28 10:05:00',
            ),
            5 => 
            array (
                'id' => 6,
                'nombre' => 'Usuarios_update',
                'created_at' => '2020-05-28 10:05:00',
                'updated_at' => '2020-05-28 10:05:00',
            ),
            6 => 
            array (
                'id' => 7,
                'nombre' => 'Usuarios_index',
                'created_at' => '2020-05-28 10:05:00',
                'updated_at' => '2020-05-28 10:05:00',
            ),
            7 => 
            array (
                'id' => 8,
                'nombre' => 'Usuarios_delete',
                'created_at' => '2020-05-28 10:05:00',
                'updated_at' => '2020-05-28 10:05:00',
            ),
            8 => 
            array (
                'id' => 9,
                'nombre' => 'plan_anual_adquisicion_index',
                'created_at' => '2020-05-28 10:05:00',
                'updated_at' => '2020-05-28 10:05:00',
            ),
            9 => 
            array (
                'id' => 10,
                'nombre' => 'plan_anual_adquisicion_update',
                'created_at' => '2020-05-28 10:05:00',
                'updated_at' => '2020-05-28 10:05:00',
            ),
            10 => 
            array (
                'id' => 11,
                'nombre' => 'plan_anual_adquisicion_create',
                'created_at' => '2020-05-28 10:05:00',
                'updated_at' => '2020-05-28 10:05:00',
            ),
            11 => 
            array (
                'id' => 12,
                'nombre' => 'plan_anual_adquisicion_delete',
                'created_at' => '2020-05-28 10:05:00',
                'updated_at' => '2020-05-28 10:05:00',
            ),
            12 => 
            array (
                'id' => 13,
                'nombre' => 'recursos_humanos_index',
                'created_at' => '2020-05-28 10:05:00',
                'updated_at' => '2020-05-28 10:05:00',
            ),
            13 => 
            array (
                'id' => 14,
                'nombre' => 'recursos_humanos_delete',
                'created_at' => '2020-05-28 10:05:00',
                'updated_at' => '2020-05-28 10:05:00',
            ),
            14 => 
            array (
                'id' => 15,
                'nombre' => 'recursos_humanos_update',
                'created_at' => '2020-05-28 10:05:00',
                'updated_at' => '2020-05-28 10:05:00',
            ),
            15 => 
            array (
                'id' => 16,
                'nombre' => 'recursos_humanos_create',
                'created_at' => '2020-05-28 10:05:00',
                'updated_at' => '2020-05-28 10:05:00',
            ),
            16 => 
            array (
                'id' => 17,
                'nombre' => 'cargos_index',
                'created_at' => '2020-05-28 10:05:00',
                'updated_at' => '2020-05-28 10:05:00',
            ),
            17 => 
            array (
                'id' => 18,
                'nombre' => 'cargos_delete',
                'created_at' => '2020-05-28 10:05:00',
                'updated_at' => '2020-05-28 10:05:00',
            ),
            18 => 
            array (
                'id' => 19,
                'nombre' => 'cargos_update',
                'created_at' => '2020-05-28 10:05:00',
                'updated_at' => '2020-05-28 10:05:00',
            ),
            19 => 
            array (
                'id' => 20,
                'nombre' => 'cargos_create',
                'created_at' => '2020-05-28 10:05:00',
                'updated_at' => '2020-05-28 10:05:00',
            ),
            20 => 
            array (
                'id' => 21,
                'nombre' => 'cdp_index',
                'created_at' => '2020-05-28 10:05:00',
                'updated_at' => '2020-05-28 10:05:00',
            ),
            21 => 
            array (
                'id' => 22,
                'nombre' => 'cdp_delete',
                'created_at' => '2020-05-28 10:05:00',
                'updated_at' => '2020-05-28 10:05:00',
            ),
            22 => 
            array (
                'id' => 23,
                'nombre' => 'cdp_update',
                'created_at' => '2020-05-28 10:05:00',
                'updated_at' => '2020-05-28 10:05:00',
            ),
            23 => 
            array (
                'id' => 24,
                'nombre' => 'cdp_create',
                'created_at' => '2020-05-28 10:05:00',
                'updated_at' => '2020-05-28 10:05:00',
            ),
            24 => 
            array (
                'id' => 25,
                'nombre' => 'protocolarizacion_update',
                'created_at' => '2020-05-28 10:05:00',
                'updated_at' => '2020-05-28 10:05:00',
            ),
            25 => 
            array (
                'id' => 26,
                'nombre' => 'protocolarizacion_index',
                'created_at' => '2020-05-28 10:05:00',
                'updated_at' => '2020-05-28 10:05:00',
            ),
            26 => 
            array (
                'id' => 27,
                'nombre' => 'protocolarizacion_delete',
                'created_at' => '2020-05-28 10:05:00',
                'updated_at' => '2020-05-28 10:05:00',
            ),
            27 => 
            array (
                'id' => 28,
                'nombre' => 'protocolarizacion_create',
                'created_at' => '2020-05-28 10:05:00',
                'updated_at' => '2020-05-28 10:05:00',
            ),
            28 => 
            array (
                'id' => 29,
                'nombre' => 'informe_supervision_index',
                'created_at' => '2020-05-28 10:05:00',
                'updated_at' => '2020-05-28 10:05:00',
            ),
            29 => 
            array (
                'id' => 30,
                'nombre' => 'informe_supervision_delete',
                'created_at' => '2020-05-28 10:05:00',
                'updated_at' => '2020-05-28 10:05:00',
            ),
            30 => 
            array (
                'id' => 31,
                'nombre' => 'informe_supervision_update',
                'created_at' => '2020-05-28 10:05:00',
                'updated_at' => '2020-05-28 10:05:00',
            ),
            31 => 
            array (
                'id' => 32,
                'nombre' => 'informe_supervision_create',
                'created_at' => '2020-05-28 10:05:00',
                'updated_at' => '2020-05-28 10:05:00',
            ),
            32 => 
            array (
                'id' => 33,
                'nombre' => 'cobertura_index',
                'created_at' => '2020-05-28 10:05:00',
                'updated_at' => '2020-05-28 10:05:00',
            ),
            33 => 
            array (
                'id' => 34,
                'nombre' => 'cobertura_delete',
                'created_at' => '2020-05-28 10:05:00',
                'updated_at' => '2020-05-28 10:05:00',
            ),
            34 => 
            array (
                'id' => 35,
                'nombre' => 'cobertura_update',
                'created_at' => '2020-05-28 10:05:00',
                'updated_at' => '2020-05-28 10:05:00',
            ),
            35 => 
            array (
                'id' => 36,
                'nombre' => 'cobertura_create',
                'created_at' => '2020-05-28 10:05:00',
                'updated_at' => '2020-05-28 10:05:00',
            ),
            36 => 
            array (
                'id' => 37,
                'nombre' => 'acta_inicio_index',
                'created_at' => '2020-05-28 10:05:00',
                'updated_at' => '2020-05-28 10:05:00',
            ),
            37 => 
            array (
                'id' => 38,
                'nombre' => 'acta_inicio_delete',
                'created_at' => '2020-05-28 10:05:00',
                'updated_at' => '2020-05-28 10:05:00',
            ),
            38 => 
            array (
                'id' => 39,
                'nombre' => 'acta_inicio_update',
                'created_at' => '2020-05-28 10:05:00',
                'updated_at' => '2020-05-28 10:05:00',
            ),
            39 => 
            array (
                'id' => 40,
                'nombre' => 'acta_inicio_create',
                'created_at' => '2020-05-28 10:05:00',
                'updated_at' => '2020-05-28 10:05:00',
            ),
            40 => 
            array (
                'id' => 41,
                'nombre' => 'acta_suspension_index',
                'created_at' => '2020-05-28 10:05:00',
                'updated_at' => '2020-05-28 10:05:00',
            ),
            41 => 
            array (
                'id' => 42,
                'nombre' => 'acta_suspension_delete',
                'created_at' => '2020-05-28 10:05:00',
                'updated_at' => '2020-05-28 10:05:00',
            ),
            42 => 
            array (
                'id' => 43,
                'nombre' => 'acta_suspension_update',
                'created_at' => '2020-05-28 10:05:00',
                'updated_at' => '2020-05-28 10:05:00',
            ),
            43 => 
            array (
                'id' => 44,
                'nombre' => 'acta_suspension_create',
                'created_at' => '2020-05-28 10:05:00',
                'updated_at' => '2020-05-28 10:05:00',
            ),
            44 => 
            array (
                'id' => 45,
                'nombre' => 'otro_si_index',
                'created_at' => '2020-05-28 10:05:00',
                'updated_at' => '2020-05-28 10:05:00',
            ),
            45 => 
            array (
                'id' => 46,
                'nombre' => 'otro_si_delete',
                'created_at' => '2020-05-28 10:05:00',
                'updated_at' => '2020-05-28 10:05:00',
            ),
            46 => 
            array (
                'id' => 47,
                'nombre' => 'otro_si_update',
                'created_at' => '2020-05-28 10:05:00',
                'updated_at' => '2020-05-28 10:05:00',
            ),
            47 => 
            array (
                'id' => 48,
                'nombre' => 'otro_si_create',
                'created_at' => '2020-05-28 10:05:00',
                'updated_at' => '2020-05-28 10:05:00',
            ),




            48 => 
            array (
                'id' => 49,
                'nombre' => 'contratacion_update',
                'created_at' => '2020-05-28 10:05:00',
                'updated_at' => '2020-05-28 10:05:00',
            ),
            49 => 
            array (
                'id' => 50,
                'nombre' => 'contratacion_index',
                'created_at' => '2020-05-28 10:05:00',
                'updated_at' => '2020-05-28 10:05:00',
            ),
            50 => 
            array (
                'id' => 51,
                'nombre' => 'contratacion_delete',
                'created_at' => '2020-05-28 10:05:00',
                'updated_at' => '2020-05-28 10:05:00',
            ),
            51 => 
            array (
                'id' => 52,
                'nombre' => 'contratacion_create',
                'created_at' => '2020-05-28 10:05:00',
                'updated_at' => '2020-05-28 10:05:00',
            ),


            52 => 
            array (
                'id' => 53,
                'nombre' => 'orden_pago_update',
                'created_at' => '2020-05-28 10:05:00',
                'updated_at' => '2020-05-28 10:05:00',
            ),
            53 => 
            array (
                'id' => 54,
                'nombre' => 'orden_pago_index',
                'created_at' => '2020-05-28 10:05:00',
                'updated_at' => '2020-05-28 10:05:00',
            ),
            54 => 
            array (
                'id' => 55,
                'nombre' => 'orden_pago_delete',
                'created_at' => '2020-05-28 10:05:00',
                'updated_at' => '2020-05-28 10:05:00',
            ),
            55 => 
            array (
                'id' => 56,
                'nombre' => 'orden_pago_create',
                'created_at' => '2020-05-28 10:05:00',
                'updated_at' => '2020-05-28 10:05:00',
            ),




            
        ));
        
        
    }
}