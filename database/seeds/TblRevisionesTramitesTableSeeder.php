<?php

use Illuminate\Database\Seeder;

class TblRevisionesTramitesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('tbl_revisiones_tramites')->delete();
        
        \DB::table('tbl_revisiones_tramites')->insert(array (
            array (
                'fecha_presentacion' => '2020-05-01',
                'motivo_devolucion' => NULL,
                'fecha_devolucion' => NULL,
                'fecha_reingreso' => NULL,
                'fecha_egreso' => '2020-07-03',
                'contrato_id' => 1,
                'created_at' => date('Y-m-d h:m:i'),
                'updated_at' => date('Y-m-d h:m:i')
            ),
            array (
                'fecha_presentacion' => '2020-05-02',
                'motivo_devolucion' => NULL,
                'fecha_devolucion' => NULL,
                'fecha_reingreso' => NULL,
                'fecha_egreso' => '2020-07-03',
                'contrato_id' => 1,
                'created_at' => date('Y-m-d h:m:i'),
                'updated_at' => date('Y-m-d h:m:i')
            ),
            array (
                'fecha_presentacion' => '2020-05-03',
                'motivo_devolucion' => NULL,
                'fecha_devolucion' => NULL,
                'fecha_reingreso' => NULL,
                'fecha_egreso' => '2020-07-03',
                'contrato_id' => 1,
                'created_at' => date('Y-m-d h:m:i'),
                'updated_at' => date('Y-m-d h:m:i')
            ),
            array (
                'fecha_presentacion' => '2020-05-04',
                'motivo_devolucion' => NULL,
                'fecha_devolucion' => NULL,
                'fecha_reingreso' => NULL,
                'fecha_egreso' => '2020-07-03',
                'contrato_id' => 1,
                'created_at' => date('Y-m-d h:m:i'),
                'updated_at' => date('Y-m-d h:m:i')
            ),
            array (
                'fecha_presentacion' => '2020-05-05',
                'motivo_devolucion' => NULL,
                'fecha_devolucion' => NULL,
                'fecha_reingreso' => NULL,
                'fecha_egreso' => '2020-07-03',
                'contrato_id' => 2,
                'created_at' => date('Y-m-d h:m:i'),
                'updated_at' => date('Y-m-d h:m:i')
            ),
            array (
                'fecha_presentacion' => '2020-05-06',
                'motivo_devolucion' => NULL,
                'fecha_devolucion' => NULL,
                'fecha_reingreso' => NULL,
                'fecha_egreso' => '2020-07-03',
                'contrato_id' => 2,
                'created_at' => date('Y-m-d h:m:i'),
                'updated_at' => date('Y-m-d h:m:i')
            ),
            array (
                'fecha_presentacion' => '2020-05-07',
                'motivo_devolucion' => NULL,
                'fecha_devolucion' => NULL,
                'fecha_reingreso' => NULL,
                'fecha_egreso' => '2020-07-03',
                'contrato_id' => 2,
                'created_at' => date('Y-m-d h:m:i'),
                'updated_at' => date('Y-m-d h:m:i')
            ),
            array (
                'fecha_presentacion' => '2020-05-08',
                'motivo_devolucion' => NULL,
                'fecha_devolucion' => NULL,
                'fecha_reingreso' => NULL,
                'fecha_egreso' => '2020-07-03',
                'contrato_id' => 2,
                'created_at' => date('Y-m-d h:m:i'),
                'updated_at' => date('Y-m-d h:m:i')
            )
        ));
        
        
    }
}