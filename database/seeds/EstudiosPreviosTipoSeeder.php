<?php

use Illuminate\Database\Seeder;
use App\Models\EstudiosPreviosTipoModel;
use Illuminate\Support\Facades\DB;
class EstudiosPreviosTipoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = new EstudiosPreviosTipoModel();
        $data->nombre = 'CONTRATACION DIRECTA';
        $data->save();
        
        $data = new EstudiosPreviosTipoModel();
        $data->nombre = 'MENOR CUANTIA';
        $data->save();
        
        $data = new EstudiosPreviosTipoModel();
        $data->nombre = 'INVITACIÓN PÚBLICA';
        $data->save();
    }
}
