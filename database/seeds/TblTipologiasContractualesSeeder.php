<?php

use Illuminate\Database\Seeder;

class TblTipologiasContractualesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('tbl_tipologias_contractuales')->insert([
            "nombre" => "CONTRATO DE PRESTACION DE SERVICIOS",
            "created_at"=> date('Y-m-d H:i:s')
            ]);
        \DB::table('tbl_tipologias_contractuales')->insert([
            "nombre" => "CONSULTORIA",
            "created_at"=> date('Y-m-d H:i:s')
            ]);
        \DB::table('tbl_tipologias_contractuales')->insert([
            "nombre" => "CONCESIÓN",
            "created_at"=> date('Y-m-d H:i:s')
            ]);
        \DB::table('tbl_tipologias_contractuales')->insert([
            "nombre" => "SUMINISTRO COMPRAVENTA Y PERMUTA DE BIENES MUEBLES",
            "created_at"=> date('Y-m-d H:i:s')
            ]);
        \DB::table('tbl_tipologias_contractuales')->insert([
            "nombre" => "COMPRAVENTA Y PERMUTA DE BIENES INMUEBLES",
            "created_at"=> date('Y-m-d H:i:s')
            ]);
        \DB::table('tbl_tipologias_contractuales')->insert([
            "nombre" => "CONTRATO DE OBRA",
            "created_at"=> date('Y-m-d H:i:s')
            ]);
        \DB::table('tbl_tipologias_contractuales')->insert([
            "nombre" => "EMPRESTITO",
            "created_at"=> date('Y-m-d H:i:s')
            ]);
        \DB::table('tbl_tipologias_contractuales')->insert([
            "nombre" => "COMODATO O PRESTAMO DE USO",
            "created_at"=> date('Y-m-d H:i:s')
            ]);
        \DB::table('tbl_tipologias_contractuales')->insert([
            "nombre" => "COMPRAVENTA DE SERVICIOS DE SALUD",
            "created_at"=> date('Y-m-d H:i:s')
            ]);
        \DB::table('tbl_tipologias_contractuales')->insert([
            "nombre" => "OTROS",
            "created_at"=> date('Y-m-d H:i:s')
            ]);
    }
}
