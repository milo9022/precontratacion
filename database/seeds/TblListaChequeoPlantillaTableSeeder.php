<?php

use Illuminate\Database\Seeder;

class TblListaChequeoPlantillaTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('tbl_lista_chequeo_plantilla')->delete();
        
        \DB::table('tbl_lista_chequeo_plantilla')->insert(array (
            0 => 
            array (
                'id' => 1,
                'documentos' => 'PLANTILLA DE CONTROL TRAZABILIDAD Y PAGO',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => date('Y-m-d h:i:s')
            ),
            1 => 
            array (
                'id' => 2,
                'documentos' => 'LISTA DE CHEQUEO',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => date('Y-m-d h:i:s')
            ),
            2 => 
            array (
                'id' => 3,
                'documentos' => 'ACTA DE SUPERVISIÓN',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => date('Y-m-d h:i:s')
            ),
            3 => 
            array (
                'id' => 4,
                'documentos' => 'CUENTA DE COBRO',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => date('Y-m-d h:i:s')
            ),
            4 => 
            array (
                'id' => 5,
                'documentos' => 'FACTURA/DOCUMENTO EQUIVALENTE A FACTURA',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => date('Y-m-d h:i:s')
            ),
            5 => 
            array (
                'id' => 7,
                'documentos' => 'CERTIFICACIÓN NUMERO DE CTA BANCARIA',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => date('Y-m-d h:i:s')
            ),
            6 => 
            array (
                'id' => 8,
                'documentos' => 'ORDEN UOPS/CONTRATO',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => date('Y-m-d h:i:s')
            ),
            7 => 
            array (
                'id' => 9,
                'documentos' => 'DISPONIBILIDAD PRESUPUESTAL',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => date('Y-m-d h:i:s')
            ),
            8 => 
            array (
                'id' => 10,
                'documentos' => 'REGISTRO PRESUPUESTAL',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => date('Y-m-d h:i:s')
            ),
            9 => 
            array (
                'id' => 11,
                'documentos' => 'POLIZA',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => date('Y-m-d h:i:s')
            ),
            10 => 
            array (
                'id' => 12,
                'documentos' => 'OFICIO DE APROBACION DE POLIZA',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => date('Y-m-d h:i:s')
            ),
            11 => 
            array (
                'id' => 13,
                'documentos' => 'RUT',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => date('Y-m-d h:i:s')
            ),
            12 => 
            array (
                'id' => 14,
                'documentos' => 'ACTA DE INICIO',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => date('Y-m-d h:i:s')
            ),
            13 => 
            array (
                'id' => 15,
                'documentos' => 'PLANILLA O CERT. FISCALES',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => date('Y-m-d h:i:s')
            ),
            14 => 
            array (
                'id' => 16,
                'documentos' => 'PLANILLA DE SEGURIDAD SOCIAL',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => date('Y-m-d h:i:s')
            ),
            15 => 
            array (
                'id' => 17,
                'documentos' => 'RECIBO DE CONSIGNACION SEGURIDAD SOCIAL',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => date('Y-m-d h:i:s')
            ),
            16 => 
            array (
                'id' => 18,
                'documentos' => 'CERTIFICACION FOSYGA',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => date('Y-m-d h:i:s')
            ),
            17 => 
            array (
                'id' => 19,
                'documentos' => 'INFORME DE ACTIVIDADES',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => date('Y-m-d h:i:s')
            ),
            18 => 
            array (
                'id' => 20,
                'documentos' => 'OFICIO DE RESULTADOS',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => date('Y-m-d h:i:s')
            ),
            19 => 
            array (
                'id' => 21,
                'documentos' => 'CERTIFICADOS RIESGOS LABORALES',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => date('Y-m-d h:i:s')
            ),
            20 => 
            array (
                'id' => 22,
                'documentos' => 'COMPROB. DE ENTRADA ALMACEN',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => date('Y-m-d h:i:s')
            ),
            21 => 
            array (
                'id' => 23,
                'documentos' => 'NOTA SUMINISTROS',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => date('Y-m-d h:i:s')
            ),
            22 => 
            array (
                'id' => 24,
                'documentos' => 'PAZ Y SALVO TESORERIA',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => date('Y-m-d h:i:s')
            ),
            23 => 
            array (
                'id' => 25,
                'documentos' => 'PAZ Y SALVO AUDITORIA',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => date('Y-m-d h:i:s')
            ),
            24 => 
            array (
                'id' => 26,
                'documentos' => 'PAZ Y SALVO ALMACEN',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => date('Y-m-d h:i:s')
            ),
            25 => 
            array (
                'id' => 27,
                'documentos' => 'ACTA DE LIQUIDACION FINAL',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => date('Y-m-d h:i:s')
            ),
            26 => 
            array (
                'id' => 28,
                'documentos' => 'OTROS',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => date('Y-m-d h:i:s')
            ),
        ));
        
        
    }
}