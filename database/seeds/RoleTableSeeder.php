<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Role;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role = new Role();
        $role->id = 1;
        $role->name = 'admin';
        $role->description = 'Administrator';
        $role->save();
        $role = new Role();
        $role->id = 2;
        $role->name = 'supervisor';
        $role->description = 'Supervisor';
        $role->save();
        $role = new Role();
        $role->id = 3;
        $role->name = 'Juridico';
        $role->description = 'Area juridica';
        $role->save();
        $role = new Role();
        $role->id = 4;
        $role->name = 'Contable';
        $role->description = 'Area contable';
        $role->save();
        $role = new Role();
        $role->id = 5;
        $role->name = 'PAA';
        $role->description = 'Plan anual de adquisición';
        $role->save();

    }
}
