<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $test = false;
        $this->call(TblConfigSeeder::class);
        $this->call(RoleTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(TblDepartamentosTableSeeder::class);
        $this->call(TblMunicipiosTableSeeder::class);
        $this->call(TblDocumentoTiposTableSeeder::class);
        $this->call(TblPermisionsTableSeeder::class);
        $this->call(TblPermisionsRolesTableSeeder::class);
        $this->call(EstudiosPreviosEstadoSeeder::class);
        $this->call(EstudiosPreviosTipoSeeder::class);
        $this->call(FormasDePagoSeeder::class);
        $this->call(TblTipologiasContractualesSeeder::class);
        $this->call(CargosTableseeder::class);
        $this->call(TblGarantiasContractualesSeeder::class);
        $this->call(TblPlanAnualAdquisicionTableSeeder::class);
        $this->call(TblContratistasTableSeeder::class);
        $this->call(TblTipoDevolucionesTableSeeder::class);
        $this->call(TblContratosEstadosSeeder::class);
        $this->call(TblRecursosHumanosTableSeeder::class);
        if($test){
        $this->call(TblContratacionDirectaTableSeeder::class);//test
        $this->call(TblEstudiosPreviosTableSeeder::class);///test
        $this->call(TblContratosCdpTableSeeder::class);///test
        $this->call(TblContratosTableSeeder::class);///test
        $this->call(TblRevisionesTramitesTableSeeder::class);///test
        $this->call(TblActasDeInicioTableSeeder::class);///test
        $this->call(TblContratosCuotasTableSeeder::class);///test
        $this->call(TblEstudiosPreviosCuotasTableSeeder::class);///test
            $this->call(TblGarantiasTableSeeder::class);
    }/*
        */
        $this->call(TblRutCodigosTableSeeder::class);
        $this->call(TblListaChequeoPlantillaTableSeeder::class);
    }
}
