<?php

use Illuminate\Database\Seeder;

class TblRutCodigosTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('tbl_rut_codigos')->delete();
        
        \DB::table('tbl_rut_codigos')->insert(array (
            0 => 
            array (
                'id' => 1,
                'codigo' => '111',
                'actividad' => 'Cultivo de cereales (excepto arroz), legumbres y semillas oleaginosas',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'codigo' => '112',
                'actividad' => 'Cultivo de arroz',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'codigo' => '113',
                'actividad' => 'Cultivo de hortalizas, raíces y tubérculos',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'codigo' => '114',
                'actividad' => 'Cultivo de tabaco',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            4 => 
            array (
                'id' => 5,
                'codigo' => '115',
                'actividad' => 'Cultivo de plantas textiles',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            5 => 
            array (
                'id' => 6,
                'codigo' => '119',
                'actividad' => 'Otros cultivos transitorios n.c.p.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            6 => 
            array (
                'id' => 7,
                'codigo' => '121',
                'actividad' => 'Cultivo de frutas tropicales y subtropicales',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            7 => 
            array (
                'id' => 8,
                'codigo' => '122',
                'actividad' => 'Cultivo de plátano y banano',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            8 => 
            array (
                'id' => 9,
                'codigo' => '123',
                'actividad' => 'Cultivo de café',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            9 => 
            array (
                'id' => 10,
                'codigo' => '124',
                'actividad' => 'Cultivo de caña de azúcar',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            10 => 
            array (
                'id' => 11,
                'codigo' => '125',
                'actividad' => 'Cultivo de flor de corte',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            11 => 
            array (
                'id' => 12,
                'codigo' => '126',
            'actividad' => 'Cultivo de palma para aceite (palma africana) y otros frutos oleaginosos',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            12 => 
            array (
                'id' => 13,
                'codigo' => '127',
                'actividad' => 'Cultivo de plantas con las que se preparan bebidas',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            13 => 
            array (
                'id' => 14,
                'codigo' => '128',
                'actividad' => 'Cultivo de especias y de plantas aromáticas y medicinales',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            14 => 
            array (
                'id' => 15,
                'codigo' => '129',
                'actividad' => 'Otros cultivos permanentes n.c.p.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            15 => 
            array (
                'id' => 16,
                'codigo' => '130',
            'actividad' => 'Propagación de plantas (actividades de los viveros, excepto viveros forestales)',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            16 => 
            array (
                'id' => 17,
                'codigo' => '141',
                'actividad' => 'Cría de ganado bovino y bufalino',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            17 => 
            array (
                'id' => 18,
                'codigo' => '142',
                'actividad' => 'Cría de caballos y otros equinos',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            18 => 
            array (
                'id' => 19,
                'codigo' => '143',
                'actividad' => 'Cría de ovejas y cabras',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            19 => 
            array (
                'id' => 20,
                'codigo' => '144',
                'actividad' => 'Cría de ganado porcino',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            20 => 
            array (
                'id' => 21,
                'codigo' => '145',
                'actividad' => 'Cría de aves de corral',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            21 => 
            array (
                'id' => 22,
                'codigo' => '149',
                'actividad' => 'Cría de otros animales n.c.p.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            22 => 
            array (
                'id' => 23,
                'codigo' => '150',
            'actividad' => 'Explotación mixta (agrícola y pecuaria)',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            23 => 
            array (
                'id' => 24,
                'codigo' => '161',
                'actividad' => 'Actividades de apoyo a la agricultura',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            24 => 
            array (
                'id' => 25,
                'codigo' => '162',
                'actividad' => 'Actividades de apoyo a la ganadería',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            25 => 
            array (
                'id' => 26,
                'codigo' => '163',
                'actividad' => 'Actividades posteriores a la cosecha',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            26 => 
            array (
                'id' => 27,
                'codigo' => '164',
                'actividad' => 'Tratamiento de semillas para propagación',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            27 => 
            array (
                'id' => 28,
                'codigo' => '170',
                'actividad' => 'Caza ordinaria y mediante trampas y actividades de servicios conexas',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            28 => 
            array (
                'id' => 29,
                'codigo' => '210',
                'actividad' => 'Silvicultura y otras actividades forestales',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            29 => 
            array (
                'id' => 30,
                'codigo' => '220',
                'actividad' => 'Extracción de madera',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            30 => 
            array (
                'id' => 31,
                'codigo' => '230',
                'actividad' => 'Recolección de productos forestales diferentes a la madera',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            31 => 
            array (
                'id' => 32,
                'codigo' => '240',
                'actividad' => 'Servicios de apoyo a la silvicultura',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            32 => 
            array (
                'id' => 33,
                'codigo' => '311',
                'actividad' => 'Pesca marítima',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            33 => 
            array (
                'id' => 34,
                'codigo' => '312',
                'actividad' => 'Pesca de agua dulce',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            34 => 
            array (
                'id' => 35,
                'codigo' => '321',
                'actividad' => 'Acuicultura marítima',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            35 => 
            array (
                'id' => 36,
                'codigo' => '322',
                'actividad' => 'Acuicultura de agua dulce',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            36 => 
            array (
                'id' => 37,
                'codigo' => '510',
            'actividad' => 'Extracción de hulla (carbón de piedra)',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            37 => 
            array (
                'id' => 38,
                'codigo' => '520',
                'actividad' => 'Extracción de carbón lignito',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            38 => 
            array (
                'id' => 39,
                'codigo' => '610',
                'actividad' => 'Extracción de petróleo crudo',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            39 => 
            array (
                'id' => 40,
                'codigo' => '620',
                'actividad' => 'Extracción de gas natural',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            40 => 
            array (
                'id' => 41,
                'codigo' => '710',
                'actividad' => 'Extracción de minerales de hierro',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            41 => 
            array (
                'id' => 42,
                'codigo' => '721',
                'actividad' => 'Extracción de minerales de uranio y de torio',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            42 => 
            array (
                'id' => 43,
                'codigo' => '722',
                'actividad' => 'Extracción de oro y otros metales preciosos',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            43 => 
            array (
                'id' => 44,
                'codigo' => '723',
                'actividad' => 'Extracción de minerales de níquel',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            44 => 
            array (
                'id' => 45,
                'codigo' => '729',
                'actividad' => 'Extracción de otros minerales metalíferos no ferrosos n.c.p.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            45 => 
            array (
                'id' => 46,
                'codigo' => '811',
                'actividad' => 'Extracción de piedra, arena, arcillas comunes, yeso y anhidrita',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            46 => 
            array (
                'id' => 47,
                'codigo' => '812',
                'actividad' => 'Extracción de arcillas de uso industrial, caliza, caolín y bentonitas',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            47 => 
            array (
                'id' => 48,
                'codigo' => '820',
                'actividad' => 'Extracción de esmeraldas, piedras preciosas y semipreciosas',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            48 => 
            array (
                'id' => 49,
                'codigo' => '891',
                'actividad' => 'Extracción de minerales para la fabricación de abonos y productos químicos',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            49 => 
            array (
                'id' => 50,
                'codigo' => '892',
            'actividad' => 'Extracción de halita (sal)',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            50 => 
            array (
                'id' => 51,
                'codigo' => '899',
                'actividad' => 'Extracción de otros minerales no metálicos n.c.p.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            51 => 
            array (
                'id' => 52,
                'codigo' => '910',
                'actividad' => 'Actividades de apoyo para la extracción de petróleo y de gas natural',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            52 => 
            array (
                'id' => 53,
                'codigo' => '990',
                'actividad' => 'Actividades de apoyo para otras actividades de explotación de minas y canteras',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            53 => 
            array (
                'id' => 54,
                'codigo' => '1011',
                'actividad' => 'Procesamiento y conservación de carne y productos cárnicos',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            54 => 
            array (
                'id' => 55,
                'codigo' => '1012',
                'actividad' => 'Procesamiento y conservación de pescados, crustáceos y moluscos',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            55 => 
            array (
                'id' => 56,
                'codigo' => '1020',
                'actividad' => 'Procesamiento y conservación de frutas, legumbres, hortalizas y tubérculos',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            56 => 
            array (
                'id' => 57,
                'codigo' => '1030',
                'actividad' => 'Elaboración de aceites y grasas de origen vegetal y animal',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            57 => 
            array (
                'id' => 58,
                'codigo' => '1040',
                'actividad' => 'Elaboración de productos lácteos',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            58 => 
            array (
                'id' => 59,
                'codigo' => '1051',
                'actividad' => 'Elaboración de productos de molinería',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            59 => 
            array (
                'id' => 60,
                'codigo' => '1052',
                'actividad' => 'Elaboración de almidones y productos derivados del almidón',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            60 => 
            array (
                'id' => 61,
                'codigo' => '1061',
                'actividad' => 'Trilla de café',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            61 => 
            array (
                'id' => 62,
                'codigo' => '1062',
                'actividad' => 'Descafeinado, tostión y molienda del café',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            62 => 
            array (
                'id' => 63,
                'codigo' => '1063',
                'actividad' => 'Otros derivados del café',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            63 => 
            array (
                'id' => 64,
                'codigo' => '1071',
                'actividad' => 'Elaboración y refinación de azúcar',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            64 => 
            array (
                'id' => 65,
                'codigo' => '1072',
                'actividad' => 'Elaboración de panela',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            65 => 
            array (
                'id' => 66,
                'codigo' => '1081',
                'actividad' => 'Elaboración de productos de panadería',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            66 => 
            array (
                'id' => 67,
                'codigo' => '1082',
                'actividad' => 'Elaboración de cacao, chocolate y productos de confitería',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            67 => 
            array (
                'id' => 68,
                'codigo' => '1083',
                'actividad' => 'Elaboración de macarrones, fideos, alcuzcuz y productos farináceos similares',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            68 => 
            array (
                'id' => 69,
                'codigo' => '1084',
                'actividad' => 'Elaboración de comidas y platos preparados',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            69 => 
            array (
                'id' => 70,
                'codigo' => '1089',
                'actividad' => 'Elaboración de otros productos alimenticios n.c.p.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            70 => 
            array (
                'id' => 71,
                'codigo' => '1090',
                'actividad' => 'Elaboración de alimentos preparados para animales',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            71 => 
            array (
                'id' => 72,
                'codigo' => '1101',
                'actividad' => 'Destilación, rectificación y mezcla de bebidas alcohólicas',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            72 => 
            array (
                'id' => 73,
                'codigo' => '1102',
                'actividad' => 'Elaboración de bebidas fermentadas no destiladas',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            73 => 
            array (
                'id' => 74,
                'codigo' => '1103',
                'actividad' => 'Producción de malta, elaboración de cervezas y otras bebidas malteadas',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            74 => 
            array (
                'id' => 75,
                'codigo' => '1104',
                'actividad' => 'Elaboración de bebidas no alcohólicas, producción de aguas minerales y de otras aguas embotelladas',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            75 => 
            array (
                'id' => 76,
                'codigo' => '1200',
                'actividad' => 'Elaboración de productos de tabaco',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            76 => 
            array (
                'id' => 77,
                'codigo' => '1311',
                'actividad' => 'Preparación e hilatura de fibras textiles',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            77 => 
            array (
                'id' => 78,
                'codigo' => '1312',
                'actividad' => 'Tejeduría de productos textiles',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            78 => 
            array (
                'id' => 79,
                'codigo' => '1313',
                'actividad' => 'Acabado de productos textiles',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            79 => 
            array (
                'id' => 80,
                'codigo' => '1391',
                'actividad' => 'Fabricación de tejidos de punto y ganchillo',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            80 => 
            array (
                'id' => 81,
                'codigo' => '1392',
                'actividad' => 'Confección de artículos con materiales textiles, excepto prendas de vestir',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            81 => 
            array (
                'id' => 82,
                'codigo' => '1393',
                'actividad' => 'Fabricación de tapetes y alfombras para pisos',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            82 => 
            array (
                'id' => 83,
                'codigo' => '1394',
                'actividad' => 'Fabricación de cuerdas, cordeles, cables, bramantes y redes',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            83 => 
            array (
                'id' => 84,
                'codigo' => '1399',
                'actividad' => 'Fabricación de otros artículos textiles n.c.p.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            84 => 
            array (
                'id' => 85,
                'codigo' => '1410',
                'actividad' => 'Confección de prendas de vestir, excepto prendas de piel',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            85 => 
            array (
                'id' => 86,
                'codigo' => '1420',
                'actividad' => 'Fabricación de artículos de piel',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            86 => 
            array (
                'id' => 87,
                'codigo' => '1430',
                'actividad' => 'Fabricación de artículos de punto y ganchillo',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            87 => 
            array (
                'id' => 88,
                'codigo' => '1511',
                'actividad' => 'Curtido y recurtido de cueros; recurtido y teñido de pieles',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            88 => 
            array (
                'id' => 89,
                'codigo' => '1512',
                'actividad' => 'Fabricación de artículos de viaje, bolsos de mano y artículos similares elaborados en cuero, y fabricación de artículos de talabartería y guarnicionería',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            89 => 
            array (
                'id' => 90,
                'codigo' => '1513',
                'actividad' => 'Fabricación de artículos de viaje, bolsos de mano y artículos similares; artículos de talabartería y guarnicionería elaborados en otros materiales',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            90 => 
            array (
                'id' => 91,
                'codigo' => '1521',
                'actividad' => 'Fabricación de calzado de cuero y piel, con cualquier tipo de suela',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            91 => 
            array (
                'id' => 92,
                'codigo' => '1522',
                'actividad' => 'Fabricación de otros tipos de calzado, excepto calzado de cuero y piel',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            92 => 
            array (
                'id' => 93,
                'codigo' => '1523',
                'actividad' => 'Fabricación de partes del calzado',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            93 => 
            array (
                'id' => 94,
                'codigo' => '1610',
                'actividad' => 'Aserrado, acepillado e impregnación de la madera',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            94 => 
            array (
                'id' => 95,
                'codigo' => '1620',
                'actividad' => 'Fabricación de hojas de madera para enchapado; fabricación de tableros contrachapados, tableros laminados, tableros de partículas y otros tableros y paneles',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            95 => 
            array (
                'id' => 96,
                'codigo' => '1630',
                'actividad' => 'Fabricación de partes y piezas de madera, de carpintería y ebanistería para la construcción',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            96 => 
            array (
                'id' => 97,
                'codigo' => '1640',
                'actividad' => 'Fabricación de recipientes de madera',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            97 => 
            array (
                'id' => 98,
                'codigo' => '1690',
                'actividad' => 'Fabricación de otros productos de madera; fabricación de artículos de corcho, cestería y espartería',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            98 => 
            array (
                'id' => 99,
                'codigo' => '1701',
            'actividad' => 'Fabricación de pulpas (pastas) celulósicas; papel y cartón',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            99 => 
            array (
                'id' => 100,
                'codigo' => '1702',
            'actividad' => 'Fabricación de papel y cartón ondulado (corrugado); fabricación de envases, empaques y de embalajes de papel y cartón.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            100 => 
            array (
                'id' => 101,
                'codigo' => '1709',
                'actividad' => 'Fabricación de otros artículos de papel y cartón',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            101 => 
            array (
                'id' => 102,
                'codigo' => '1811',
                'actividad' => 'Actividades de impresión',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            102 => 
            array (
                'id' => 103,
                'codigo' => '1812',
                'actividad' => 'Actividades de servicios relacionados con la impresión',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            103 => 
            array (
                'id' => 104,
                'codigo' => '1820',
                'actividad' => 'Producción de copias a partir de grabaciones originales',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            104 => 
            array (
                'id' => 105,
                'codigo' => '1910',
                'actividad' => 'Fabricación de productos de hornos de coque',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            105 => 
            array (
                'id' => 106,
                'codigo' => '1921',
                'actividad' => 'Fabricación de productos de la refinación del petróleo',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            106 => 
            array (
                'id' => 107,
                'codigo' => '1922',
                'actividad' => 'Actividad de mezcla de combustibles',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            107 => 
            array (
                'id' => 108,
                'codigo' => '2011',
                'actividad' => 'Fabricación de sustancias y productos químicos básicos',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            108 => 
            array (
                'id' => 109,
                'codigo' => '2012',
                'actividad' => 'Fabricación de abonos y compuestos inorgánicos nitrogenados',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            109 => 
            array (
                'id' => 110,
                'codigo' => '2013',
                'actividad' => 'Fabricación de plásticos en formas primarias',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            110 => 
            array (
                'id' => 111,
                'codigo' => '2014',
                'actividad' => 'Fabricación de caucho sintético en formas primarias',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            111 => 
            array (
                'id' => 112,
                'codigo' => '2021',
                'actividad' => 'Fabricación de plaguicidas y otros productos químicos de uso agropecuario',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            112 => 
            array (
                'id' => 113,
                'codigo' => '2022',
                'actividad' => 'Fabricación de pinturas, barnices y revestimientos similares, tintas para impresión y masillas',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            113 => 
            array (
                'id' => 114,
                'codigo' => '2023',
                'actividad' => 'Fabricación de jabones y detergentes, preparados para limpiar y pulir; perfumes y preparados de tocador',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            114 => 
            array (
                'id' => 115,
                'codigo' => '2029',
                'actividad' => 'Fabricación de otros productos químicos n.c.p.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            115 => 
            array (
                'id' => 116,
                'codigo' => '2030',
                'actividad' => 'Fabricación de fibras sintéticas y artificiales',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            116 => 
            array (
                'id' => 117,
                'codigo' => '2100',
                'actividad' => 'Fabricación de productos farmacéuticos, sustancias químicas medicinales y productos botánicos de uso farmacéutico',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            117 => 
            array (
                'id' => 118,
                'codigo' => '2211',
                'actividad' => 'Fabricación de llantas y neumáticos de caucho',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            118 => 
            array (
                'id' => 119,
                'codigo' => '2212',
                'actividad' => 'Reencauche de llantas usadas',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            119 => 
            array (
                'id' => 120,
                'codigo' => '2219',
                'actividad' => 'Fabricación de formas básicas de caucho y otros productos de caucho n.c.p.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            120 => 
            array (
                'id' => 121,
                'codigo' => '2221',
                'actividad' => 'Fabricación de formas básicas de plástico',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            121 => 
            array (
                'id' => 122,
                'codigo' => '2229',
                'actividad' => 'Fabricación de artículos de plástico n.c.p.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            122 => 
            array (
                'id' => 123,
                'codigo' => '2310',
                'actividad' => 'Fabricación de vidrio y productos de vidrio',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            123 => 
            array (
                'id' => 124,
                'codigo' => '2391',
                'actividad' => 'Fabricación de productos refractarios',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            124 => 
            array (
                'id' => 125,
                'codigo' => '2392',
                'actividad' => 'Fabricación de materiales de arcilla para la construcción',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            125 => 
            array (
                'id' => 126,
                'codigo' => '2393',
                'actividad' => 'Fabricación de otros productos de cerámica y porcelana',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            126 => 
            array (
                'id' => 127,
                'codigo' => '2394',
                'actividad' => 'Fabricación de cemento, cal y yeso',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            127 => 
            array (
                'id' => 128,
                'codigo' => '2395',
                'actividad' => 'Fabricación de artículos de hormigón, cemento y yeso',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            128 => 
            array (
                'id' => 129,
                'codigo' => '2396',
                'actividad' => 'Corte, tallado y acabado de la piedra',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            129 => 
            array (
                'id' => 130,
                'codigo' => '2399',
                'actividad' => 'Fabricación de otros productos minerales no metálicos n.c.p.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            130 => 
            array (
                'id' => 131,
                'codigo' => '2410',
                'actividad' => 'Industrias básicas de hierro y de acero',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            131 => 
            array (
                'id' => 132,
                'codigo' => '2421',
                'actividad' => 'Industrias básicas de metales preciosos',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            132 => 
            array (
                'id' => 133,
                'codigo' => '2429',
                'actividad' => 'Industrias básicas de otros metales no ferrosos',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            133 => 
            array (
                'id' => 134,
                'codigo' => '2431',
                'actividad' => 'Fundición de hierro y de acero',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            134 => 
            array (
                'id' => 135,
                'codigo' => '2432',
                'actividad' => 'Fundición de metales no ferrosos',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            135 => 
            array (
                'id' => 136,
                'codigo' => '2511',
                'actividad' => 'Fabricación de productos metálicos para uso estructural',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            136 => 
            array (
                'id' => 137,
                'codigo' => '2512',
                'actividad' => 'Fabricación de tanques, depósitos y recipientes de metal, excepto los utilizados para el envase o transporte de mercancías',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            137 => 
            array (
                'id' => 138,
                'codigo' => '2513',
                'actividad' => 'Fabricación de generadores de vapor, excepto calderas de agua caliente para calefacción central',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            138 => 
            array (
                'id' => 139,
                'codigo' => '2520',
                'actividad' => 'Fabricación de armas y municiones',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            139 => 
            array (
                'id' => 140,
                'codigo' => '2591',
                'actividad' => 'Forja, prensado, estampado y laminado de metal; pulvimetalurgia',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            140 => 
            array (
                'id' => 141,
                'codigo' => '2592',
                'actividad' => 'Tratamiento y revestimiento de metales; mecanizado',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            141 => 
            array (
                'id' => 142,
                'codigo' => '2593',
                'actividad' => 'Fabricación de artículos de cuchillería, herramientas de mano y artículos de ferretería',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            142 => 
            array (
                'id' => 143,
                'codigo' => '2599',
                'actividad' => 'Fabricación de otros productos elaborados de metal n.c.p.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            143 => 
            array (
                'id' => 144,
                'codigo' => '2610',
                'actividad' => 'Fabricación de componentes y tableros electrónicos',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            144 => 
            array (
                'id' => 145,
                'codigo' => '2620',
                'actividad' => 'Fabricación de computadoras y de equipo periférico',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            145 => 
            array (
                'id' => 146,
                'codigo' => '2630',
                'actividad' => 'Fabricación de equipos de comunicación',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            146 => 
            array (
                'id' => 147,
                'codigo' => '2640',
                'actividad' => 'Fabricación de aparatos electrónicos de consumo',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            147 => 
            array (
                'id' => 148,
                'codigo' => '2651',
                'actividad' => 'Fabricación de equipo de medición, prueba, navegación y control',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            148 => 
            array (
                'id' => 149,
                'codigo' => '2652',
                'actividad' => 'Fabricación de relojes',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            149 => 
            array (
                'id' => 150,
                'codigo' => '2660',
                'actividad' => 'Fabricación de equipo de irradiación y equipo electrónico de uso médico y terapéutico',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            150 => 
            array (
                'id' => 151,
                'codigo' => '2670',
                'actividad' => 'Fabricación de instrumentos ópticos y equipo fotográfico',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            151 => 
            array (
                'id' => 152,
                'codigo' => '2680',
                'actividad' => 'Fabricación de medios magnéticos y ópticos para almacenamiento de datos',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            152 => 
            array (
                'id' => 153,
                'codigo' => '2711',
                'actividad' => 'Fabricación de motores, generadores y transformadores eléctricos',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            153 => 
            array (
                'id' => 154,
                'codigo' => '2712',
                'actividad' => 'Fabricación de aparatos de distribución y control de la energía eléctrica',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            154 => 
            array (
                'id' => 155,
                'codigo' => '2720',
                'actividad' => 'Fabricación de pilas, baterías y acumuladores eléctricos',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            155 => 
            array (
                'id' => 156,
                'codigo' => '2731',
                'actividad' => 'Fabricación de hilos y cables eléctricos y de fibra óptica',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            156 => 
            array (
                'id' => 157,
                'codigo' => '2732',
                'actividad' => 'Fabricación de dispositivos de cableado',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            157 => 
            array (
                'id' => 158,
                'codigo' => '2740',
                'actividad' => 'Fabricación de equipos eléctricos de iluminación',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            158 => 
            array (
                'id' => 159,
                'codigo' => '2750',
                'actividad' => 'Fabricación de aparatos de uso doméstico',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            159 => 
            array (
                'id' => 160,
                'codigo' => '2790',
                'actividad' => 'Fabricación de otros tipos de equipo eléctrico n.c.p.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            160 => 
            array (
                'id' => 161,
                'codigo' => '2811',
                'actividad' => 'Fabricación de motores, turbinas, y partes para motores de combustión interna',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            161 => 
            array (
                'id' => 162,
                'codigo' => '2812',
                'actividad' => 'Fabricación de equipos de potencia hidráulica y neumática',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            162 => 
            array (
                'id' => 163,
                'codigo' => '2813',
                'actividad' => 'Fabricación de otras bombas, compresores, grifos y válvulas',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            163 => 
            array (
                'id' => 164,
                'codigo' => '2814',
                'actividad' => 'Fabricación de cojinetes, engranajes, trenes de engranajes y piezas de transmisión',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            164 => 
            array (
                'id' => 165,
                'codigo' => '2815',
                'actividad' => 'Fabricación de hornos, hogares y quemadores industriales',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            165 => 
            array (
                'id' => 166,
                'codigo' => '2816',
                'actividad' => 'Fabricación de equipo de elevación y manipulación',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            166 => 
            array (
                'id' => 167,
                'codigo' => '2817',
            'actividad' => 'Fabricación de maquinaria y equipo de oficina (excepto computadoras y equipo periférico)',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            167 => 
            array (
                'id' => 168,
                'codigo' => '2818',
                'actividad' => 'Fabricación de herramientas manuales con motor',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            168 => 
            array (
                'id' => 169,
                'codigo' => '2819',
                'actividad' => 'Fabricación de otros tipos de maquinaria y equipo de uso general n.c.p.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            169 => 
            array (
                'id' => 170,
                'codigo' => '2821',
                'actividad' => 'Fabricación de maquinaria agropecuaria y forestal',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            170 => 
            array (
                'id' => 171,
                'codigo' => '2822',
                'actividad' => 'Fabricación de máquinas formadoras de metal y de máquinas herramienta',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            171 => 
            array (
                'id' => 172,
                'codigo' => '2823',
                'actividad' => 'Fabricación de maquinaria para la metalurgia',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            172 => 
            array (
                'id' => 173,
                'codigo' => '2824',
                'actividad' => 'Fabricación de maquinaria para explotación de minas y canteras y para obras de construcción',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            173 => 
            array (
                'id' => 174,
                'codigo' => '2825',
                'actividad' => 'Fabricación de maquinaria para la elaboración de alimentos, bebidas y tabaco',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            174 => 
            array (
                'id' => 175,
                'codigo' => '2826',
                'actividad' => 'Fabricación de maquinaria para la elaboración de productos textiles, prendas de vestir y cueros',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            175 => 
            array (
                'id' => 176,
                'codigo' => '2829',
                'actividad' => 'Fabricación de otros tipos de maquinaria y equipo de uso especial n.c.p.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            176 => 
            array (
                'id' => 177,
                'codigo' => '2910',
                'actividad' => 'Fabricación de vehículos automotores y sus motores',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            177 => 
            array (
                'id' => 178,
                'codigo' => '2920',
                'actividad' => 'Fabricación de carrocerías para vehículos automotores; fabricación de remolques y semirremolques',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            178 => 
            array (
                'id' => 179,
                'codigo' => '2930',
            'actividad' => 'Fabricación de partes, piezas (autopartes) y accesorios (lujos) para vehículos automotores',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            179 => 
            array (
                'id' => 180,
                'codigo' => '3011',
                'actividad' => 'Construcción de barcos y de estructuras flotantes',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            180 => 
            array (
                'id' => 181,
                'codigo' => '3012',
                'actividad' => 'Construcción de embarcaciones de recreo y deporte',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            181 => 
            array (
                'id' => 182,
                'codigo' => '3020',
                'actividad' => 'Fabricación de locomotoras y de material rodante para ferrocarriles',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            182 => 
            array (
                'id' => 183,
                'codigo' => '3030',
                'actividad' => 'Fabricación de aeronaves, naves espaciales y de maquinaria conexa',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            183 => 
            array (
                'id' => 184,
                'codigo' => '3040',
                'actividad' => 'Fabricación de vehículos militares de combate',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            184 => 
            array (
                'id' => 185,
                'codigo' => '3091',
                'actividad' => 'Fabricación de motocicletas',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            185 => 
            array (
                'id' => 186,
                'codigo' => '3092',
                'actividad' => 'Fabricación de bicicletas y de sillas de ruedas para personas con discapacidad',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            186 => 
            array (
                'id' => 187,
                'codigo' => '3099',
                'actividad' => 'Fabricación de otros tipos de equipo de transporte n.c.p.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            187 => 
            array (
                'id' => 188,
                'codigo' => '3110',
                'actividad' => 'Fabricación de muebles',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            188 => 
            array (
                'id' => 189,
                'codigo' => '3120',
                'actividad' => 'Fabricación de colchones y somieres',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            189 => 
            array (
                'id' => 190,
                'codigo' => '3210',
                'actividad' => 'Fabricación de joyas, bisutería y artículos conexos',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            190 => 
            array (
                'id' => 191,
                'codigo' => '3220',
                'actividad' => 'Fabricación de instrumentos musicales',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            191 => 
            array (
                'id' => 192,
                'codigo' => '3230',
                'actividad' => 'Fabricación de artículos y equipo para la práctica del deporte',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            192 => 
            array (
                'id' => 193,
                'codigo' => '3240',
                'actividad' => 'Fabricación de juegos, juguetes y rompecabezas',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            193 => 
            array (
                'id' => 194,
                'codigo' => '3250',
            'actividad' => 'Fabricación de instrumentos, aparatos y materiales médicos y odontológicos (incluido mobiliario)',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            194 => 
            array (
                'id' => 195,
                'codigo' => '3290',
                'actividad' => 'Otras industrias manufactureras n.c.p.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            195 => 
            array (
                'id' => 196,
                'codigo' => '3311',
                'actividad' => 'Mantenimiento y reparación especializado de productos elaborados en metal',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            196 => 
            array (
                'id' => 197,
                'codigo' => '3312',
                'actividad' => 'Mantenimiento y reparación especializado de maquinaria y equipo',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            197 => 
            array (
                'id' => 198,
                'codigo' => '3313',
                'actividad' => 'Mantenimiento y reparación especializado de equipo electrónico y óptico',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            198 => 
            array (
                'id' => 199,
                'codigo' => '3314',
                'actividad' => 'Mantenimiento y reparación especializado de equipo eléctrico',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            199 => 
            array (
                'id' => 200,
                'codigo' => '3315',
                'actividad' => 'Mantenimiento y reparación especializado de equipo de transporte, excepto los vehículos automotores, motocicletas y bicicletas',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            200 => 
            array (
                'id' => 201,
                'codigo' => '3319',
                'actividad' => 'Mantenimiento y reparación de otros tipos de equipos y sus componentes n.c.p.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            201 => 
            array (
                'id' => 202,
                'codigo' => '3320',
                'actividad' => 'Instalación especializada de maquinaria y equipo industrial',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            202 => 
            array (
                'id' => 203,
                'codigo' => '3511',
                'actividad' => 'Generación de energía eléctrica',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            203 => 
            array (
                'id' => 204,
                'codigo' => '3512',
                'actividad' => 'Transmisión de energía eléctrica',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            204 => 
            array (
                'id' => 205,
                'codigo' => '3513',
                'actividad' => 'Distribución de energía eléctrica',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            205 => 
            array (
                'id' => 206,
                'codigo' => '3514',
                'actividad' => 'Comercialización de energía eléctrica',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            206 => 
            array (
                'id' => 207,
                'codigo' => '3520',
                'actividad' => 'Producción de gas; distribución de combustibles gaseosos por tuberías',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            207 => 
            array (
                'id' => 208,
                'codigo' => '3530',
                'actividad' => 'Suministro de vapor y aire acondicionado',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            208 => 
            array (
                'id' => 209,
                'codigo' => '3600',
                'actividad' => 'Captación, tratamiento y distribución de agua',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            209 => 
            array (
                'id' => 210,
                'codigo' => '3700',
                'actividad' => 'Evacuación y tratamiento de aguas residuales',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            210 => 
            array (
                'id' => 211,
                'codigo' => '3811',
                'actividad' => 'Recolección de desechos no peligrosos',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            211 => 
            array (
                'id' => 212,
                'codigo' => '3812',
                'actividad' => 'Recolección de desechos peligrosos',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            212 => 
            array (
                'id' => 213,
                'codigo' => '3821',
                'actividad' => 'Tratamiento y disposición de desechos no peligrosos',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            213 => 
            array (
                'id' => 214,
                'codigo' => '3822',
                'actividad' => 'Tratamiento y disposición de desechos peligrosos',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            214 => 
            array (
                'id' => 215,
                'codigo' => '3830',
                'actividad' => 'Recuperación de materiales',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            215 => 
            array (
                'id' => 216,
                'codigo' => '3900',
                'actividad' => 'Actividades de saneamiento ambiental y otros servicios de gestión de desechos',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            216 => 
            array (
                'id' => 217,
                'codigo' => '4111',
                'actividad' => 'Construcción de edificios residenciales',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            217 => 
            array (
                'id' => 218,
                'codigo' => '4112',
                'actividad' => 'Construcción de edificios no residenciales',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            218 => 
            array (
                'id' => 219,
                'codigo' => '4210',
                'actividad' => 'Construcción de carreteras y vías de ferrocarril',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            219 => 
            array (
                'id' => 220,
                'codigo' => '4220',
                'actividad' => 'Construcción de proyectos de servicio público',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            220 => 
            array (
                'id' => 221,
                'codigo' => '4290',
                'actividad' => 'Construcción de otras obras de ingeniería civil',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            221 => 
            array (
                'id' => 222,
                'codigo' => '4311',
                'actividad' => 'Demolición',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            222 => 
            array (
                'id' => 223,
                'codigo' => '4312',
                'actividad' => 'Preparación del terreno',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            223 => 
            array (
                'id' => 224,
                'codigo' => '4321',
                'actividad' => 'Instalaciones eléctricas',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            224 => 
            array (
                'id' => 225,
                'codigo' => '4322',
                'actividad' => 'Instalaciones de fontanería, calefacción y aire acondicionado',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            225 => 
            array (
                'id' => 226,
                'codigo' => '4329',
                'actividad' => 'Otras instalaciones especializadas',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            226 => 
            array (
                'id' => 227,
                'codigo' => '4330',
                'actividad' => 'Terminación y acabado de edificios y obras de ingeniería civil',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            227 => 
            array (
                'id' => 228,
                'codigo' => '4390',
                'actividad' => 'Otras actividades especializadas para la construcción de edificios y obras de ingeniería civil',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            228 => 
            array (
                'id' => 229,
                'codigo' => '4511',
                'actividad' => 'Comercio de vehículos automotores nuevos',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            229 => 
            array (
                'id' => 230,
                'codigo' => '4512',
                'actividad' => 'Comercio de vehículos automotores usados',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            230 => 
            array (
                'id' => 231,
                'codigo' => '4520',
                'actividad' => 'Mantenimiento y reparación de vehículos automotores',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            231 => 
            array (
                'id' => 232,
                'codigo' => '4530',
            'actividad' => 'Comercio de partes, piezas (autopartes) y accesorios (lujos) para vehículos automotores',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            232 => 
            array (
                'id' => 233,
                'codigo' => '4541',
                'actividad' => 'Comercio de motocicletas y de sus partes, piezas y accesorios',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            233 => 
            array (
                'id' => 234,
                'codigo' => '4542',
                'actividad' => 'Mantenimiento y reparación de motocicletas y de sus partes y piezas',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            234 => 
            array (
                'id' => 235,
                'codigo' => '4610',
                'actividad' => 'Comercio al por mayor a cambio de una retribución o por contrata',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            235 => 
            array (
                'id' => 236,
                'codigo' => '4620',
                'actividad' => 'Comercio al por mayor de materias primas agropecuarias; animales vivos',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            236 => 
            array (
                'id' => 237,
                'codigo' => '4631',
                'actividad' => 'Comercio al por mayor de productos alimenticios',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            237 => 
            array (
                'id' => 238,
                'codigo' => '4632',
                'actividad' => 'Comercio al por mayor de bebidas y tabaco',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            238 => 
            array (
                'id' => 239,
                'codigo' => '4641',
                'actividad' => 'Comercio al por mayor de productos textiles, productos confeccionados para uso doméstico',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            239 => 
            array (
                'id' => 240,
                'codigo' => '4642',
                'actividad' => 'Comercio al por mayor de prendas de vestir',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            240 => 
            array (
                'id' => 241,
                'codigo' => '4643',
                'actividad' => 'Comercio al por mayor de calzado',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            241 => 
            array (
                'id' => 242,
                'codigo' => '4644',
                'actividad' => 'Comercio al por mayor de aparatos y equipo de uso doméstico',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            242 => 
            array (
                'id' => 243,
                'codigo' => '4645',
                'actividad' => 'Comercio al por mayor de productos farmacéuticos, medicinales, cosméticos y de tocador',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            243 => 
            array (
                'id' => 244,
                'codigo' => '4649',
                'actividad' => 'Comercio al por mayor de otros utensilios domésticos n.c.p.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            244 => 
            array (
                'id' => 245,
                'codigo' => '4651',
                'actividad' => 'Comercio al por mayor de computadores, equipo periférico y programas de informática',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            245 => 
            array (
                'id' => 246,
                'codigo' => '4652',
                'actividad' => 'Comercio al por mayor de equipo, partes y piezas electrónicos y de telecomunicaciones',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            246 => 
            array (
                'id' => 247,
                'codigo' => '4653',
                'actividad' => 'Comercio al por mayor de maquinaria y equipo agropecuarios',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            247 => 
            array (
                'id' => 248,
                'codigo' => '4659',
                'actividad' => 'Comercio al por mayor de otros tipos de maquinaria y equipo n.c.p.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            248 => 
            array (
                'id' => 249,
                'codigo' => '4661',
                'actividad' => 'Comercio al por mayor de combustibles sólidos, líquidos, gaseosos y productos conexos',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            249 => 
            array (
                'id' => 250,
                'codigo' => '4662',
                'actividad' => 'Comercio al por mayor de metales y productos metalíferos',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            250 => 
            array (
                'id' => 251,
                'codigo' => '4663',
                'actividad' => 'Comercio al por mayor de materiales de construcción, artículos de ferretería, pinturas, productos de vidrio, equipo y materiales de fontanería y calefacción',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            251 => 
            array (
                'id' => 252,
                'codigo' => '4664',
                'actividad' => 'Comercio al por mayor de productos químicos básicos, cauchos y plásticos en formas primarias y productos químicos de uso agropecuario',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            252 => 
            array (
                'id' => 253,
                'codigo' => '4665',
                'actividad' => 'Comercio al por mayor de desperdicios, desechos y chatarra',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            253 => 
            array (
                'id' => 254,
                'codigo' => '4669',
                'actividad' => 'Comercio al por mayor de otros productos n.c.p.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            254 => 
            array (
                'id' => 255,
                'codigo' => '4690',
                'actividad' => 'Comercio al por mayor no especializado',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            255 => 
            array (
                'id' => 256,
                'codigo' => '4711',
                'actividad' => 'Comercio al por menor en establecimientos no especializados con surtido compuesto principalmente por alimentos, bebidas o tabaco',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            256 => 
            array (
                'id' => 257,
                'codigo' => '4719',
            'actividad' => 'Comercio al por menor en establecimientos no especializados, con surtido compuesto principalmente por productos diferentes de alimentos (víveres en general), bebidas y tabaco',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            257 => 
            array (
                'id' => 258,
                'codigo' => '4721',
                'actividad' => 'Comercio al por menor de productos agrícolas para el consumo en establecimientos especializados',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            258 => 
            array (
                'id' => 259,
                'codigo' => '4722',
                'actividad' => 'Comercio al por menor de leche, productos lácteos y huevos, en establecimientos especializados',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            259 => 
            array (
                'id' => 260,
                'codigo' => '4723',
            'actividad' => 'Comercio al por menor de carnes (incluye aves de corral), productos cárnicos, pescados y productos de mar, en establecimientos especializados',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            260 => 
            array (
                'id' => 261,
                'codigo' => '4724',
                'actividad' => 'Comercio al por menor de bebidas y productos del tabaco, en establecimientos especializados',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            261 => 
            array (
                'id' => 262,
                'codigo' => '4729',
                'actividad' => 'Comercio al por menor de otros productos alimenticios n.c.p., en establecimientos especializados',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            262 => 
            array (
                'id' => 263,
                'codigo' => '4731',
                'actividad' => 'Comercio al por menor de combustible para automotores',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            263 => 
            array (
                'id' => 264,
                'codigo' => '4732',
            'actividad' => 'Comercio al por menor de lubricantes (aceites, grasas), aditivos y productos de limpieza para vehículos automotores',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            264 => 
            array (
                'id' => 265,
                'codigo' => '4741',
                'actividad' => 'Comercio al por menor de computadores, equipos periféricos, programas de informática y equipos de telecomunicaciones en establecimientos especializados',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            265 => 
            array (
                'id' => 266,
                'codigo' => '4742',
                'actividad' => 'Comercio al por menor de equipos y aparatos de sonido y de video, en establecimientos especializados',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            266 => 
            array (
                'id' => 267,
                'codigo' => '4751',
                'actividad' => 'Comercio al por menor de productos textiles en establecimientos especializados',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            267 => 
            array (
                'id' => 268,
                'codigo' => '4752',
                'actividad' => 'Comercio al por menor de artículos de ferretería, pinturas y productos de vidrio en establecimientos especializados',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            268 => 
            array (
                'id' => 269,
                'codigo' => '4753',
                'actividad' => 'Comercio al por menor de tapices, alfombras y cubrimientos para paredes y pisos en establecimientos especializados',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            269 => 
            array (
                'id' => 270,
                'codigo' => '4754',
                'actividad' => 'Comercio al por menor de electrodomésticos y gasodomésticos de uso doméstico, muebles y equipos de iluminación',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            270 => 
            array (
                'id' => 271,
                'codigo' => '4755',
                'actividad' => 'Comercio al por menor de artículos y utensilios de uso doméstico',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            271 => 
            array (
                'id' => 272,
                'codigo' => '4759',
                'actividad' => 'Comercio al por menor de otros artículos domésticos en establecimientos especializados',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            272 => 
            array (
                'id' => 273,
                'codigo' => '4761',
                'actividad' => 'Comercio al por menor de libros, periódicos, materiales y artículos de papelería y escritorio, en establecimientos especializados',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            273 => 
            array (
                'id' => 274,
                'codigo' => '4762',
                'actividad' => 'Comercio al por menor de artículos deportivos, en establecimientos especializados',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            274 => 
            array (
                'id' => 275,
                'codigo' => '4769',
                'actividad' => 'Comercio al por menor de otros artículos culturales y de entretenimiento n.c.p. en establecimientos especializados',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            275 => 
            array (
                'id' => 276,
                'codigo' => '4771',
            'actividad' => 'Comercio al por menor de prendas de vestir y sus accesorios (incluye artículos de piel) en establecimientos especializados',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            276 => 
            array (
                'id' => 277,
                'codigo' => '4772',
                'actividad' => 'Comercio al por menor de todo tipo de calzado y artículos de cuero y sucedáneos del cuero en establecimientos especializados.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            277 => 
            array (
                'id' => 278,
                'codigo' => '4773',
                'actividad' => 'Comercio al por menor de productos farmacéuticos y medicinales, cosméticos y artículos de tocador en establecimientos especializados',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            278 => 
            array (
                'id' => 279,
                'codigo' => '4774',
                'actividad' => 'Comercio al por menor de otros productos nuevos en establecimientos especializados',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            279 => 
            array (
                'id' => 280,
                'codigo' => '4775',
                'actividad' => 'Comercio al por menor de artículos de segunda mano',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            280 => 
            array (
                'id' => 281,
                'codigo' => '4781',
                'actividad' => 'Comercio al por menor de alimentos, bebidas y tabaco, en puestos de venta móviles',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            281 => 
            array (
                'id' => 282,
                'codigo' => '4782',
                'actividad' => 'Comercio al por menor de productos textiles, prendas de vestir y calzado, en puestos de venta móviles',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            282 => 
            array (
                'id' => 283,
                'codigo' => '4789',
                'actividad' => 'Comercio al por menor de otros productos en puestos de venta móviles',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            283 => 
            array (
                'id' => 284,
                'codigo' => '4791',
                'actividad' => 'Comercio al por menor realizado a través de Internet',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            284 => 
            array (
                'id' => 285,
                'codigo' => '4792',
                'actividad' => 'Comercio al por menor realizado a través de casas de venta o por correo',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            285 => 
            array (
                'id' => 286,
                'codigo' => '4799',
                'actividad' => 'Otros tipos de comercio al por menor no realizado en establecimientos, puestos de venta o mercados.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            286 => 
            array (
                'id' => 287,
                'codigo' => '4911',
                'actividad' => 'Transporte férreo de pasajeros',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            287 => 
            array (
                'id' => 288,
                'codigo' => '4912',
                'actividad' => 'Transporte férreo de carga',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            288 => 
            array (
                'id' => 289,
                'codigo' => '4921',
                'actividad' => 'Transporte de pasajeros',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            289 => 
            array (
                'id' => 290,
                'codigo' => '4922',
                'actividad' => 'Transporte mixto',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            290 => 
            array (
                'id' => 291,
                'codigo' => '4923',
                'actividad' => 'Transporte de carga por carretera',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            291 => 
            array (
                'id' => 292,
                'codigo' => '4930',
                'actividad' => 'Transporte por tuberías',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            292 => 
            array (
                'id' => 293,
                'codigo' => '5011',
                'actividad' => 'Transporte de pasajeros marítimo y de cabotaje',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            293 => 
            array (
                'id' => 294,
                'codigo' => '5012',
                'actividad' => 'Transporte de carga marítimo y de cabotaje',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            294 => 
            array (
                'id' => 295,
                'codigo' => '5021',
                'actividad' => 'Transporte fluvial de pasajeros',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            295 => 
            array (
                'id' => 296,
                'codigo' => '5022',
                'actividad' => 'Transporte fluvial de carga',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            296 => 
            array (
                'id' => 297,
                'codigo' => '5111',
                'actividad' => 'Transporte aéreo nacional de pasajeros',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            297 => 
            array (
                'id' => 298,
                'codigo' => '5112',
                'actividad' => 'Transporte aéreo internacional de pasajeros',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            298 => 
            array (
                'id' => 299,
                'codigo' => '5121',
                'actividad' => 'Transporte aéreo nacional de carga',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            299 => 
            array (
                'id' => 300,
                'codigo' => '5122',
                'actividad' => 'Transporte aéreo internacional de carga',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            300 => 
            array (
                'id' => 301,
                'codigo' => '5210',
                'actividad' => 'Almacenamiento y depósito',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            301 => 
            array (
                'id' => 302,
                'codigo' => '5221',
                'actividad' => 'Actividades de estaciones, vías y servicios complementarios para el transporte terrestre',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            302 => 
            array (
                'id' => 303,
                'codigo' => '5222',
                'actividad' => 'Actividades de puertos y servicios complementarios para el transporte acuático',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            303 => 
            array (
                'id' => 304,
                'codigo' => '5223',
                'actividad' => 'Actividades de aeropuertos, servicios de navegación aérea y demás actividades conexas al transporte aéreo',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            304 => 
            array (
                'id' => 305,
                'codigo' => '5224',
                'actividad' => 'Manipulación de carga',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            305 => 
            array (
                'id' => 306,
                'codigo' => '5229',
                'actividad' => 'Otras actividades complementarias al transporte',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            306 => 
            array (
                'id' => 307,
                'codigo' => '5310',
                'actividad' => 'Actividades postales nacionales',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            307 => 
            array (
                'id' => 308,
                'codigo' => '5320',
                'actividad' => 'Actividades de mensajería',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            308 => 
            array (
                'id' => 309,
                'codigo' => '5511',
                'actividad' => 'Alojamiento en hoteles',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            309 => 
            array (
                'id' => 310,
                'codigo' => '5512',
                'actividad' => 'Alojamiento en apartahoteles',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            310 => 
            array (
                'id' => 311,
                'codigo' => '5513',
                'actividad' => 'Alojamiento en centros vacacionales',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            311 => 
            array (
                'id' => 312,
                'codigo' => '5514',
                'actividad' => 'Alojamiento rural',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            312 => 
            array (
                'id' => 313,
                'codigo' => '5519',
                'actividad' => 'Otros tipos de alojamientos para visitantes',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            313 => 
            array (
                'id' => 314,
                'codigo' => '5520',
                'actividad' => 'Actividades de zonas de camping y parques para vehículos recreacionales',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            314 => 
            array (
                'id' => 315,
                'codigo' => '5530',
                'actividad' => 'Servicio por horas',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            315 => 
            array (
                'id' => 316,
                'codigo' => '5590',
                'actividad' => 'Otros tipos de alojamiento n.c.p.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            316 => 
            array (
                'id' => 317,
                'codigo' => '5611',
                'actividad' => 'Expendio a la mesa de comidas preparadas',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            317 => 
            array (
                'id' => 318,
                'codigo' => '5612',
                'actividad' => 'Expendio por autoservicio de comidas preparadas',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            318 => 
            array (
                'id' => 319,
                'codigo' => '5613',
                'actividad' => 'Expendio de comidas preparadas en cafeterías',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            319 => 
            array (
                'id' => 320,
                'codigo' => '5619',
                'actividad' => 'Otros tipos de expendio de comidas preparadas n.c.p.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            320 => 
            array (
                'id' => 321,
                'codigo' => '5621',
                'actividad' => 'Catering para eventos',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            321 => 
            array (
                'id' => 322,
                'codigo' => '5629',
                'actividad' => 'Actividades de otros servicios de comidas',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            322 => 
            array (
                'id' => 323,
                'codigo' => '5630',
                'actividad' => 'Expendio de bebidas alcohólicas para el consumo dentro del establecimiento',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            323 => 
            array (
                'id' => 324,
                'codigo' => '5811',
                'actividad' => 'Edición de libros',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            324 => 
            array (
                'id' => 325,
                'codigo' => '5812',
                'actividad' => 'Edición de directorios y listas de correo',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            325 => 
            array (
                'id' => 326,
                'codigo' => '5813',
                'actividad' => 'Edición de periódicos, revistas y otras publicaciones periódicas',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            326 => 
            array (
                'id' => 327,
                'codigo' => '5819',
                'actividad' => 'Otros trabajos de edición',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            327 => 
            array (
                'id' => 328,
                'codigo' => '5820',
            'actividad' => 'Edición de programas de informática (software)',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            328 => 
            array (
                'id' => 329,
                'codigo' => '5911',
                'actividad' => 'Actividades de producción de películas cinematográficas, videos, programas, anuncios y comerciales de televisión',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            329 => 
            array (
                'id' => 330,
                'codigo' => '5912',
                'actividad' => 'Actividades de posproducción de películas cinematográficas, videos, programas, anuncios y comerciales de televisión',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            330 => 
            array (
                'id' => 331,
                'codigo' => '5913',
                'actividad' => 'Actividades de distribución de películas cinematográficas, videos, programas, anuncios y comerciales de televisión',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            331 => 
            array (
                'id' => 332,
                'codigo' => '5914',
                'actividad' => 'Actividades de exhibición de películas cinematográficas y videos',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            332 => 
            array (
                'id' => 333,
                'codigo' => '5920',
                'actividad' => 'Actividades de grabación de sonido y edición de música',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            333 => 
            array (
                'id' => 334,
                'codigo' => '6010',
                'actividad' => 'Actividades de programación y transmisión en el servicio de radiodifusión sonora',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            334 => 
            array (
                'id' => 335,
                'codigo' => '6020',
                'actividad' => 'Actividades de programación y transmisión de televisión',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            335 => 
            array (
                'id' => 336,
                'codigo' => '6110',
                'actividad' => 'Actividades de telecomunicaciones alámbricas',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            336 => 
            array (
                'id' => 337,
                'codigo' => '6120',
                'actividad' => 'Actividades de telecomunicaciones inalámbricas',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            337 => 
            array (
                'id' => 338,
                'codigo' => '6130',
                'actividad' => 'Actividades de telecomunicación satelital',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            338 => 
            array (
                'id' => 339,
                'codigo' => '6190',
                'actividad' => 'Otras actividades de telecomunicaciones',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            339 => 
            array (
                'id' => 340,
                'codigo' => '6201',
            'actividad' => 'Actividades de desarrollo de sistemas informáticos (planificación, análisis, diseño, programación, pruebas)',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            340 => 
            array (
                'id' => 341,
                'codigo' => '6202',
                'actividad' => 'Actividades de consultoría informática y actividades de administración de instalaciones informáticas',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            341 => 
            array (
                'id' => 342,
                'codigo' => '6209',
                'actividad' => 'Otras actividades de tecnologías de información y actividades de servicios informáticos',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            342 => 
            array (
                'id' => 343,
                'codigo' => '6311',
            'actividad' => 'Procesamiento de datos, alojamiento (hosting) y actividades relacionadas',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            343 => 
            array (
                'id' => 344,
                'codigo' => '6312',
                'actividad' => 'Portales web',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            344 => 
            array (
                'id' => 345,
                'codigo' => '6391',
                'actividad' => 'Actividades de agencias de noticias',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            345 => 
            array (
                'id' => 346,
                'codigo' => '6399',
                'actividad' => 'Otras actividades de servicio de información n.c.p.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            346 => 
            array (
                'id' => 347,
                'codigo' => '6411',
                'actividad' => 'Banco Central',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            347 => 
            array (
                'id' => 348,
                'codigo' => '6412',
                'actividad' => 'Bancos comerciales',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            348 => 
            array (
                'id' => 349,
                'codigo' => '6421',
                'actividad' => 'Actividades de las corporaciones financieras',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            349 => 
            array (
                'id' => 350,
                'codigo' => '6422',
                'actividad' => 'Actividades de las compañías de financiamiento',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            350 => 
            array (
                'id' => 351,
                'codigo' => '6423',
                'actividad' => 'Banca de segundo piso',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            351 => 
            array (
                'id' => 352,
                'codigo' => '6424',
                'actividad' => 'Actividades de las cooperativas financieras',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            352 => 
            array (
                'id' => 353,
                'codigo' => '6431',
                'actividad' => 'Fideicomisos, fondos y entidades financieras similares',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            353 => 
            array (
                'id' => 354,
                'codigo' => '6432',
                'actividad' => 'Fondos de cesantías',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            354 => 
            array (
                'id' => 355,
                'codigo' => '6491',
            'actividad' => 'Leasing financiero (arrendamiento financiero)',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            355 => 
            array (
                'id' => 356,
                'codigo' => '6492',
                'actividad' => 'Actividades financieras de fondos de empleados y otras formas asociativas del sector solidario',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            356 => 
            array (
                'id' => 357,
                'codigo' => '6493',
                'actividad' => 'Actividades de compra de cartera o factoring',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            357 => 
            array (
                'id' => 358,
                'codigo' => '6494',
                'actividad' => 'Otras actividades de distribución de fondos',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            358 => 
            array (
                'id' => 359,
                'codigo' => '6495',
                'actividad' => 'Instituciones especiales oficiales',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            359 => 
            array (
                'id' => 360,
                'codigo' => '6499',
                'actividad' => 'Otras actividades de servicio financiero, excepto las de seguros y pensiones n.c.p.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            360 => 
            array (
                'id' => 361,
                'codigo' => '6511',
                'actividad' => 'Seguros generales',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            361 => 
            array (
                'id' => 362,
                'codigo' => '6512',
                'actividad' => 'Seguros de vida',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            362 => 
            array (
                'id' => 363,
                'codigo' => '6513',
                'actividad' => 'Reaseguros',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            363 => 
            array (
                'id' => 364,
                'codigo' => '6514',
                'actividad' => 'Capitalización',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            364 => 
            array (
                'id' => 365,
                'codigo' => '6521',
                'actividad' => 'Servicios de seguros sociales de salud',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            365 => 
            array (
                'id' => 366,
                'codigo' => '6522',
                'actividad' => 'Servicios de seguros sociales de riesgos profesionales',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            366 => 
            array (
                'id' => 367,
                'codigo' => '6531',
            'actividad' => 'Régimen de prima media con prestación definida (RPM)',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            367 => 
            array (
                'id' => 368,
                'codigo' => '6532',
            'actividad' => 'Régimen de ahorro individual (RAI)',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            368 => 
            array (
                'id' => 369,
                'codigo' => '6611',
                'actividad' => 'Administración de mercados financieros',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            369 => 
            array (
                'id' => 370,
                'codigo' => '6612',
                'actividad' => 'Corretaje de valores y de contratos de productos básicos',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            370 => 
            array (
                'id' => 371,
                'codigo' => '6613',
                'actividad' => 'Otras actividades relacionadas con el mercado de valores',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            371 => 
            array (
                'id' => 372,
                'codigo' => '6614',
                'actividad' => 'Actividades de las casas de cambio',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            372 => 
            array (
                'id' => 373,
                'codigo' => '6615',
                'actividad' => 'Actividades de los profesionales de compra y venta de divisas',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            373 => 
            array (
                'id' => 374,
                'codigo' => '6619',
                'actividad' => 'Otras actividades auxiliares de las actividades de servicios financieros n.c.p.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            374 => 
            array (
                'id' => 375,
                'codigo' => '6621',
                'actividad' => 'Actividades de agentes y corredores de seguros',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            375 => 
            array (
                'id' => 376,
                'codigo' => '6629',
                'actividad' => 'Evaluación de riesgos y daños, y otras actividades de servicios auxiliares',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            376 => 
            array (
                'id' => 377,
                'codigo' => '6630',
                'actividad' => 'Actividades de administración de fondos',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            377 => 
            array (
                'id' => 378,
                'codigo' => '6810',
                'actividad' => 'Actividades inmobiliarias realizadas con bienes propios o arrendados',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            378 => 
            array (
                'id' => 379,
                'codigo' => '6820',
                'actividad' => 'Actividades inmobiliarias realizadas a cambio de una retribución o por contrata',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            379 => 
            array (
                'id' => 380,
                'codigo' => '6910',
                'actividad' => 'Actividades jurídicas',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            380 => 
            array (
                'id' => 381,
                'codigo' => '6920',
                'actividad' => 'Actividades de contabilidad, teneduría de libros, auditoría financiera y asesoría tributaria',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            381 => 
            array (
                'id' => 382,
                'codigo' => '7010',
                'actividad' => 'Actividades de administración empresarial',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            382 => 
            array (
                'id' => 383,
                'codigo' => '7020',
                'actividad' => 'Actividades de consultaría de gestión',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            383 => 
            array (
                'id' => 384,
                'codigo' => '7110',
                'actividad' => 'Actividades de arquitectura e ingeniería y otras actividades conexas de consultoría técnica',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            384 => 
            array (
                'id' => 385,
                'codigo' => '7120',
                'actividad' => 'Ensayos y análisis técnicos',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            385 => 
            array (
                'id' => 386,
                'codigo' => '7210',
                'actividad' => 'Investigaciones y desarrollo experimental en el campo de las ciencias naturales y la ingeniería',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            386 => 
            array (
                'id' => 387,
                'codigo' => '7220',
                'actividad' => 'Investigaciones y desarrollo experimental en el campo de las ciencias sociales y las humanidades',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            387 => 
            array (
                'id' => 388,
                'codigo' => '7310',
                'actividad' => 'Publicidad',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            388 => 
            array (
                'id' => 389,
                'codigo' => '7320',
                'actividad' => 'Estudios de mercado y realización de encuestas de opinión pública',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            389 => 
            array (
                'id' => 390,
                'codigo' => '7410',
                'actividad' => 'Actividades especializadas de diseño',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            390 => 
            array (
                'id' => 391,
                'codigo' => '7420',
                'actividad' => 'Actividades de fotografía',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            391 => 
            array (
                'id' => 392,
                'codigo' => '7490',
                'actividad' => 'Otras actividades profesionales, científicas y técnicas n.c.p.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            392 => 
            array (
                'id' => 393,
                'codigo' => '7500',
                'actividad' => 'Actividades veterinarias',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            393 => 
            array (
                'id' => 394,
                'codigo' => '7710',
                'actividad' => 'Alquiler y arrendamiento de vehículos automotores',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            394 => 
            array (
                'id' => 395,
                'codigo' => '7721',
                'actividad' => 'Alquiler y arrendamiento de equipo recreativo y deportivo',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            395 => 
            array (
                'id' => 396,
                'codigo' => '7722',
                'actividad' => 'Alquiler de videos y discos',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            396 => 
            array (
                'id' => 397,
                'codigo' => '7729',
                'actividad' => 'Alquiler y arrendamiento de otros efectos personales y enseres domésticos n.c.p.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            397 => 
            array (
                'id' => 398,
                'codigo' => '7730',
                'actividad' => 'Alquiler y arrendamiento de otros tipos de maquinaria, equipo y bienes tangibles n.c.p.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            398 => 
            array (
                'id' => 399,
                'codigo' => '7740',
                'actividad' => 'Arrendamiento de propiedad intelectual y productos similares, excepto obras protegidas por derechos de autor',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            399 => 
            array (
                'id' => 400,
                'codigo' => '7810',
                'actividad' => 'Actividades de agencias de empleo',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            400 => 
            array (
                'id' => 401,
                'codigo' => '7820',
                'actividad' => 'Actividades de agencias de empleo temporal',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            401 => 
            array (
                'id' => 402,
                'codigo' => '7830',
                'actividad' => 'Otras actividades de suministro de recurso humano',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            402 => 
            array (
                'id' => 403,
                'codigo' => '7911',
                'actividad' => 'Actividades de las agencias de viaje',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            403 => 
            array (
                'id' => 404,
                'codigo' => '7912',
                'actividad' => 'Actividades de operadores turísticos',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            404 => 
            array (
                'id' => 405,
                'codigo' => '7990',
                'actividad' => 'Otros servicios de reserva y actividades relacionadas',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            405 => 
            array (
                'id' => 406,
                'codigo' => '8010',
                'actividad' => 'Actividades de seguridad privada',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            406 => 
            array (
                'id' => 407,
                'codigo' => '8020',
                'actividad' => 'Actividades de servicios de sistemas de seguridad',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            407 => 
            array (
                'id' => 408,
                'codigo' => '8030',
                'actividad' => 'Actividades de detectives e investigadores privados',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            408 => 
            array (
                'id' => 409,
                'codigo' => '8110',
                'actividad' => 'Actividades combinadas de apoyo a instalaciones',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            409 => 
            array (
                'id' => 410,
                'codigo' => '8121',
                'actividad' => 'Limpieza general interior de edificios',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            410 => 
            array (
                'id' => 411,
                'codigo' => '8129',
                'actividad' => 'Otras actividades de limpieza de edificios e instalaciones industriales',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            411 => 
            array (
                'id' => 412,
                'codigo' => '8130',
                'actividad' => 'Actividades de paisajismo y servicios de mantenimiento conexos',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            412 => 
            array (
                'id' => 413,
                'codigo' => '8211',
                'actividad' => 'Actividades combinadas de servicios administrativos de oficina',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            413 => 
            array (
                'id' => 414,
                'codigo' => '8219',
                'actividad' => 'Fotocopiado, preparación de documentos y otras actividades especializadas de apoyo a oficina',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            414 => 
            array (
                'id' => 415,
                'codigo' => '8220',
            'actividad' => 'Actividades de centros de llamadas (Call center)',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            415 => 
            array (
                'id' => 416,
                'codigo' => '8230',
                'actividad' => 'Organización de convenciones y eventos comerciales',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            416 => 
            array (
                'id' => 417,
                'codigo' => '8291',
                'actividad' => 'Actividades de agencias de cobranza y oficinas de calificación crediticia',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            417 => 
            array (
                'id' => 418,
                'codigo' => '8292',
                'actividad' => 'Actividades de envase y empaque',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            418 => 
            array (
                'id' => 419,
                'codigo' => '8299',
                'actividad' => 'Otras actividades de servicio de apoyo a las empresas n.c.p.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            419 => 
            array (
                'id' => 420,
                'codigo' => '8411',
                'actividad' => 'Actividades legislativas de la administración pública',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            420 => 
            array (
                'id' => 421,
                'codigo' => '8412',
                'actividad' => 'Actividades ejecutivas de la administración pública',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            421 => 
            array (
                'id' => 422,
                'codigo' => '8413',
                'actividad' => 'Regulación de las actividades de organismos que prestan servicios de salud, educativos, culturales y otros servicios sociales, excepto servicios de seguridad social',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            422 => 
            array (
                'id' => 423,
                'codigo' => '8414',
                'actividad' => 'Actividades reguladoras y facilitadoras de la actividad económica',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            423 => 
            array (
                'id' => 424,
                'codigo' => '8415',
                'actividad' => 'Actividades de los otros órganos de control',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            424 => 
            array (
                'id' => 425,
                'codigo' => '8421',
                'actividad' => 'Relaciones exteriores',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            425 => 
            array (
                'id' => 426,
                'codigo' => '8422',
                'actividad' => 'Actividades de defensa',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            426 => 
            array (
                'id' => 427,
                'codigo' => '8423',
                'actividad' => 'Orden público y actividades de seguridad',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            427 => 
            array (
                'id' => 428,
                'codigo' => '8424',
                'actividad' => 'Administración de justicia',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            428 => 
            array (
                'id' => 429,
                'codigo' => '8430',
                'actividad' => 'Actividades de planes de seguridad social de afiliación obligatoria',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            429 => 
            array (
                'id' => 430,
                'codigo' => '8511',
                'actividad' => 'Educación de la primera infancia',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            430 => 
            array (
                'id' => 431,
                'codigo' => '8512',
                'actividad' => 'Educación preescolar',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            431 => 
            array (
                'id' => 432,
                'codigo' => '8513',
                'actividad' => 'Educación básica primaria',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            432 => 
            array (
                'id' => 433,
                'codigo' => '8521',
                'actividad' => 'Educación básica secundaria',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            433 => 
            array (
                'id' => 434,
                'codigo' => '8522',
                'actividad' => 'Educación media académica',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            434 => 
            array (
                'id' => 435,
                'codigo' => '8523',
                'actividad' => 'Educación media técnica y de formación laboral',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            435 => 
            array (
                'id' => 436,
                'codigo' => '8530',
                'actividad' => 'Establecimientos que combinan diferentes niveles de educación',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            436 => 
            array (
                'id' => 437,
                'codigo' => '8541',
                'actividad' => 'Educación técnica profesional',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            437 => 
            array (
                'id' => 438,
                'codigo' => '8542',
                'actividad' => 'Educación tecnológica',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            438 => 
            array (
                'id' => 439,
                'codigo' => '8543',
                'actividad' => 'Educación de instituciones universitarias o de escuelas tecnológicas',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            439 => 
            array (
                'id' => 440,
                'codigo' => '8544',
                'actividad' => 'Educación de universidades',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            440 => 
            array (
                'id' => 441,
                'codigo' => '8551',
                'actividad' => 'Formación académica no formal',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            441 => 
            array (
                'id' => 442,
                'codigo' => '8552',
                'actividad' => 'Enseñanza deportiva y recreativa',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            442 => 
            array (
                'id' => 443,
                'codigo' => '8553',
                'actividad' => 'Enseñanza cultural',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            443 => 
            array (
                'id' => 444,
                'codigo' => '8559',
                'actividad' => 'Otros tipos de educación n.c.p.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            444 => 
            array (
                'id' => 445,
                'codigo' => '8560',
                'actividad' => 'Actividades de apoyo a la educación',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            445 => 
            array (
                'id' => 446,
                'codigo' => '8610',
                'actividad' => 'Actividades de hospitales y clínicas, con internación',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            446 => 
            array (
                'id' => 447,
                'codigo' => '8621',
                'actividad' => 'Actividades de la práctica médica, sin internación',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            447 => 
            array (
                'id' => 448,
                'codigo' => '8622',
                'actividad' => 'Actividades de la práctica odontológica',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            448 => 
            array (
                'id' => 449,
                'codigo' => '8691',
                'actividad' => 'Actividades de apoyo diagnóstico',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            449 => 
            array (
                'id' => 450,
                'codigo' => '8692',
                'actividad' => 'Actividades de apoyo terapéutico',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            450 => 
            array (
                'id' => 451,
                'codigo' => '8699',
                'actividad' => 'Otras actividades de atención de la salud humana',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            451 => 
            array (
                'id' => 452,
                'codigo' => '8710',
                'actividad' => 'Actividades de atención residencial medicalizada de tipo general',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            452 => 
            array (
                'id' => 453,
                'codigo' => '8720',
                'actividad' => 'Actividades de atención residencial, para el cuidado de pacientes con retardo mental, enfermedad mental y consumo de sustancias psicoactivas',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            453 => 
            array (
                'id' => 454,
                'codigo' => '8730',
                'actividad' => 'Actividades de atención en instituciones para el cuidado de personas mayores y/o discapacitadas',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            454 => 
            array (
                'id' => 455,
                'codigo' => '8790',
                'actividad' => 'Otras actividades de atención en instituciones con alojamiento',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            455 => 
            array (
                'id' => 456,
                'codigo' => '8810',
                'actividad' => 'Actividades de asistencia social sin alojamiento para personas mayores y discapacitadas',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            456 => 
            array (
                'id' => 457,
                'codigo' => '8890',
                'actividad' => 'Otras actividades de asistencia social sin alojamiento',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            457 => 
            array (
                'id' => 458,
                'codigo' => '9001',
                'actividad' => 'Creación literaria',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            458 => 
            array (
                'id' => 459,
                'codigo' => '9002',
                'actividad' => 'Creación musical',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            459 => 
            array (
                'id' => 460,
                'codigo' => '9003',
                'actividad' => 'Creación teatral',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            460 => 
            array (
                'id' => 461,
                'codigo' => '9004',
                'actividad' => 'Creación audiovisual',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            461 => 
            array (
                'id' => 462,
                'codigo' => '9005',
                'actividad' => 'Artes plásticas y visuales',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            462 => 
            array (
                'id' => 463,
                'codigo' => '9006',
                'actividad' => 'Actividades teatrales',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            463 => 
            array (
                'id' => 464,
                'codigo' => '9007',
                'actividad' => 'Actividades de espectáculos musicales en vivo',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            464 => 
            array (
                'id' => 465,
                'codigo' => '9008',
                'actividad' => 'Otras actividades de espectáculos en vivo',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            465 => 
            array (
                'id' => 466,
                'codigo' => '9101',
                'actividad' => 'Actividades de bibliotecas y archivos',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            466 => 
            array (
                'id' => 467,
                'codigo' => '9102',
                'actividad' => 'Actividades y funcionamiento de museos, conservación de edificios y sitios históricos',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            467 => 
            array (
                'id' => 468,
                'codigo' => '9103',
                'actividad' => 'Actividades de jardines botánicos, zoológicos y reservas naturales',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            468 => 
            array (
                'id' => 469,
                'codigo' => '9200',
                'actividad' => 'Actividades de juegos de azar y apuestas',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            469 => 
            array (
                'id' => 470,
                'codigo' => '9311',
                'actividad' => 'Gestión de instalaciones deportivas',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            470 => 
            array (
                'id' => 471,
                'codigo' => '9312',
                'actividad' => 'Actividades de clubes deportivos',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            471 => 
            array (
                'id' => 472,
                'codigo' => '9319',
                'actividad' => 'Otras actividades deportivas',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            472 => 
            array (
                'id' => 473,
                'codigo' => '9321',
                'actividad' => 'Actividades de parques de atracciones y parques temáticos',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            473 => 
            array (
                'id' => 474,
                'codigo' => '9329',
                'actividad' => 'Otras actividades recreativas y de esparcimiento n.c.p.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            474 => 
            array (
                'id' => 475,
                'codigo' => '9411',
                'actividad' => 'Actividades de asociaciones empresariales y de empleadores',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            475 => 
            array (
                'id' => 476,
                'codigo' => '9412',
                'actividad' => 'Actividades de asociaciones profesionales',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            476 => 
            array (
                'id' => 477,
                'codigo' => '9420',
                'actividad' => 'Actividades de sindicatos de empleados',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            477 => 
            array (
                'id' => 478,
                'codigo' => '9491',
                'actividad' => 'Actividades de asociaciones religiosas',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            478 => 
            array (
                'id' => 479,
                'codigo' => '9492',
                'actividad' => 'Actividades de asociaciones políticas',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            479 => 
            array (
                'id' => 480,
                'codigo' => '9499',
                'actividad' => 'Actividades de otras asociaciones n.c.p.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            480 => 
            array (
                'id' => 481,
                'codigo' => '9511',
                'actividad' => 'Mantenimiento y reparación de computadores y de equipo periférico',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            481 => 
            array (
                'id' => 482,
                'codigo' => '9512',
                'actividad' => 'Mantenimiento y reparación de equipos de comunicación',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            482 => 
            array (
                'id' => 483,
                'codigo' => '9521',
                'actividad' => 'Mantenimiento y reparación de aparatos electrónicos de consumo',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            483 => 
            array (
                'id' => 484,
                'codigo' => '9522',
                'actividad' => 'Mantenimiento y reparación de aparatos y equipos domésticos y de jardinería',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            484 => 
            array (
                'id' => 485,
                'codigo' => '9523',
                'actividad' => 'Reparación de calzado y artículos de cuero',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            485 => 
            array (
                'id' => 486,
                'codigo' => '9524',
                'actividad' => 'Reparación de muebles y accesorios para el hogar',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            486 => 
            array (
                'id' => 487,
                'codigo' => '9529',
                'actividad' => 'Mantenimiento y reparación de otros efectos personales y enseres domésticos',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            487 => 
            array (
                'id' => 488,
                'codigo' => '9601',
                'actividad' => 'Lavado y limpieza, incluso la limpieza en seco, de productos textiles y de piel',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            488 => 
            array (
                'id' => 489,
                'codigo' => '9602',
                'actividad' => 'Peluquería y otros tratamientos de belleza',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            489 => 
            array (
                'id' => 490,
                'codigo' => '9603',
                'actividad' => 'Pompas fúnebres y actividades relacionadas',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            490 => 
            array (
                'id' => 491,
                'codigo' => '9609',
                'actividad' => 'Otras actividades de servicios personales n.c.p.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            491 => 
            array (
                'id' => 492,
                'codigo' => '9700',
                'actividad' => 'Actividades de los hogares individuales como empleadores de personal doméstico',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            492 => 
            array (
                'id' => 493,
                'codigo' => '9810',
                'actividad' => 'Actividades no diferenciadas de los hogares individuales como productores de bienes para uso propio',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            493 => 
            array (
                'id' => 494,
                'codigo' => '9820',
                'actividad' => 'Actividades no diferenciadas de los hogares individuales como productores de servicios para uso propio',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
            494 => 
            array (
                'id' => 495,
                'codigo' => '9900',
                'actividad' => 'Actividades de organizaciones y entidades extraterritoriales',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => NULL,
            ),
        ));
        
        
    }
}