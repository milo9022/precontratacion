<?php

use Illuminate\Database\Seeder;

class TblGarantiasContractualesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('tbl_garantias')->insert(
            [
                "nombre"=>"Prestacion de servicios",
                "created_at"=> date('Y-m-d H:i:s')
            ]);
        \DB::table('tbl_garantias')->insert(
            [
                "nombre"=>"consultoria",
                "created_at"=> date('Y-m-d H:i:s')
            ]);
        \DB::table('tbl_garantias')->insert(
            [
                "nombre"=>"Consecion",
                "created_at"=> date('Y-m-d H:i:s')
            ]);
        \DB::table('tbl_garantias')->insert(
            [
                "nombre"=>"Suministro",
                "created_at"=> date('Y-m-d H:i:s')
            ]);
        \DB::table('tbl_garantias')->insert(
            [
                "nombre"=>"compraventa",
                "created_at"=> date('Y-m-d H:i:s')
            ]);
        \DB::table('tbl_garantias')->insert(
            [
                "nombre"=>"Comodato",
                "created_at"=> date('Y-m-d H:i:s')
            ]);
        \DB::table('tbl_garantias')->insert(
            [
                "nombre"=>"Contrato de obra",
                "created_at"=> date('Y-m-d H:i:s')
            ]);
        \DB::table('tbl_garantias')->insert(
            [
                "nombre"=>"Emprestito",
                "created_at"=> date('Y-m-d H:i:s')
            ]);
        \DB::table('tbl_garantias')->insert(
            [
                "nombre"=>"Arrendamiento",
                "created_at"=> date('Y-m-d H:i:s')
            ]);
        \DB::table('tbl_garantias')->insert(
            [
                "nombre"=>"N/A",
                "created_at"=> date('Y-m-d H:i:s')
            ]);
    }
}
