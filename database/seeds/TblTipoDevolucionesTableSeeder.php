<?php

use Illuminate\Database\Seeder;

class TblTipoDevolucionesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tipos=[
            ['id'=>1,'nombre'=>'Informe de supervision',"created_at"=> now(),"updated_at" => now()],
            ['id'=>2,'nombre'=>'Contrato',"created_at"=> now(),"updated_at" => now()],
            ['id'=>3,'nombre'=>'Elaboracion y trámite de cancelación',"created_at"=> now(),"updated_at" => now()],
            ['id'=>4,'nombre'=>'Registro Contable',"created_at"=> now(),"updated_at" => now()],
            ['id'=>5,'nombre'=>'Registro presupuestal',"created_at"=> now(),"updated_at" => now()],
            ['id'=>6,'nombre'=>'Firma profesional universitario de administración',"created_at"=> now(),"updated_at" => now()],
            ['id'=>7,'nombre'=>'Firma de gerencia',"created_at"=> now(),"updated_at" => now()],
            ['id'=>8,'nombre'=>'Tesorería',"created_at"=> now(),"updated_at" => now()],
            ['id'=>9,'nombre'=>'Presentacion de la cuenta y demas soportes',"created_at"=> now(),"updated_at" => now()]
        ];
    }
}
