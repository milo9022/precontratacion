<?php

use Illuminate\Database\Seeder;

class TblContratacionDirectaTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('tbl_contratacion_directa')->delete();
        
        \DB::table('tbl_contratacion_directa')->insert(array (
            0 => 
            array (
                'id' => 1,
                'causal' => 'causal',
                'observacion' => 'prueba',
                'estudios_previos_id' => 1,
                'deleted_at' => NULL,
                'created_at' => '2020-04-29 18:20:21',
                'updated_at' => '2020-04-29 18:20:21',
            ),
        ));
        
        
    }
}