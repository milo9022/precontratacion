<?php

use Illuminate\Database\Seeder;

class CargosTableseeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('tbl_cargos')->insert([
            "nombre" => "Funcionario de la ESE Popayán"
        ]);
    }
}
