<?php

use Illuminate\Database\Seeder;

class TblContratosCuotasTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('tbl_contratos_cuotas')->delete();
        
        \DB::table('tbl_contratos_cuotas')->insert(array (
            0 => 
            array (
                'id' => 1,
                'cuota' => 1,
                'contratos_id' => 1,
                'fecha' => '2020-04-01',
                'valor' => 1007900.0,
                'acta' => 'prueba de cuota 1',
                'created_at' => '2020-04-29 18:22:23',
                'updated_at' => '2020-04-29 18:22:23',
            ),
            1 => 
            array (
                'id' => 2,
                'cuota' => 2,
                'contratos_id' => 1,
                'fecha' => '2020-04-02',
                'valor' => 1007900.0,
                'acta' => 'prueba de cuota 2',
                'created_at' => '2020-04-29 18:22:23',
                'updated_at' => '2020-04-29 18:22:23',
            ),
        ));
        
        
    }
}