<?php

use Illuminate\Database\Seeder;
use App\Models\EstudiosPreviosEstadoModel;

class EstudiosPreviosEstadoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = new EstudiosPreviosEstadoModel();
        $data->id=1;
        $data->nombre = 'ETAPA PRECONTRACTUAL';
        $data->save();
        
        $data = new EstudiosPreviosEstadoModel();
        $data->id=2;
        $data->nombre = 'ETAPA CONTRACTUAL';
        $data->save();
        
        $data = new EstudiosPreviosEstadoModel();
        $data->id=3;
        $data->nombre = 'ETAPA POST-CONTRACTUAL';
        $data->save();
    }
}
