<?php

use Illuminate\Database\Seeder;

class TblContratosEstadosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('tbl_contratos_estados')->delete();
        $data=array();
        $data[]=['id'=>1,'nombre'=>'En ejecución'];
        $data[]=['id'=>2,'nombre'=>'Suspendido'];
        $data[]=['id'=>3,'nombre'=>'Con otro si'];
        $data[]=['id'=>4,'nombre'=>'En liquidación'];
        $data[]=['id'=>5,'nombre'=>'Procede a liquidar'];
        \DB::table('tbl_contratos_estados')->insert($data);
    }
}
