<?php

use Illuminate\Database\Seeder;

class TblRecursosHumanosTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('tbl_recursos_humanos')->delete();
        
        \DB::table('tbl_recursos_humanos')->insert(array (
            0 => 
            array (
                'id' => 1,
                'nombre_primero' => 'camilo',
                'nombre_segundo' => 'Ernesto',
                'apellido_primero' => 'Supervisor',
                'apellido_segundo' => 'Final',
                'documento' => '1061716139',
                'direccion' => '',
                'documento_tipo_id' => 1,
                'celular' => '3186234042',
                'user_id' => 2,
                'activo' => 1,
                'email' => 'sup@esepopayan.gov.co',
                'cargo_id' => 1,
                'deleted_at' => NULL,
                'created_at' => '2020-06-24 11:51:20',
                'updated_at' => '2020-06-24 11:51:20',
            ),
            1 => 
            array (
                'id' => 2,
                'nombre_primero' => 'EFREN',
                'nombre_segundo' => NULL,
                'apellido_primero' => 'DIAZ',
                'apellido_segundo' => 'BONILLA',
                'documento' => '1',
                'direccion' => NULL,
                'documento_tipo_id' => 3,
                'celular' => '1',
                'user_id' => 3,
                'activo' => 1,
                'email' => '1@esepopayan.gov.co',
                'cargo_id' => 1,
                'deleted_at' => NULL,
                'created_at' => '2020-06-24 11:55:34',
                'updated_at' => '2020-06-24 11:55:34',
            ),
            2 => 
            array (
                'id' => 3,
                'nombre_primero' => 'FRANKLIN',
                'nombre_segundo' => 'RENE',
                'apellido_primero' => 'PATIÑO',
                'apellido_segundo' => NULL,
                'documento' => '2',
                'direccion' => NULL,
                'documento_tipo_id' => 3,
                'celular' => '2',
                'user_id' => 4,
                'activo' => 1,
                'email' => '2@esepopayan.gov.co',
                'cargo_id' => 1,
                'deleted_at' => NULL,
                'created_at' => '2020-06-24 11:59:49',
                'updated_at' => '2020-06-24 11:59:49',
            ),
            3 => 
            array (
                'id' => 4,
                'nombre_primero' => 'HERNÁN',
                'nombre_segundo' => 'EFRÉN',
                'apellido_primero' => 'DÍAZ',
                'apellido_segundo' => 'BONILLA',
                'documento' => '3',
                'direccion' => NULL,
                'documento_tipo_id' => 3,
                'celular' => '3',
                'user_id' => 5,
                'activo' => 1,
                'email' => '3@esepopayan.gov.co',
                'cargo_id' => 1,
                'deleted_at' => NULL,
                'created_at' => '2020-06-24 12:00:21',
                'updated_at' => '2020-06-24 12:00:21',
            ),
            4 => 
            array (
                'id' => 5,
                'nombre_primero' => 'JUAN',
                'nombre_segundo' => 'CARLOS',
                'apellido_primero' => 'COTAZO',
                'apellido_segundo' => NULL,
                'documento' => '4',
                'direccion' => NULL,
                'documento_tipo_id' => 3,
                'celular' => '4',
                'user_id' => 6,
                'activo' => 1,
                'email' => '4@esepopayan.gov.co',
                'cargo_id' => 1,
                'deleted_at' => NULL,
                'created_at' => '2020-06-24 12:00:55',
                'updated_at' => '2020-06-24 12:00:55',
            ),
            5 => 
            array (
                'id' => 6,
                'nombre_primero' => 'CARMEN',
                'nombre_segundo' => 'STELLA',
                'apellido_primero' => 'POTES',
                'apellido_segundo' => 'SATIZÁBAL',
                'documento' => '5',
                'direccion' => NULL,
                'documento_tipo_id' => 3,
                'celular' => '5',
                'user_id' => 7,
                'activo' => 1,
                'email' => '5@esepopayan.gov.co',
                'cargo_id' => 1,
                'deleted_at' => NULL,
                'created_at' => '2020-06-24 12:01:41',
                'updated_at' => '2020-06-24 12:01:41',
            ),
            6 => 
            array (
                'id' => 7,
                'nombre_primero' => 'EDILBERTO',
                'nombre_segundo' => NULL,
                'apellido_primero' => 'PALOMINO',
                'apellido_segundo' => 'MARTÍNEZ',
                'documento' => '6',
                'direccion' => NULL,
                'documento_tipo_id' => 3,
                'celular' => '6',
                'user_id' => 8,
                'activo' => 1,
                'email' => '6@esepopayan.gov.co',
                'cargo_id' => 1,
                'deleted_at' => NULL,
                'created_at' => '2020-06-24 12:03:45',
                'updated_at' => '2020-06-24 12:03:45',
            ),
            7 => 
            array (
                'id' => 8,
                'nombre_primero' => 'LUISA',
                'nombre_segundo' => NULL,
                'apellido_primero' => 'TISOY',
                'apellido_segundo' => 'CASTRO',
                'documento' => '7',
                'direccion' => NULL,
                'documento_tipo_id' => 3,
                'celular' => '7',
                'user_id' => 9,
                'activo' => 1,
                'email' => '7@esepopayan.gov.co',
                'cargo_id' => 1,
                'deleted_at' => NULL,
                'created_at' => '2020-06-24 12:04:31',
                'updated_at' => '2020-06-24 12:04:31',
            ),
        ));
        
        
    }
}