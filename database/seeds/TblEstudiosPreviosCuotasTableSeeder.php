<?php

use Illuminate\Database\Seeder;

class TblEstudiosPreviosCuotasTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('tbl_estudios_previos_cuotas')->delete();
        
        \DB::table('tbl_estudios_previos_cuotas')->insert(array (
            0 => 
            array (
                'id' => 1,
                'estudios_previos_id' => 1,
                'cuota' => 1,
                'valor' => 1007900.0,
                'created_at' => '2020-04-29 18:20:14',
                'updated_at' => '2020-04-29 18:20:14',
            ),
            1 => 
            array (
                'id' => 2,
                'estudios_previos_id' => 1,
                'cuota' => 2,
                'valor' => 1007900.0,
                'created_at' => '2020-04-29 18:20:14',
                'updated_at' => '2020-04-29 18:20:14',
            ),
        ));
        
        
    }
}