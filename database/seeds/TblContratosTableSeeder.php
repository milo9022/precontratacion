<?php

use Illuminate\Database\Seeder;

class TblContratosTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('tbl_contratos')->delete();
        
        \DB::table('tbl_contratos')->insert(array (
            0 => 
            array (
                'id' => 1,
                'numero' => '1231',
                'suscripcion_fecha' => '2020-04-01',
                'objeto' => 'objeto',
                'personeria_tipo' => 'juridica',
                'contratista_id' => 1,
                'valor_contrato' => 2015800.0,
                'vigencia_desde' => '2020-04-01',
                'vigencia_hasta' => '2020-04-02',
                'forma_de_pago_id' => 2,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
                'estudios_previos_id' => 1,
                'recursos_humanos_supervisor_id' => 1,
                'recursos_humanos_apoyo_id' => 3,
            ),
            1 => 
            array (
                'id' => 2,
                'numero' => '3424',
                'suscripcion_fecha' => '2020-04-04',
                'objeto' => 'objeto 2',
                'personeria_tipo' => 'juridica',
                'contratista_id' => 1,
                'valor_contrato' => 2015800.0,
                'vigencia_desde' => '2020-04-01',
                'vigencia_hasta' => '2020-04-02',
                'forma_de_pago_id' => 2,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
                'estudios_previos_id' => 1,
                'recursos_humanos_supervisor_id' => 1,
                'recursos_humanos_apoyo_id' => 3,
            ),
            
        ));
        
        
    }
}